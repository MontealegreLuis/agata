<?php
/**
 * Determine if a record does not exist in a given table filtering by a given
 * field
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LNJ <lemuel.nonoal@mandragora-web-systems.com>
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Validate\Db\Doctrine;

/**
 * Determine if a record does not exist in a given table filtering by a given
 * field
 *
 * @author     LNJ <lemuel.nonoal@mandragora-web-systems.com>
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class NoRecordExists extends ValidateRecord
{
    /**
     * @param string
     * @return boolean
     */
    public function isValid($value)
    {
        $this->_setValue($value);

        if ($this->query($value)) {
            $this->_error(static::ERROR_RECORD_FOUND);

            return false;
        }

        return true;
    }
}
