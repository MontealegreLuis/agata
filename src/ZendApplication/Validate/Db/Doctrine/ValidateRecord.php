<?php
/**
 * Base class to determine if a record exists or not in a given table filtering
 * by a given field
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LNJ <lemuel.nonoal@mandragora-web-systems.com>
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Validate\Db\Doctrine;

use \Zend_Validate_Abstract as Validation;
use \Zend_Validate_Exception as ValidationException;
use \Doctrine_Core as Doctrine;

/**
 * Base class to determine if a record exists or not in a given table filtering
 * by a given field
 *
 * @author     LNJ <lemuel.nonoal@mandragora-web-systems.com>
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
abstract class ValidateRecord extends Validation
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $field;

    /**
     * Error thrown when the record already exists in the table
     *
     * @var string
     */
    const ERROR_RECORD_FOUND = 'errorRecordFound';

    /**
     * Error thrown when the record does not exist in the table
     *
     * @var string
     */
    const ERROR_NO_RECORD_FOUND = 'errorNoRecordFound';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::ERROR_RECORD_FOUND =>
            "The field '%field%' already has a value '%value%'",
        self::ERROR_NO_RECORD_FOUND =>
            "The field '%field%' does not have a value '%value%'"
    );

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $_messageVariables = array(
        'field' => 'field',
    );

    /**
     * @param array $options
     *      - table: The Doctrine table name
     *      - field: The field name which will filter the query
     */
    public function __construct(array $options)
    {
        if (!array_key_exists('table', $options)) {
            throw new ValidationException('Table option missing');
        }
        if (!array_key_exists('field', $options)) {
            throw new ValidationException('Field option missing');
        }
        $this->table = (string) $options['table'];
        $this->setField($options['field']);
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = (string) $field;
    }

    /**
     * @param  string          $value
     * @return Doctrine_Record | boolean
     */
    protected function query($value)
    {
        $table = Doctrine::getTable($this->table);
        $query = 'findOneBy' . ucfirst($this->field);

        return $table->$query($value);
    }
}
