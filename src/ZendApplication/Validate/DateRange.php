<?php
/**
 * Validate a date range
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Validate;

use \Zend_Validate_Abstract as Validation;
use \Zend_Validate_Date as DateValidation;
use \Zend_Date as Date;

/**
 * Validate a date range
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class DateRange extends Validation
{
    const INVALID_START_DATE = 'invalidStartDate';
    const INVALID_STOP_DATE = 'invalidStopDate';
    const STOP_DATE_OUT_OF_BOUNDS = 'stopDateOutOfBound';
    const INVALID_RANGE_DATE = 'invalidRangeDate';

    /**
     * @var string
     */
    protected $startDate;

    /**
     * @var string
     */
    protected $stopDate;

    /**
     * @var string
     */
    protected $maxStopDate;

    /**
     * @var Zend_Validate_Date
     */
    protected $dateValidator;

    /**
     * @var array
     */
    protected $_messageTemplates = [
        static::INVALID_START_DATE =>
            "'%startDate%' no es una fecha inicial válida",
        static::INVALID_STOP_DATE =>
            "'%stopDate%' no es una fecha final válida",
        static::INVALID_RANGE_DATE =>
            "'%startDate%' debe ser una fecha anterior a '%stopDate%'",
        static::STOP_DATE_OUT_OF_BOUNDS =>
            "'%stopDate%' debe ser una fecha anterior a '%maxStopDate%'",
    ];

    /**
     * @var array
     */
    protected $_messageVariables = array(
        'startDate' => 'startDate',
        'stopDate' => 'stopDate',
        'maxStopDate' => 'maxStopDate',
    );

    public function __construct(Date $maxStopDate = null)
    {
        $this->dateValidator = new DateValidation(['format' => 'YYYY-MM-dd']);
        $this->maxStopDate = $maxStopDate;
    }

    /**
     * @param  string $startDate
     * @return void
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @param  string  $value
     * @return boolean
     */
    public function isValid($value)
    {
        $this->stopDate = $value;
        if (!$this->dateValidator->isValid($this->startDate)) {
            $this->_error(static::INVALID_START_DATE);

            return false;
        }
        if (!$this->dateValidator->isValid($this->stopDate)) {
            $this->_error(static::INVALID_STOP_DATE);

            return false;
        }
        $startDate = new Date($this->startDate, 'YYYY-MM-dd');
        $stopDate = new Date($this->stopDate, 'YYYY-MM-dd');
        if ($this->maxStopDate != null && $stopDate->compare($this->maxStopDate) > 0) {
            $this->_error(self::STOP_DATE_OUT_OF_BOUNDS);

            return false;
        }
        if ($startDate->compare($stopDate) > 0) {
            $this->_error(self::INVALID_RANGE_DATE);

            return false;
        }

        return true;
    }
}
