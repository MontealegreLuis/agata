<?php
/**
 * Handle PHP errors as exceptions
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\ErrorHandler;

use \ErrorException;

/**
 * Handle PHP errors as exceptions
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class ErrorToExceptionHandler
{
    /**
     * @return void
     */
    public function register()
    {
        set_error_handler([$this, 'handleError']);
    }

    /**
     * @param int    $errorNumber
     * @param string $errorMessage
     */
    public function handleError($errorNumber, $errorMessage, $errfile, $errline, $errcontext)
    {
        throw new ErrorException($errorMessage, 0, $errorNumber, $errfile, $errline);
    }
}
