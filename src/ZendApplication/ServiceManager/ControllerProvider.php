<?php
/**
 * Base class for ServiceManager configuration classes that provide controllers
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ZendApplication\ServiceManager;

use \Zend_Controller_Request_Http as Request;
use \Zend_Controller_Response_Http as Response;
use \Zend\ServiceManager\ConfigInterface;

/**
 * Base class for ServiceManager configuration classes that provide controllers
 */
abstract class ControllerProvider implements ConfigInterface
{
    /** @type string */
    protected $key;

    /** @type Request */
    protected $request;

    /** @type Response */
    protected $response;

    /** @type array */
    protected $params;

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $params
     */
    public function configure(Request $request, Response $response, array $params)
    {
        $this->request = $request;
        $this->response = $response;
        $this->params = $params;
    }
}
