<?php
namespace ZendApplication\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\Config;
use \Zend\ServiceManager\Exception\ServiceNotFoundException;

abstract class ApplicationKernel implements ConfigInterface
{
    /** @type ConfigInterface[] */
    protected $serviceProviders;

    /** @type ControllerProvider[] */
    protected $controllerProviders;

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        /** @type \Zend\ServiceManager\Config $provider */
        foreach ($this->serviceProviders as $provider) {
            $provider->configureServiceManager($serviceManager);
        }
    }

    /**
     * @param  string                   $key
     * @return ControllerProvider
     * @throws ServiceNotFoundException
     */
    public function getControllerProvider($key)
    {
        foreach ($this->controllerProviders as $provider) {
            if ($key === $provider->getKey()) {
                return $provider;
            }
        }
        throw new ServiceNotFoundException("Controller provider with key '$key' not found");
    }
}
