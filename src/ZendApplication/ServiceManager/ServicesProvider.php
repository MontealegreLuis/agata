<?php
/**
 * Shared services configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\ServiceManager;

use \Zend_Application_Resource_Mail as Mailer;
use \Zend_Log as Logger;
use \Zend\ServiceManager\Config;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\View\TwigViewProvider;
use \ZendApplication\Cache\CacheManagerProvider;
use \ZendApplication\Locale\LocaleProvider;
use \ZendApplication\Authentication\AuthenticationProvider;
use \ZendApplication\Paginator\PaginatorFactory;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;

/**
 * Shared services configuration
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class ServicesProvider extends Config
{
    /**
     * Register shared application services
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'view' => function (ServiceLocatorInterface $sl) {
                $twig = new TwigViewProvider(require $sl->get('view.configuration'));

                return $twig->createService($sl);
            },
            'cache.manager' => function (ServiceLocatorInterface $sl) {
                $manager = new CacheManagerProvider(require $sl->get('cache.configuration'));

                return $manager->createService($sl);
            },
            'locale' => function (ServiceLocatorInterface $sl) {
                $locale =  new LocaleProvider(require $sl->get('locale.configuration'));

                return $locale->createService($sl);
            },
            'authentication' => function (ServiceLocatorInterface $sl) {
                $auth = new AuthenticationProvider(require $sl->get('authentication.configuration'));

                return $auth->createService($sl);
            },
            'logger' => function(ServiceLocatorInterface $sl) {
                $configuration = require $sl->get('logger.configuration');
                if (isset($configuration['mail'])) {
                    $mailer = $sl->get('mailer');
                    $mailer->init();
                }

                return Logger::factory($configuration);
            },
            'mailer' => function(ServiceLocatorInterface $sl) {
                return new Mailer(require $sl->get('mailer.configuration'));
            },
            'paginator' => function(ServiceLocatorInterface $sl) {
                return new PaginatorFactory(require $sl->get('paginator.configuration'));
            },
        ];
        $this->config['initializers'] = [
            function($instance, ServiceLocatorInterface $sl) {
                if ($instance instanceof ControllerAction) {
                    $event = new HttpActionEvent();
                    $event->setRequest($instance->getRequest());
                    $event->setResponse($instance->getResponse());
                    $event->setView($sl->get('view'));

                    $instance->setActionEvent($event);
                }
            },
        ];
    }
}
