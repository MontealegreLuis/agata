<?php
/**
 * Initializes Zend_Auth
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Authentication;

use \Zend_Auth as Auth;
use \Zend_Auth_Storage_Session as SessionStorage;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\FactoryInterface;
use \ZendApplication\Config\CanBeConfigured;

/**
 * Initializes Zend_Auth
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class AuthenticationProvider extends CanBeConfigured implements FactoryInterface
{
    /**
     * @param  ServiceLocatorInterface $sl
     * @return Zend_Auth
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        $authentication = Auth::getInstance();
        $storage = new SessionStorage($this->options['namespace']);
        $authentication->setStorage($storage);

        return $authentication;
    }
}
