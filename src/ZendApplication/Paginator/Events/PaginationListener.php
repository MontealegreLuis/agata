<?php
namespace ZendApplication\Paginator\Events;

use \ZendApplication\Paginator\PaginatorFactory;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;
use \Zend_Controller_Request_Http as Request;
use \Zend_View_Abstract as View;
use \Countable;

class PaginationListener
{
    /**
     * @var PaginatorFactory
     */
    protected $paginatorFactory;

    /**
     * @var Countable
     */
    protected $table;

    /**
     * @param PaginatorFactory $paginatorFactory
     * @param Countable        $table
     */
    public function __construct(PaginatorFactory $paginatorFactory, Countable $table)
    {
        $this->paginatorFactory = $paginatorFactory;
        $this->table = $table;
    }

    /**
     * @param HttpActionEvent $event
     */
    public function __invoke(HttpActionEvent $event)
    {
        $this->addPagination($event->getRequest(), $event->getView());
    }

    /**
     * @param  PaginationEvent $event
     * @return \Zend_Paginator
     */
    public function addPagination(Request $request, View $view)
    {
        $view->paginator = $this->paginatorFactory->fromArray(
            $view->items, $this->table->count($request->getParams()), $request->getParam('page', 1)
        );
    }
}
