<?php
namespace ZendApplication\Paginator;

use \Zend_Paginator as Paginator;
use \ZendApplication\Paginator\Adapter\FixedAdapter;
use \ZendApplication\Config\CanBeConfigured;

class PaginatorFactory extends CanBeConfigured
{
    /**
     * @return integer
     */
    public function getItemCountPerPage()
    {
        return $this->getOption('itemCountPerPage');
    }

    /**
     * @param  array     $items
     * @param  integer   $count
     * @param  integer   $pageNumber
     * @return Paginator
     */
    public function fromArray(array $items, $count, $pageNumber)
    {
        $paginator = new Paginator(new FixedAdapter($items, $count));
        $paginator->setItemCountPerPage($this->getItemCountPerPage());
        $paginator->setPageRange($this->getOption('pageRange'));
        $paginator->setCurrentPageNumber($pageNumber);

        return $paginator;
    }
}
