<?php
namespace ZendApplication\Paginator\Adapter;

use \Zend_Paginator_Adapter_Interface as Paginator;

class FixedAdapter implements Paginator
{
    /**
     * @var array
     */
    protected $items;

    /**
     * @var count
     */
    protected $count;

    /**
     * @param array $items
     * @param int   $count
     */
    public function __construct(array $items, $count)
    {
        $this->items = $items;
        $this->count = $count;
    }

    /**
     * @see Zend_Paginator_Adapter_Interface::getItems()
     */
    public function getItems($offset, $itemCountPerPage)
    {
        return $this->items;
    }

    /**
     * @see Countable::count()
     */
    public function count()
    {
        return $this->count;
    }
}
