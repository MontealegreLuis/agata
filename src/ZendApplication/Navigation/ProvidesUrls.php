<?php
namespace ZendApplication\Navigation;

trait ProvidesUrls
{
    /**
     * @var UrlContainer
     */
    protected $urlContainer;

    /**
     * @param UrlContainer $urlContainer
     */
    public function setUrlContainer(UrlContainer $urlContainer)
    {
        $this->urlContainer = $urlContainer;
    }

    /**
     * @param  string $key
     * @param  array  $params
     * @return string
     */
    public function getUrl($key, array $params =[])
    {
        return $this->urlContainer->get($key, $params);
    }
}
