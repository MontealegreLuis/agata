<?php
namespace ZendApplication\Navigation;

use \Zend_View_Abstract as View;

class UrlContainer
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var array
     */
    protected $mvcPages;

    /**
     * @param array $pages
     * @param View  $view
     */
    public function __construct(array $pages, View $view)
    {
        $this->mvcPages = $pages;
        $this->view = $view;
    }

    /**
     * Returns the URL for a given Zend MVC route
     *
     * @param  string $key
     * @param  array  $params
     * @return string
     */
    public function get($key, array $params = [])
    {
        $route = $this->mvcPages[$key]['route'];
        unset($this->mvcPages[$key]['route']);

        return $this->view->url(
            array_merge($this->mvcPages[$key], $params), $route, $reset = true
        );
    }
}
