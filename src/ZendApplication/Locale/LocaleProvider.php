<?php
/**
 * Set the default locale for the application
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Locale;

use \Zend_Locale as Locale;
use \Zend_Cache_Core as Cache;
use \Zend_Registry as Registry;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\FactoryInterface;
use \ZendApplication\Config\CanBeConfigured;

/**
 * Set the default locale for the application
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class LocaleProvider extends CanBeConfigured implements FactoryInterface
{
    const DEFAULT_REGISTRY_KEY = 'Zend_Locale';

    /**
     * @var Zend_Locale
     */
    protected $locale;

    /**
     * Retrieve locale object
     *
     * @return Zend_Locale
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        $this->locale = new Locale($this->options['default']);
        Locale::setCache($sl->get('cache.manager')->getCache($this->options['cache']));
        // Many other components grab the Locale from the registry in a ZF1 application
        Registry::set(static::DEFAULT_REGISTRY_KEY, $this->locale);

        return $this->locale;
    }
}
