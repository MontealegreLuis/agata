<?php
/**
 * Custom dispatcher that uses a service manager to instantiate controllers
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ZendApplication\Controller\Dispatcher;

use \Zend\ServiceManager\Exception\ServiceNotFoundException;
use \Zend_Controller_Dispatcher_Standard as StandardDispatcher;
use \Zend_Controller_Request_Abstract as Request;
use \Zend_Controller_Response_Abstract as Response;
use \Zend_Controller_Dispatcher_Exception as DispatcherException;
use \Zend_Controller_Action_Interface as ActionInterface;
use \Zend_Controller_Action as Action;
use \Zend_Controller_Front as FrontController;
use \Zend\ServiceManager\ServiceManager;
use \Exception;

/**
 * Custom dispatcher that uses a service manager to instantiate controllers
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class ActionDispatcher extends StandardDispatcher
{
    /** @type ServiceManager */
    protected $serviceManager;

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @see Zend_Controller_Dispatcher_Standard::dispatch()
     */
    public function dispatch(Request $request, Response $response)
    {
        $this->setResponse($response);

        $controller = $this->createController($this->getControllerKey($request), $request);

        if (!($controller instanceof ActionInterface) && !($controller instanceof Action)) {
            $message = sprintf(
                'Controller "%s" is not an instance of Zend_Controller_Action_Interface',
                get_class($controller)
            );
            throw new DispatcherException($message);
        }

        $request->setDispatched(true);

        // by default, buffer output
        $disableOb = $this->getParam('disableOutputBuffering');
        $obLevel   = ob_get_level();
        if (empty($disableOb)) {
            ob_start();
        }

        try {
            $controller->dispatch($this->getActionMethod($request));
        } catch (Exception $e) {
            // Clean output buffer on error
            $curObLevel = ob_get_level();
            if ($curObLevel > $obLevel) {
                do {
                    ob_get_clean();
                    $curObLevel = ob_get_level();
                } while ($curObLevel > $obLevel);
            }
            throw $e;
        }

        if (empty($disableOb)) {
            $content = ob_get_clean();
            $response->appendBody($content);
        }

        $controller = null;
    }

    /**
     * @param  string              $key
     * @param  Request             $request
     * @return ActionInterface
     * @throws DispatcherException
     */
    protected function createController($key, Request $request)
    {
        $application = FrontController::getInstance()->getParam('bootstrap')->getApplication();

        try {
            $provider = $application->getControllerProvider($this->getControllerProviderKey($request));
            $provider->configure($request, $this->getResponse(), $this->getParams());
            $provider->configureServiceManager($this->serviceManager);

            return $this->serviceManager->get($key);
        } catch (ServiceNotFoundException $e) {
            throw new DispatcherException("Invalid controller specified ({$key})");
        }
    }

    /**
     * @param  Request $request
     * @return string
     */
    public function getControllerProviderKey(Request $request)
    {
        return strtolower(sprintf(
            '%s.%s',
            $this->formatModuleName($request->getModuleName()),
            $request->getControllerName()
        ));
    }

    /**
     * Format action class name
     *
     * @param  string $moduleName Name of the current module
     * @param  string $actionName Name of the current action
     * @param  string $className  Name of the action class
     * @return string Formatted class name
     */
    public function getControllerKey(Request $request)
    {
        return strtolower(sprintf(
            '%s.%s.%s',
            $this->formatModuleName($request->getModuleName()),
            $request->getControllerName(),
            $request->getActionName()
        ));
    }
}
