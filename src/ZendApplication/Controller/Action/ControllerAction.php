<?php
/**
 * Base class for Mandragora controllers
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Controller\Action;

use \Zend_Controller_Action as ZendController;
use \Zend\EventManager\ProvidesEvents;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;

/**
 * Base class for Mandragora controllers
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
abstract class ControllerAction extends ZendController
{
    use ProvidesEvents;

    /**
     * @var HttpActionEvent
     */
    protected $actionEvent;

    /**
     * @param HttpActionEvent $event
     */
    public function setActionEvent(HttpActionEvent $event)
    {
        $this->actionEvent = $event;
    }

    /**
     * Redirects to a given URL without prepending the base path
     *
     * @param string $url
     */
    public function redirectTo($url)
    {
        $this->redirect($url, ['prependBase' => false]);
    }

    /**
     * @param $namespace = null
     * @return Zend_Controller_Action_Helper_FlashMessenger
     */
    public function flash($namespace = null)
    {
        if ($namespace) {
            $this->_helper->flashMessenger->setNamespace($namespace);
        }

        return $this->_helper->flashMessenger;
    }

    /**
     * @return array
     */
    public function params()
    {
        return $this->_request->getParams();
    }

    /**
     * @param string $key
     * @param $default = null
     * @return mixed
     */
    public function param($key, $default = null)
    {
        return $this->_request->getParam($key, $default);
    }

    /**
     * @param  string $key     = null
     * @param  mixed  $default = null
     * @return array  | string
     */
    public function post($key = null, $default = null)
    {
        if (!$key) {
            return $this->_request->getPost();
        }

        return $this->_request->getPost($key, $default);
    }

    /**
     * @see Zend_Controller_Action::dispatch()
     */
    public function dispatch($action)
    {
        $this->actionEvent->setName('preDispatch');
        $this->getEventManager()->trigger($this->actionEvent);

        $this->_helper->notifyPreDispatch();

        $this->preDispatch();
        if ($this->getRequest()->isDispatched()) {
            if (!$this->getResponse()->isRedirect()) {
                $this->$action();
            }
            $this->postDispatch();
        }

        $this->actionEvent->setName('postDispatch');
        $this->getEventManager()->trigger($this->actionEvent);

        $this->_helper->notifyPostDispatch();
    }
}
