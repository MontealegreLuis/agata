<?php
namespace ZendApplication\Controller\Action\Events;

use \Zend_Controller_Action_Exception as ActionException;
use \Zend_Filter_Input as InputFilter;

class FilterInputListener
{
    /**
     * @var array
     */
    protected $validations;

    /**
     * @var array
     */
    protected $defaultFilters;

    /**
     * @param array $validations
     */
    public function __construct(array $validations)
    {
        $this->validations = $validations;
        $this->defaultFilters = ['HtmlEntities', 'StripTags', 'StringTrim'];
    }

    /**
     * Filters and validates the request parameters for the actions that do not use a form to filter
     * its input
     *
     * @param HttpActionEvent $event
     */
    public function __invoke(HttpActionEvent $event)
    {
        $filteredParams = $this->validateInput($event->getRequest()->getParams());
        $event->getRequest()->setParams($filteredParams);
    }

    /**
     * Validate parameters of GET and Ajax requests
     *
     * @param array $params
     * @retrurn array
     * @throws ActionException
     */
    public function validateInput(array $params)
    {
        $filters = [];
        $validators = [];
        foreach ($this->validations as $key => $validationType) {
            $filters[$key] = $this->defaultFilters;
            switch ($validationType) {
                case 'int' :
                    $validators[$key] = ['NotEmpty', 'Int'];
                    $filters[$key][] = 'Int';
                    $defaultFilter = 'Int';
                    break;
                case 'id' :
                    $validators[$key] = ['NotEmpty', 'Int', ['GreaterThan', 0]];
                    $filters[$key][] = 'Int';
                    $defaultFilter = 'Int';
                    break;
                case 'key' :
                    $validators[$key] = [['NotEmpty', ['string']], 'Alnum'];
                    $defaultFilter = 'Alnum';
                    break;
            }
        }

        $input = new InputFilter($filters, $validators, $params, ['escapeFilter' => $defaultFilter]);

        if (!$input->isValid()) {
            throw new ActionException(sprintf(
                'Error occured when validating the following input parameters: %s',
                implode(', ', array_keys($input->getErrors()))
            ));
        }

        return $input->getEscaped();
    }
}
