<?php
namespace ZendApplication\Controller\Action\Events;

use \Zend_Controller_Action_Exception as ActionException;
use \Zend_Controller_Request_Http as Request;

class HttpValidationListener
{
    /**
     * @var string
     */
    protected $validMethod;

    /**
     * @param string $validMethod
     */
    public function __construct($validMethod = 'get')
    {
        $this->validMethod =  ucfirst(strtolower($validMethod));
    }

    /**
     * Call <code>validateHttpMethod</code>
     */
    public function __invoke(HttpActionEvent $event)
    {
        $this->validateHttpMethod($event->getRequest());
    }

    /**
     * @param  string                           $method
     * @throws Zend_Controller_Action_Exception
     */
    public function validateHttpMethod(Request $request)
    {
        $isMethod = "is{$this->validMethod}";

        if (!$request->$isMethod()) {

            throw new ActionException(sprintf(
                'Invalid method "%" found, "%s" was expected',
                $request->getMethod(), strtoupper($this->validMethod)
            ));
        }
    }
}
