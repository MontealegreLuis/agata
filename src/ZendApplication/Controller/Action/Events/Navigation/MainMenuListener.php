<?php
/**
 * Pass the navigation menu to the view
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Controller\Action\Events\Navigation;

use \Zend_Cache_Core as Cache;
use \Zend_Navigation as Navigation;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;
use \RecursiveIteratorIterator;

/**
 * Pass the navigation menu to the view
 */
class MainMenuListener
{
    /** @type array */
    protected $pages;

    /** @type Cache */
    protected $cache;

    /**
     * @param array   $pages
     * @param Request $request
     * @param Cache   $cache
     * @param View    $view
     */
    public function __construct(array $pages, Cache $cache)
    {
        $this->pages = $pages;
        $this->cache = $cache;
    }

    /**
     * Select the menu accordingly
     *
     * @param  Request $request
     * @return void
     */
    public function __invoke(HttpActionEvent $event)
    {
        $request = $event->getRequest();
        if ($request->getParam('error_handler')) {
            return; // No menu is needed when an exception has been thrown
        }
        $module = $request->getModuleName();
        $pages = $this->cache->load("{$module}Nav");
        if (!$pages) {
            $container = new Navigation($this->pages);
            $this->setAccessKeyValues($container);
            $this->cache->save($container->toArray(), "{$module}Nav");
        } else {
            $container = new Navigation($pages);
        }
        $event->getView()->navigation($container);
    }

    /**
     * @param  Navigation $container
     * @return void
     */
    protected function setAccessKeyValues(Navigation $container)
    {
        $it = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);
        $i = 1;
        foreach ($it as $page) {
            $page->accesskey = (string) $i;
            $i++;
        }
    }
}
