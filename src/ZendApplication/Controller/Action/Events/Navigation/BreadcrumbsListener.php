<?php
/**
 * Breadcrumbs builder.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ZendApplication\Controller\Action\Events\Navigation;

use \Zend_Navigation as Navigation;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;
use \RecursiveIteratorIterator;

/**
 * Breadcrumbs builder.
 */
class BreadcrumbsListener
{
    /** @type array */
    protected $breadcrumbs;

    /**
     * @param array $breadcrumbs
     */
    public function __construct(array $breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * Pass the breadcrumbs to the view
     */
    public function __invoke(HttpActionEvent $event)
    {
        $container = new Navigation($this->breadcrumbs);
        $it = new RecursiveIteratorIterator($container, RecursiveIteratorIterator::SELF_FIRST);

        foreach ($it as $page) {
            foreach ($page->getParams() as $key => $value) {
                if (isset($params[$key])) {
                    $pageParams = $page->getParams();
                    $pageParams[$key] = $params[$key];
                    $page->setParams($pageParams);
                }
            }
        }

        $event->getView()->breadcrumbs = $container;
    }
}
