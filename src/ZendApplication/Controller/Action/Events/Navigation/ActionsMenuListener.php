<?php
/**
 * Configure navigation action links for common CRUD operations
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragorawebsystems.com>
 * @copyright  Mandrágora WebBased Systems 2010-2014
 */
namespace ZendApplication\Controller\Action\Events\Navigation;

use \Zend_Navigation as Navigation;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;

/**
 * Configure navigation action links for common CRUD operations
 */
class ActionsMenuListener
{
    /** @type array */
    protected $pages;

    /**
     * @param array $pages
     */
    public function __construct(array $pages)
    {
        $this->pages = $pages;
    }

    /**
     * Create the navigation for the entities actions menu
     */
    public function __invoke(HttpActionEvent $event)
    {
        $event->getView()->actions = new Navigation($this->pages);
    }
}
