<?php
/**
 * Factory for ZF1 CacheManager
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Cache;

use \Zend_Cache_Manager as CacheManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\FactoryInterface;
use \ZendApplication\Config\CanBeConfigured;

/**
 * Factory for ZF1 CacheManager
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class CacheManagerProvider extends CanBeConfigured implements FactoryInterface
{
    /**
     * @var Zend_Cache_Manager
     */
    protected $manager;

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        $this->manager = new CacheManager();
        foreach ($this->options as $key => $value) {
            if ($this->manager->hasCacheTemplate($key)) {
                $this->manager->setTemplateOptions($key, $value);
            } else {
                $this->manager->setCacheTemplate($key, $value);
            }
        }

        return $this->manager;
    }
}
