<?php
/**
 * Base class for objects with configuration options
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Config;

/**
 * Base class for objects with configuration options
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class CanBeConfigured
{
    use Options;

    public function __construct(array $options = [])
    {
        $this->options = $options;
    }
}
