<?php
/**
 * Trait to add configuration options
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Config;

/**
 * Trait to add configuration options
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
trait Options
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @param  string  $key
     * @return boolean
     */
    public function hasOption($key)
    {
        return isset($this->options[$key]);
    }

    /**
     * @param  string $key
     * @return mixed
     */
    public function getOption($key)
    {
        return $this->options[$key];
    }
}
