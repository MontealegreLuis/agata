<?php
/**
 * Exception thrown when read/write or create operations cannot be performed
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\File;

use \RuntimeException;

/**
 * Exception thrown when read/write or create operations cannot be performed.
 */
class IOException extends RuntimeException {}
