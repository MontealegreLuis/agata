<?php
/**
 * Handles read/write and delete/rename operations on files
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\File;

use \SplFileInfo;

/**
 * Handles read/write and delete/rename operations on files.
 */
class File extends SplFileInfo
{
    /**
     * @return string
     *      The content of the file
     * @throws Mandragora_File_IOException
     *      Thrown if the file cannot be read
     */
    public function read()
    {
        if (!is_readable($this->getRealPath())) {
            throw new IOException("The file {$this->getRealPath()} cannot be read");
        }

        return file_get_contents($this->getRealPath());
    }

    /**
     * @param string $content
     *      The content of the file
     * @return boolean
     *      True if the file is successfully modified
     * @throws Mandragora_File_IOException
     *      Thrown if the file cannot be written
     */
    public function write($content)
    {
        if (!is_writable($this->getRealPath())) {

            throw new IOException("The file {$this->getRealPath()} cannot be written");
        }

        return file_put_contents($this->getRealPath(), $content);
    }

    /**
     * @return boolean
     *      True if the file is successfully deleted
     */
    public function delete()
    {
        return unlink($this->getRealPath());
    }

    /**
     * @param  string  $newName
     * @return boolean
     *      True if the file can be renamed
     */
    public function rename($newName)
    {
        $renameSucceed = rename($this->getRealPath(), (string) $newName);

        if (!$renameSucceed) {
            throw new IOException("The file {$this->getRealPath()} cannot be renamed");
        }

        return new self($newName);
    }

    /**
     * Check if this file exists
     *
     * @return boolean
     */
    public function exists()
    {
        return file_exists($this->getRealPath());
    }

    /**
     * Create an empty text file
     */
    public function create()
    {
        $file = $this->openFile('a');
        $file->fwrite('');
    }
}
