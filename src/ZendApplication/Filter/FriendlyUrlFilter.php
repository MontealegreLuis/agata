<?php
/**
 * Filter to transform a sentence into a friendly URL
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Filter;

use \Zend_Filter_Interface as Filter;
use \Zend_Filter_StringToLower as StringToLowerFilter;
use \Zend_Filter_Word_SeparatorToDash as SeparatorToDashFilter;

/**
 * Filter to transform a sentence into a friendly URL
 */
class FriendlyUrlFilter implements Filter
{
    /** @type StringToLowerFilter */
    protected $stringToLowerFilter;

    /** @type SeparatorToDashFilter */
    protected $separatorToDashFilter;

    /** @type AccentsAndSpecialSymbolsFilter */
    protected $accentsAndSpecialSymbolsFilter;

    /** @type string */
    protected $suffix;

    /** @type string */
    protected $prefix;

    /**
     * @param StringToLowerFilter            $stringToLowerFilter
     * @param SeparatorToDashFilter          $separatorToDashFilter
     * @param AccentsAndSpecialSymbolsFilter $accentsAndSpecialSymbolsFilter
     * @param string                         $suffix
     */
    public function __construct(
        StringToLowerFilter $stringToLowerFilter,
        SeparatorToDashFilter $separatorToDashFilter,
        AccentsAndSpecialSymbolsFilter $accentsAndSpecialSymbolsFilter,
        $suffix = '',
        $prefix = ''
    )
    {
        $this->stringToLowerFilter = $stringToLowerFilter;
        $this->separatorToDashFilter = $separatorToDashFilter;
        $this->accentsAndSpecialSymbolsFilter = $accentsAndSpecialSymbolsFilter;
        $this->suffix = $suffix;
        $this->prefix = $prefix;
    }

    /**
     * Apply the AccentsAndSpecialSymbols filter, lower case the string and finally change spaces
     * for dashes
     *
     * @param  string $value
     * @return string
     */
    public function filter($value)
    {
        /* Remove accents */
        $value = $this->accentsAndSpecialSymbolsFilter->filter($value);

        /* Transform to lower case */
        $value = $this->stringToLowerFilter->filter($value);

        /* Use dashes as separators */
        $value = $this->separatorToDashFilter->filter($value);

        /* Remove repeated dashes */
        $value = preg_replace('/[-]{2,}/', '-', $value);

        return $this->prefix . $value . $this->suffix;
    }
}
