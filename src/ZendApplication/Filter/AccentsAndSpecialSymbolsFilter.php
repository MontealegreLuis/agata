<?php
/**
 * Replaces accents and Ñ's with vouels and n, respectively, it also removes non alphanumeric
 * characters
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Filter;

use \Zend_Filter_Interface as Filter;
use \Zend_Filter_Alnum as AlphanumericFilter;

/**
 * Replaces accents and Ñ's with vouels and n, respectively, it also removes non alphanumeric
 * characters
 */
class AccentsAndSpecialSymbolsFilter implements Filter
{
    /** @type AlphanumericFilter */
    protected $alphanumericFilter;

    /**
     * @param AlphanumericFilter $alphanumericFilter
     */
    public function __construct(AlphanumericFilter $alphanumericFilter)
    {
        $this->alphanumericFilter = $alphanumericFilter;
    }

    /**
     * Filter accents, non alphanumeric symbols and the letter ñ (case insensitive)
     *
     * @param  string $value
     * @return string
     */
    public function filter($value)
    {
        /* First remove non alphanumeric symbols */
        $value = $this->alphanumericFilter->filter($value);

        /* Then the accents, the ñ and the ü */

        return $this->removeAccents($value);
    }

    /**
     * Replace the accents, the ñ and the ü for an equivalent in the english alphabet
     *
     * @param  string $value
     * @return string
     */
    protected function removeAccents($value)
    {
        $accents = [
            'á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ', 'ü', 'Ü'
        ];
        $replacements = [
            'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U', 'n', 'N', 'u', 'U'
        ];

        return str_replace($accents, $replacements, (string) $value);
    }
}
