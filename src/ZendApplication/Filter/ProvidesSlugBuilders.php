<?php
namespace ZendApplication\Filter;

use \Zend_Filter_StringToLower as StringToLowerFilter;
use \Zend_Filter_Word_SeparatorToDash as SeparatorToDashFilter;
use \Zend_Filter_Alnum as AlphanumericFilter;

trait ProvidesSlugBuilders
{
    /**
     * @param  string            $suffix
     * @param  string            $prefix
     * @return FriendlyUrlFilter
     */
    public function createSlugBuilder($suffix = '', $prefix = '')
    {
        return new FriendlyUrlFilter(
            new StringToLowerFilter(['encoding' => 'utf-8']),
            new SeparatorToDashFilter(),
            new AccentsAndSpecialSymbolsFilter(new AlphanumericFilter($alloWhiteSpaces = true)),
            $suffix,
            $prefix
        );
    }
}
