<?php
/**
 * Configure the view
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\View;

use \Zwig_View as View;
use \Zwig_Environment as Environment;
use \Twig_Loader_Filesystem as FilesystemLoader;
use \Zend_Controller_Action_HelperBroker as HelperBroker;
use \Zend_Controller_Action_Helper_ViewRenderer as ViewRenderer;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\FactoryInterface;
use \ZendApplication\Config\CanBeConfigured;

/**
 * Configure the view
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class TwigViewProvider extends CanBeConfigured implements FactoryInterface
{
    /**
     * @var Zwig_View
     */
    protected $view;

    /**
     * Configure view
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        $this->view = new View($this->options);

        if ($this->hasOption('doctype')) {
            $doctype = strtoupper($this->getOption('doctype'));
            $this->view->doctype()->setDoctype($doctype);
        }
        if ($this->hasOption('charset') && $this->view->doctype()->isHtml5()) {
            $this->view->headMeta()->setCharset($this->getOption('charset'));
        }
        if ($this->hasOption('contentType')) {
            $this->view->headMeta()
                       ->appendHttpEquiv('Content-Type', $this->getOption('contentType'));
        }

        foreach ($this->getOption('scriptPaths') as $path) {
            $this->view->addScriptPath($path);
        }

        $this->initZwig();
        $this->initBundleHelpers();

        $viewRenderer = new ViewRenderer($this->view, $this->getOption('viewRenderer'));
        HelperBroker::addHelper($viewRenderer);

        return $this->view;
    }

    /**
     * Initialize Zwig (Zend_View integration with Twig)
     */
    protected function initZwig()
    {
        $loader = new FilesystemLoader([]);
        $engine = new Environment($this->view, $loader, $this->getOption('twig'));

        $this->view->setEngine($engine);
    }

    /**
     * Initialize assets minification and compression helpers
     */
    protected function initBundleHelpers()
    {
        $options = $this->getOption('bundleHelpers');
        $command = sprintf($options['command'], escapeshellarg($options['yuiCompressorPath']));
        $this->view->getHelper('BundleScript')
                   ->setCacheDir($options['jsBundle']['cachePath'])
                   ->setDocRoot($options['jsBundle']['publicPath'])
                   ->setMinifyCommand($command)
                   ->setUrlPrefix($options['jsBundle']['publicPathPrefix']);
        $this->view->getHelper('BundleLink')
                   ->setCacheDir($options['cssBundle']['cachePath'])
                   ->setDocRoot($options['cssBundle']['publicPath'])
                   ->setMinifyCommand($command)
                   ->setUrlPrefix($options['cssBundle']['publicPathPrefix']);
    }
}
