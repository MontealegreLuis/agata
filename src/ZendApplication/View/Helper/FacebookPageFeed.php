<?php
namespace ZendApplication\View\Helper;

use \Zend\Feed\Reader\Reader;
use \Zend\Http\Client;
use \LimitIterator;

class FacebookPageFeed
{
    const FACEBOOK_FEED_URL = 'https://www.facebook.com/feeds/page.php?format=atom10&id=';

    public function facebookPageFeed($pageId)
    {
        $client = new Client('', ['sslcapath' => '/etc/ssl/certs']);
        Reader::setHttpClient($client);

        return new LimitIterator(Reader::import(static::FACEBOOK_FEED_URL . $pageId), 0, 5);
    }
}