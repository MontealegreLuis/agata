<?php
/**
 * Flash messenger view helper
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\View\Helper;

use \Zend_Controller_Action_HelperBroker as HelperBroker;

/**
 * Flash messenger view helper
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class FlashMessenger
{
    /**
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $flashMessenger;

    /**
     * Initialize the flashMessenger helper
     */
    public function __construct()
    {
        $this->flashMessenger = HelperBroker::getStaticHelper('FlashMessenger');
    }

    /**
     * @return FlashMessenger
     */
    public function flashMessenger()
    {
        return $this;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->flashMessenger->getMessages() ?: [];
    }

    /**
     * @param  string  $namespace
     * @return boolean
     */
    public function hasMessages($namespace)
    {
        return $this->flashMessenger->setNamespace($namespace)->hasMessages();
    }
}
