<?php
/**
 * Helper for bundling of all included JavaScript files into a single file
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\View\Helper;

use \ZendX_JQuery_View_Helper_JQuery as JQuery;
use \Zend_View_Interface as View;
use \Zend_Controller_Front as FrontController;
use \ZendApplication\File\File;
use \BadMethodCallException;

/**
 * Helper for bundling of all included javascripts into a single file
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 **/
class BundleScript extends JQuery
{
    /**
     * local Zend_View reference
     *
     * @var Zend_View_Interface
     */
    public $view;

    /**
     * Registry key for placeholder
     * @var string
     */
    protected $_regKey = 'ZendApplication\View\Helper\BundleScript';

    /**
     * Local reference to $view->baseUrl()
     *
     * @var string
     */
    protected $_baseUrl;

    /**
     * Directory in which to write bundled javascript
     *
     * @var string
     */
    protected $_cacheDir;

    /**
     * Directory in which to look for js files
     *
     * @var string
     */
    protected $_docRoot;

    /**
     * Path the generated bundle is publicly accessible under
     *
     * @var string
     */
    protected $_urlPrefix = "/javascripts";

    /**
     * External command used to minify javascript
     *
     * This command must write the bundled file to disk, STDOUT will be ignored.
     * The token ':filename'  must be present in command, this will be replaced
     * with the generated bundle name.
     *
     * @var string
     */
    protected $_minifyCommand;

    /**
     * @var string
     */
    protected $contents;

    public function setView(View $view)
    {
        $this->view = $view;
        $this->_baseUrl = $this->view->baseUrl();
    }

    /**
     * Proxies to Zend_View_Helper_HeadScript::headScript()
     *
     * @return BundlePhu_View_Helper_BundleScript
     */
    public function bundleScript($render = false)
    {
        if ($render) {
            return $this;
        } else {
            return parent::jQuery();
        }
    }

    /**
     * Sets the cache dir
     *
     * This is where the bundled files are written.
     *
     * @param  string                             $dir
     * @return BundlePhu_View_Helper_BundleScript
     */
    public function setCacheDir($dir)
    {
        $this->_cacheDir = $dir;

        return $this;
    }

    /**
     * DocRoot is the base directory on disk where the relative js files can be found.
     *
     * e.g.
     *
     * if $docRoot == '/var/www/foo' then '/js/foo.js' will be found in '/var/www/foo/js/foo.js'
     *
     * @param  string                             $docRoot
     * @return BundlePhu_View_Helper_BundleScript
     */
    public function setDocRoot($docRoot)
    {
        $this->_docRoot = $docRoot;

        return $this;
    }

    /**
     * Sets the URL prefix used for the generated script tag
     *
     * e.g. if $urlPrefix == '/javascripts' then '/javascripts/bundle_123fdfc3fe8ba8.js'
     * will be the src for the script tag.
     *
     * @param  string                             $prefix
     * @return BundlePhu_View_Helper_BundleScript
     */
    public function setUrlPrefix($prefix)
    {
        $this->_urlPrefix = $prefix;

        return $this;
    }

    /**
     * Command used to generate the minified output file
     *
     * The output of this command is not returned, it must write the output to
     * the generated filename for the bundle. The ':filename' token will be
     * replaced with the generated filename.
     *
     * @param  string                             $command Must contain :filename token
     * @return BundlePhu_View_Helper_BundleScript
     */
    public function setMinifyCommand($command)
    {
        $this->_minifyCommand = $command;

        return $this;
    }

    /**
     * Iterates over scripts, concatenating, optionally minifying,
     * optionally compressiong, and caching them.
     *
     * This detects updates to the source javascripts using filemtime.
     * A file with an mtime more recent than the mtime of the cached bundle will
     * invalidate the cached bundle.
     *
     * Modifications of captured scripts cannot be detected by this.
     * DONT USE DYNAMICALLY GENERATED CAPTURED SCRIPTS.
     *
     * @param  string                   $indent
     * @return void
     * @throws UnexpectedValueException if item has no src attribute or contains no captured source
     */
    public function toString()
    {
        $fc = FrontController::getInstance();
        $module = $fc->getRequest()->getModuleName();
        $controller = $fc->getRequest()->getControllerName();
        $action = $fc->getRequest()->getActionName();
        $hash = sprintf('%s-%s-%s', $module, $controller, $action);
        $cacheFilePath = "{$this->_docRoot}/{$this->_urlPrefix}/bundle-{$hash}.js";
        $cacheFile = new File($cacheFilePath);
        if (!$cacheFile->exists()) {
            $this->_getJsData();
            $this->_writeUncompressed($cacheFile, $this->contents);
        }
        $cacheTime = $cacheFile->getMTime();
        $urlPath = "{$this->_baseUrl}/{$this->_urlPrefix}/bundle-{$hash}.js?{$cacheTime}";
        $ret = PHP_EOL . '<script type="text/javascript" src="' . $urlPath . '"></script>';
        $onLoad = $this->formatJqueryOnload();
        if (!empty($onLoad)) {
            $ret .= PHP_EOL . "<script type=\"text/javascript\">\n//<![CDATA[\n";
            $ret .= "{$this->formatJqueryOnload()}\n//]]>\n</script>" . PHP_EOL;
        }

        return $ret;
    }

    /**
     * Iterates through the scripts and returning a concatenated result.
     *
     * Assumes the container is sorted prior to entry.
     *
     * @return string Concatenated javascripts
     */
    protected function _getJsData()
    {
        $jsFiles = array();
        if ($this->_container->isEnabled()) {
            $jsFiles[] = $this->_container->getLocalPath();
            if ($this->_container->uiIsEnabled()) {
                $jsFiles[] = $this->_container->getUiLocalPath();
            }
        }
        $jsFiles = array_merge($jsFiles, $this->_container->getJavascriptFiles());
        foreach ($jsFiles as $item) {
            $this->contents .= file_get_contents($this->_docRoot . $item) .  PHP_EOL;
        }
    }

    /**
     * Add document ready statement to the current registered onLoad actions
     *
     * @return string
     */
    protected function formatJqueryOnload()
    {
        $actions = $this->_container->getOnLoadActions();
        $onLoad = '';
        if (count($actions) > 0) {
            $onLoad = implode(PHP_EOL, $this->_container->getOnLoadActions());
            $onLoad = sprintf("$(document).ready(function() {\n    %s\n});", $onLoad);
        }

        return $onLoad;
    }

    /**
     * Writes uncompressed bundle to disk
     *
     * @param  string                 $cacheFile name of bundle file to write
     * @param  string                 $data      bundled JS data
     * @throws BadMethodCallException When neither _minifyCommand or _minifyCallback are defined
     * @return void
     */
    protected function _writeUncompressed(File $cacheFile, $data)
    {
        if (!empty($this->_minifyCommand)) {
            $temp = new File("{$this->_cacheDir}/{$cacheFile->getBasename()}");
            $temp->create();
            $temp->write($data);
            $command = str_replace(
                ':filename', escapeshellarg($cacheFile->getPathname()), $this->_minifyCommand
            );
            $command = str_replace(
                ':sourceFile', escapeshellarg($temp->getRealPath()), $command
            );
            $output = trim(`$command`);
            $temp->delete();

            return;
        }
        throw new BadMethodCallException("_minifyCommand is not defined.");
    }
}
