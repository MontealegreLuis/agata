<?php
/**
 * Google Analytics helper
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\View\Helper;

/**
 * Google Analytics helper
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class GoogleAnalytics
{
    /**
     * @param  string $trackerId
     * @return string
     */
    protected function getHtml($trackerId)
    {
        return "<script type=\"text/javascript\">
        var gaJsHost = ((\"https:\" == document.location.protocol)
            ? \"https://ssl.\"
            : \"http://www.\");
        document.write(unescape(\"%3Cscript src='\" + gaJsHost +
        \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));

        </script>
        <script type=\"text/javascript\">
             var pageTracker = _gat._getTracker(\"{$trackerId}\");
                 pageTracker._initData(); pageTracker._trackPageview();
        </script>";
    }

    /**
     * @param  string $trackerId
     * @return string
     */
    public function googleAnalytics($trackerId)
    {
        if (APPLICATION_ENV == 'production') {
            return $this->getHtml($trackerId);
        }

        return '';
    }
}
