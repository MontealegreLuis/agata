<?php
/**
 * Helper for bundling of all included stylesheets into a single file
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\View\Helper;

use \Zend_View_Helper_HeadLink as HeadLink;
use \Zend_View_Interface as View;
use \Zend_Controller_Front as FrontController;
use \BadMethodCallException;
use \ZendApplication\File\File;

/**
 * Helper for bundling of all included stylesheets into a single file
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based-Systems 2010-2014
 */
class BundleLink extends HeadLink
{
    /**
     * Local Zend_View reference
     *
     * @var Zend_View_Interface
     */
    public $view;

    /**
     * Registry key for placeholder
     *
     * @var string
     */
    protected $_regKey = 'ZendApplication\View\Helper\BundleLink';

    /**
     * Local reference to $view->baseUrl()
     *
     * @var string
     */
    protected $_baseUrl;

    /**
     * Directory in which to write bundled css
     *
     * @var string
     */
    protected $_cacheDir;

    /**
     * Directory in which to look for css files
     *
     * @var string
     */
    protected $_docRoot;

    /**
     * Path the generated bundle is publicly accessible under
     *
     * @var string
     */
    protected $_urlPrefix = "/stylesheets";

    /**
     * External command used to minify css
     *
     * This command must write the bundled file to disk, STDOUT will be ignored.
     * The token ':filename'  must be present in command, this will be replaced
     * with the generated bundle name.
     *
     * @var string
     */
    protected $_minifyCommand;

    /**
     * @var array
     */
    protected $contents = array(
        'screen, projection' => '', 'print' => '', 'IE' => ''
    );

    /**
     * Inject the local copy of the current Zend_View object
     *
     * @param  Zend_View_Interface $view
     * @return void
     */
    public function setView(View $view)
    {
        $this->view = $view;
        $this->_baseUrl = $this->view->baseUrl();
    }

    /**
     * Proxies to Zend_View_Helper_HeadLink::headLink()
     *
     * @return BundlePhu_View_Helper_BundleLink
     */
    public function bundleLink()
    {
        return parent::headLink();
    }

    /**
     * Sets the cache dir
     *
     * This is where the bundled files are written.
     *
     * @param  string                           $dir
     * @return BundlePhu_View_Helper_BundleLink
     */
    public function setCacheDir($dir)
    {
        $this->_cacheDir = $dir;

        return $this;
    }

    /**
     * DocRoot is the base directory on disk where the relative css files can be found.
     *
     * e.g.
     *
     * if $docRoot == '/var/www/foo' then '/css/foo.css' will be found in '/var/www/foo/css/foo.css'
     *
     * @param  string                           $docRoot
     * @return BundlePhu_View_Helper_BundleLink
     */
    public function setDocRoot($docRoot)
    {
        $this->_docRoot = $docRoot;

        return $this;
    }

    /**
     * Sets the URL prefix used for the generated link tag
     *
     * e.g. if $urlPrefix == '/stylesheets' then '/stylesheets/bundle_123fdfc3fe8ba8.css'
     * will be the src for the link tag.
     *
     * @param  string                           $prefix
     * @return BundlePhu_View_Helper_BundleLink
     */
    public function setUrlPrefix($prefix)
    {
        $this->_urlPrefix = $prefix;

        return $this;
    }

    /**
     * Command used to generate the minified output file
     *
     * The output of this command is not returned, it must write the output to
     * the generated filename for the bundle. The ':filename' token will be
     * replaced with the generated filename.
     *
     * @param  string                           $command Must contain :filename token
     * @return BundlePhu_View_Helper_BundleLink
     */
    public function setMinifyCommand($command)
    {
        $this->_minifyCommand = $command;

        return $this;
    }

    /**
     * Iterates over stylesheets, concatenating, optionally minifying,
     * optionally compressiong, and caching them.
     *
     * This detects updates to the source stylesheets using filemtime.
     * A file with an mtime more recent than the mtime of the cached bundle will
     * invalidate the cached bundle.
     *
     * Modifications of captured css cannot be detected by this.
     *
     * @param  string $indent
     * @return string
     */
    public function toString($render = true)
    {
        if (!$render) {
            return parent::toString();
        }

        $fc = FrontController::getInstance();
        $module = $fc->getRequest()->getModuleName();
        $controller = $fc->getRequest()->getControllerName();
        $action = $fc->getRequest()->getActionName();
        $link = '';
        $isCssBundled = false;
        foreach ($this->contents as $key => $headLink) {
            $fileKey = $key == 'screen, projection' ? 'screen' : $key;
            $hash = sprintf('%s-%s-%s-%s', $module, $controller, $action, $fileKey);
            $cacheFilePath = "{$this->_docRoot}/{$this->_urlPrefix}/bundle-{$hash}.css";
            $cacheFile = new File($cacheFilePath);
            if (!$cacheFile->exists()) {
                if (!$isCssBundled) {
                    $this->_setCssData();
                    $isCssBundled =  true;
                }
                $this->_writeUncompressed($cacheFile, $this->contents[$key]);
            }
            $urlPath = "{$this->_baseUrl}/{$this->_urlPrefix}/bundle-{$hash}.css";
            if ($cacheFile->exists()) {
                $urlPath .= "?{$cacheFile->getMTime()}";
                if (strpos($key, 'IE') === false) {
                    $link .= PHP_EOL . '<link href="' . $urlPath . '" media="' . $key . '" rel="stylesheet" type="text/css" />';
                } else {
                    $link .= PHP_EOL . '<!--[if ' . $key .']><link rel="stylesheet" type="text/css" media="screen, projection" href="' . $urlPath . '" /><![endif]-->';
                }
            }
        }

        return $link;
    }

    /**
     * @return void
     */
    protected function _setCssData()
    {
        foreach ($this as $item) {
            $href = $item->href;
            if ($this->_baseUrl && strpos($href, $this->_baseUrl) !== false) {
                $href =  substr($href, strlen($this->_baseUrl));
            }
            if ($item->conditionalStylesheet) {
                $this->contents[$item->conditionalStylesheet] .=
                    file_get_contents($this->_docRoot . $href) . PHP_EOL;
            } else {
                $this->contents[$item->media] .=
                    file_get_contents($this->_docRoot . $href) . PHP_EOL;
            }
        }
    }

    /**
     * Writes uncompressed bundle to disk
     *
     * @param  string                 $cacheFile name of bundle file to write
     * @param  string                 $data      bundled data
     * @throws BadMethodCallException When neither _minifyCommand or _minifyCallback are defined
     * @return void
     */
    protected function _writeUncompressed(File $cacheFile, $data)
    {
        if (!empty($this->_minifyCommand)) {
            if ($data !== '') {
                $temp = new File("{$this->_cacheDir}/{$cacheFile->getBasename()}");
                $temp->create();
                $temp->write($data);

                $command = str_replace(
                    ':filename', escapeshellarg($cacheFile->getPathname()), $this->_minifyCommand
                );
                $command = str_replace(
                    ':sourceFile', escapeshellarg($temp->getRealPath()), $command
                );
                $output = trim(`$command`);

                $temp->delete();
            }

            return;
        }
        throw new BadMethodCallException('Neither _minifyCommand or _minifyCallback are defined.');
    }
}
