<?php
/**
 * Helper to generate a tree of checkbox elements
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ZendApplication\View\Helper;

use \Zend_View_Helper_FormRadio as FormRadio;
use \Zend_View_Abstract as View;
use \Zend_Filter_PregReplace as PregReplaceFilter;

/**
 * Helper to generate a tree of checkbox elements
 *
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class FormTreeMulticheckbox extends FormRadio
{
    /**
     * Input type to use
     * @var string
     */
    protected $_inputType = 'checkbox';

    /**
     * Whether or not this element represents an array collection by default
     *
     * @var bool
     */
    protected $_isArray = true;

    /**
     * @var array
     */
    protected $info;

    /**
     * Generates a set of checkbox button elements.
     *
     * @access public
     *
     * @param string|array $name If a string, the element name.  If an
     * array, all other parameters are ignored, and the array elements
     * are extracted in place of added parameters.
     *
     * @param mixed $value The checkbox value to mark as 'checked'.
     *
     * @param array $options An array of key-value pairs where the array
     * key is the checkbox value, and the array value is the radio text.
     *
     * @param array|string $attribs Attributes added to each radio.
     *
     * @return string The radio buttons XHTML.
     */
    public function formTreeMultiCheckbox(
        $name, $value = null, $attribs = null, $options = null, $listsep = ''
    )
    {
        $this->info = $this->_getInfo($name, $value, $attribs, $options, $listsep);

        return $this->renderBranches($options, $this->info['id']);
    }

    /**
     * @param  array  $nodes
     * @param  string $elementId = ''
     * @return string
     */
    public function renderBranches($nodes, $elementId = '')
    {
        extract($this->info); // name, value, attribs, options, listsep, disable
        if (isset($attribs['elementPrefixPath'])) {
            unset($attribs['elementPrefixPath']);
        }
        // retrieve attributes for labels (prefixed with 'label_' or 'label')
        $labelAttribs = array();
        foreach ($attribs as $key => $val) {
            $tmp    = false;
            $keyLen = strlen($key);
            if ((6 < $keyLen) && (substr($key, 0, 6) == 'label_')) {
                $tmp = substr($key, 6);
            } elseif ((5 < $keyLen) && (substr($key, 0, 5) == 'label')) {
                $tmp = substr($key, 5);
            }
            if ($tmp) {
                // make sure first char is lowercase
                $tmp[0] = strtolower($tmp[0]);
                $labelAttribs[$tmp] = $val;
                unset($attribs[$key]);
            }
        }
        // the checbox values and labels
        $options = (array) $options;
        // build the element
        $xhtml = '';
        $list  = array();
        // should the name affect an array collection?
        $name = $this->view->escape($name);
        if ($this->_isArray && ('[]' != substr($name, -2))) {
            $name .= '[]';
        }
        // ensure value is an array to allow matching multiple times
        $value = (array) $value;
        // XHTML or HTML end tag?
        $endTag = ' />';
        if (($this->view instanceof View) && !$this->view->doctype()->isXhtml()) {
            $endTag= '>';
        }
        $pattern = @preg_match('/\pL/u', 'a')
                 ? '/[^\p{L}\p{N}\-\_]/u'    // Unicode
                 : '/[^a-zA-Z0-9\-\_]/';     // No Unicode
        $filter = new PregReplaceFilter($pattern, "");
        $xhtml = '';
        foreach ($nodes as $option => $labelAndChildren) {
            // Should the label be escaped?
            if ($escape) {
                $label = $this->view->escape($labelAndChildren['label']);
            }
            // is it disabled?
            $disabled = '';
            if (true === $disable) {
                $disabled = ' disabled="disabled"';
            } elseif (is_array($disable) && in_array($option, $disable)) {
                $disabled = ' disabled="disabled"';
            }
            // is it checked?
            $checked = '';
            if (in_array($option, $value)) {
                $checked = ' checked="checked"';
            }
            // generate ID
            $optId = $id . '-' . $filter->filter($option);
            $checkbox =  '<input type="' . $this->_inputType . '"'
                . ' name="' . $name . '"'
                . ' id="' . $optId . '"'
                . ' value="' . $this->view->escape($option) . '"'
                . $checked
                . $disabled
                . $this->_htmlAttribs($attribs)
                . $endTag
                . '<label'
                . $this->_htmlAttribs($labelAttribs) . ' for="' . $optId . '">'
                . $label
                . '</label>';

            // Append checkbox and its children
            $xhtml .= '<li>' . $checkbox
                   . $this->renderBranches($labelAndChildren['children'])
                   . '</li>';
        }
        $openUl = $elementId ? "\n<ul id=\"$elementId\">\n\t" : "\n<ul>\n\t";
        $xhtml = $xhtml ? $openUl . $xhtml . "\n</ul>" : $xhtml;

        return $xhtml;
    }
}
