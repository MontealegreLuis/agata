<?php
/**
 * View helper to show the accesskey attribute in links
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\View\Helper;

use \Zend_View_Helper_Navigation_HelperAbstract as NavigationHelper;
use \Zend_Navigation_Container as NavigationContainer;

/**
 * View helper to show the accesskey attribute in links
 *
 * @category   Library
 * @package    Mandragora
 * @subpackage View_Helper
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010
 * @version    SVN: $Id$
 */
class AccessibleMenu extends NavigationHelper
{
    /**
     * @param  Zend_Navigation_Page                  $page
     * @return Mandragora_View_Helper_AccessibleMenu
     */
    public function accessibleMenu()
    {
        return $this;
    }

    /**
     * @param  Zend_Navigation_Container $page = null
     * @return string
     */
    public function render(NavigationContainer $page = null)
    {
        $label = $page->getLabel();
        $title = $page->getTitle();
        $attribs = array(
            'id'     => $page->getId(),
            'title'  => $title,
            'accesskey' => $page->accesskey
        );
        if ($page->isActive()) {
            $attribs['class'] = 'nav-link active';
        } else {
            $attribs['class'] = 'nav-link';
        }
        if ($href = $page->getHref()) {
            $element = 'a';
            $attribs['href'] = $href;
            $attribs['target'] = $page->getTarget();
        } else {
            $element = 'span';
        }

        return '<' . $element . $this->attribs($attribs) . '>'
             . $this->view->escape($label)
             . '</' . $element . '>';
    }

    /**
     * @param  array  $attribs
     * @return string
     */
    public function attribs(array $attribs)
    {
        $xhtml = '';
        foreach ($attribs as $key => $value) {
            if ($value !== null) {
                $xhtml .= " $key=\"$value\"";
            }
        }

        return $xhtml;
    }
}
