<?php
/**
 * Base class for forms
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication\Form;

use \Zend_Form as ZendForm;

/**
 * Base class for forms
 */
abstract class Form extends ZendForm
{
    /**
     * @param  array $options
     * @return void
     */
    public function addHash(array $options)
    {
        $hash = $this->createElement('hash', 'csrf');
        $decorators = [['ViewScript', ['viewScript' => $options['viewScript']]]];
        $hash->setSalt($options['saltValue'])->setDecorators($decorators);
        $this->addElement($hash);
        $this->getDisplayGroup('data')->addElement($hash);
    }

    /**
     * Save the image file and rename it
     *
     * @param string $elementId
     * @param string $newName
     */
    protected function saveFile($elementId, $newName)
    {
        $file = $this->getElement($elementId);
        $file->addFilter('Rename', [
            'source' => '*',
            'target' => $newName,
            'overwrite' => true
        ]);
        $file->receive();
    }

    /**
     * Add error messages to the specified element.
     *
     * @param string $element
     * @param array  $errors
     */
    public function addErrorsToElement($element, array $errors)
    {
        $this->getElement($element)->setErrors($errors);
    }
}
