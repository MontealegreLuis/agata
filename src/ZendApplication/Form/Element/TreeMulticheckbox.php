<?php
/**
 * Form element that contains a hierarchical structure of checkboxes
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ZendApplication\Form\Element;

use \Zend_Form_Element_Multi as MultioptionsElement;

/**
 * Form element that contains a hierarchical structure of checkboxes
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class TreeMulticheckbox extends MultioptionsElement
{
    /**
     * @var string
     */
    public $helper = "formTreeMulticheckbox";

    /**
     * TreeMulticheckbox is an array of values by default
     *
     * @var bool
     */
    protected $_isArray = true;
}
