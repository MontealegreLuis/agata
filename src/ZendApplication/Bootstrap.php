<?php
/**
 * Bootstrap class for a ZF1 application
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace ZendApplication;

use \Zend_Application_Bootstrap_Bootstrap as ApplicationBootstrap;

/**
 * Bootstrap class for a ZF1 application
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based-Systems 2010-2014
 */
class Bootstrap extends ApplicationBootstrap {}
