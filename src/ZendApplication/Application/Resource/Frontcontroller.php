<?php
/**
 * Initialize both the dispatcher and the service manager
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ZendApplication\Application\Resource;

use \Zend_Application_Resource_ResourceAbstract as ApplicationResource;
use \Zend_Controller_Front as MvcFrontController;
use \Zend_Application_Exception as ApplicationException;
use \Zend_Controller_Action_HelperBroker as HelperBroker;
use \Zend\ServiceManager\ServiceManager;

/**
 * Initialize both the dispatcher and the service manager
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class Frontcontroller extends ApplicationResource
{
    /**
     * Initialize Front Controller
     *
     * @return Zend_Controller_Front
     */
    public function init()
    {
        $front = MvcFrontController::getInstance();
        $serviceManager = $this->getBootstrap()->getApplication()->getServiceManager();
        foreach ($this->getOptions() as $key => $value) {
            switch (strtolower($key)) {
                case 'dispatcher':
                    $this->initDispatcher($front, $value, $serviceManager);
                    break;
                case 'controllerdirectory':
                    $this->initControllersDirectories($front, $value);
                    break;
                case 'params':
                    $front->setParams($value);
                    break;
                case 'plugins':
                    $this->initPlugins($front, $value, $serviceManager);
                    break;
                case 'throwexceptions':
                    $front->throwExceptions((bool) $value);
                    break;
                case 'actionhelperpaths':
                    $this->initHelperPaths($value);
                    break;
                case 'defaultmodule':
                    $front->setDefaultModule($value);
                    break;
                default:
                    $front->setParam($key, $value);
                    break;
            }
        }
        $this->getBootstrap()->frontController = $front;

        return $front;
    }

    /**
     * @param  MvcFrontController   $front
     * @param  array                $options
     * @param  ServiceManager       $serviceManager
     * @throws ApplicationException
     */
    protected function initDispatcher(
        MvcFrontController $front, array $options, ServiceManager $serviceManager
    )
    {
        if (!isset($options['class'])) {
            throw new ApplicationException('You must specify the dispatcher class name');
        }
        $dispatchClass = $options['class'];
        $options['params'] = isset($options['params']) ? $options['params'] : [];

        $dispatcher = new $dispatchClass($options['params']);
        $dispatcher->setServiceManager($serviceManager);
        $front->setDispatcher($dispatcher);
    }

    /**
     * @param MvcFrontController $front
     * @param array              $directories
     */
    public function initControllersDirectories(MvcFrontController $front, array $directories)
    {
        foreach ($directories as $module => $directory) {
            $front->addControllerDirectory($directory, $module);
        }
    }

    /**
     * @param MvcFrontController $front
     * @param array              $plugins
     * @param ServiceManager     $serviceManager
     */
    protected function initPlugins(
        MvcFrontController $front, array $plugins, ServiceManager $serviceManager
    )
    {
        foreach ($plugins as $pluginKey) {
            $front->registerPlugin($serviceManager->get($pluginKey), $stackIndex = null);
        }
    }

    /**
     * @param array $paths
     */
    public function initHelperPaths(array $paths)
    {
        foreach ($paths as $helperPrefix => $helperPath) {
            HelperBroker::addPath($helperPath, $helperPrefix);
        }
    }
}
