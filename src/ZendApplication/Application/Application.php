<?php
/**
 * Custom Zend_Application that uses ZF2 ServiceManager
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ZendApplication\Application;

use \Zend_Application as ZendApplication;
use \Zend\ServiceManager\ServiceManager;
use \ZendApplication\ServiceManager\ApplicationKernel;

/**
 * Custom Zend_Application that uses ZF2 ServiceManager
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class Application extends ZendApplication
{
    /** @type ServiceManager */
    protected $serviceManager;

    /** @type ApplicationKernel */
    protected $kernel;

    /**
     * @param  ApplicationKernel $kernel
     * @return Application
     */
    public function setKernel(ApplicationKernel $kernel)
    {
        $this->kernel = $kernel;
        $this->serviceManager = new ServiceManager();
        $this->serviceManager->setAllowOverride(true);
        $this->kernel->configureServiceManager($this->serviceManager);

        return $this;
    }

    /**
     * @return \Zend\ServiceManager\ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @param  string             $key
     * @return ControllerProvider
     */
    public function getControllerProvider($key)
    {
        return $this->kernel->getControllerProvider($key);
    }
}
