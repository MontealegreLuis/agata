<?php
/**
 * Authentication result adpater for Zend_Auth
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityZendAuth\Identity;

use \Zend_Auth_Result as Result;
use \Security\Identity\AuthenticationResult;

/**
 * Authentication result adpater for Zend_Auth
 */
class ZendAuthResult extends Result implements AuthenticationResult {}
