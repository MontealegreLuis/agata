<?php
/**
 * The authentication adapter for handling users login/logout process
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace SecurityZendAuth\Identity;

use \Zend_Auth_Adapter_Interface as Adapter;
use \Zend_Auth_Adapter_Exception as AdapterException;
use \SecurityZendAuth\Identity\ZendAuthResult as AuthenticationResult;
use \Doctrine1\TableGateway\NoResultsFoundException;
use \Security\Identity\UserRepository;
use \Security\Identity\User;
use \Exception;

/**
 * The authentication adapter for handling users login/logout process
 */
class ZendAuthAdapter implements Adapter
{
    /** @type User */
    protected $user;

    /** @type UserRepository */
    protected $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param User $user
     */
    public function setCurrentUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return AuthenticationResult
     * @throws AdapterException
     */
    public function authenticate()
    {
        try {
            $identity = $this->repository->findOneByUsername($this->user->getUsername());
        } catch (NoResultsFoundException $e) {
            $identity = null;
        } catch (Exception $e) {
            throw new AdapterException($e->getMessage());
        }

        return $this->createAuthenticationResult($identity);
    }

    /**
     * Create the apropriate result object according to query result
     *
     * @param  User                 $identity
     * @return AuthenticationResult
     */
    protected function createAuthenticationResult($identity)
    {
        if (!$identity) {
            return $this->createResultIdentityNotFound();
        } elseif (!password_verify($this->user->getPassword(), $identity->getPassword())) {
            return $this->createResultInvalidCredentials();
        }

        return $this->createResultAuthenticationSucceed($identity);
    }

    /**
     * Create the result with identity not found details
     *
     * @return AuthenticationResult
     */
    protected function createResultIdentityNotFound()
    {
        return new AuthenticationResult(
            AuthenticationResult::FAILURE_IDENTITY_NOT_FOUND,
            null, ['username' => 'auth.userNotFound']
        );
    }

    /**
     * @params User                 $identity
     * @return AuthenticationResult
     */
    protected function createResultAuthenticationSucceed(User $identity)
    {
        $identity->clearPassword();

        return new AuthenticationResult(AuthenticationResult::SUCCESS, $identity);
    }

    /**
     * Create a result with invalid password details
     *
     * @return AuthenticationResult
     */
    protected function createResultInvalidCredentials()
    {
        return new AuthenticationResult(
            AuthenticationResult::FAILURE_CREDENTIAL_INVALID,
            null, ['password' => 'auth.wrongPassword']
        );
    }
}
