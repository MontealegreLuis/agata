<?php
/**
 * Firewall implementation using Zend_Auth
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace SecurityZendAuth\Identity;

use \Security\Identity\Firewall;
use \Security\Identity\User;
use \Zend_Auth as ZendAuthentication;

/**
 * Firewall implementation using Zend_Auth
 */
class ZendAuthFirewall implements Firewall
{
    /** @type ZendAuthentication */
    protected $auth;

    /** @type ZendAuthAdapter */
    protected $adapter;

    /**
     * @param ZendAuthentication $auth
     */
    public function __construct(ZendAuthentication $auth)
    {
        $this->auth = $auth;
    }

    public function setAdapter(ZendAuthAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @see \Security\Identity\Firewall::authenticate()
     */
    public function authenticate(User $user)
    {
        $this->adapter->setCurrentUser($user);

        return $this->auth->authenticate($this->adapter);
    }

    /**
     * @see \Security\Identity\Firewall::hasIdentity()
     */
    public function hasIdentity()
    {
        return $this->auth->hasIdentity();
    }

    /**
     * @see \Security\Identity\Firewall::getIdentity()
     */
    public function getIdentity()
    {
        return $this->auth->getIdentity();
    }

    /**
     * @see \Security\Identity\Firewall::clearIdentity()
     */
    public function clearIdentity()
    {
        $this->auth->clearIdentity();
    }
}
