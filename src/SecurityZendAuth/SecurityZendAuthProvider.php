<?php
/**
 * Service manager configuration for Security's Doctrine 1 persistence port
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityZendAuth;

use \Zend\ServiceManager\Config;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \SecurityZendAuth\Identity\ZendAuthFirewall;
use \SecurityZendAuth\Identity\ZendAuthAdapter;

/**
 * Service manager configuration for Security's Doctrine 1 persistence port
 */
class SecurityZendAuthProvider extends Config
{
    /**
     * Add configuration for the shared services and the security module.
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'security.firewall' => function(ServiceLocatorInterface $sl) {
                return new ZendAuthFirewall($sl->get('authentication'));
            },
            'user.auth.adapter' => function(ServiceLocatorInterface $sl) {
                return new ZendAuthAdapter($sl->get('user.repository'));
            },
        ];
    }
}
