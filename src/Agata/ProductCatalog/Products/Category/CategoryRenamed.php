<?php
/**
 * Event listener triggered by the CategoryRenamed event.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Category;

use \ProductCatalog\DomainEvents\DomainObjectChanged;
use \ZendApplication\Filter\FriendlyUrlFilter;

/**
 * Rename the category's image file
 */
class CategoryRenamed
{
    /** @type string */
    protected $path;

    /** @type FriendlyUrlFilter */
    protected $filter;

    /**
     * @param string            $path
     * @param FriendlyUrlFilter $filter
     */
    public function __construct($path, FriendlyUrlFilter $filter)
    {
        $this->path = $path;
        $this->filter = $filter;
    }

    /**
     * Handle the CategoryRenamed event
     *
     * @param DomainObjectChanged $event
     */
    public function __invoke(DomainObjectChanged $event)
    {
        $this->renameCategoryImage($event->getOriginalValue(), $event->getNewValue());
    }

    /**
     * @param string $oldName
     * @param string $newName
     */
    public function renameCategoryImage($oldName, $newName)
    {
        $oldImageName = $this->filter->filter($oldName);
        $newImageName = $this->filter->filter($newName);
        rename("{$this->path}/{$oldImageName}", "{$this->path}/{$newImageName}");
    }
}
