<?php
/**
 * Event listener triggered by the CategorySaved event that saves a category image and generates
 * its corresponding thumbnails.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Category;

use \Agata\ProductCatalog\Products\Images\ThumbnailsManager;
use \CatalogModule\Forms\CategoryForm;
use \ProductCatalog\Products\CategoryEvent;

/**
 * If present, save an uploaded category's image and generate its corresponding thumbnails.
 */
class CategoryImageUploaded
{
    /** @type string */
    protected $path;

    /** @var ThumbnailsManager */
    protected $thumbnails;

    /** @var CategoryForm */
    protected $categoryForm;

    /**
     * @param string            $path
     * @param ThumbnailsManager $thumbnails
     * @param CategoryForm      $categoryForm
     */
    public function __construct($path, ThumbnailsManager $thumbnails, CategoryForm $categoryForm)
    {
        $this->path = $path;
        $this->thumbnails = $thumbnails;
        $this->categoryForm = $categoryForm;
    }

    /**
     * @param CategoryEvent $event
     */
    public function __invoke(CategoryEvent $event)
    {
        $category = $event->getCategory();

        if ($this->categoryForm->hasImage()) {
            $this->categoryForm->saveImage("{$this->path}/{$category->image}");
            $this->thumbnails->setImageName("{$this->path}/{$category->image}");
            $this->thumbnails->generateThumbnails();
        }
    }
}
