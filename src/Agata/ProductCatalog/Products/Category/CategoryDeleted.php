<?php
/**
 * Event listener triggered by the CategoryDeleted event.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Agata\ProductCatalog\Products\Category;

use \ProductCatalog\Products\CategoryEvent;

/**
 * Event listener triggered by the CategoryDeleted event.
 *
 * It removes the category image from the file system if present.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
class CategoryDeleted
{
    /** @type string */
    protected $path;

    /**
     * @param string $path The path to the category image
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Remove the category's image from the file system if needed.
     *
     * @param CategoryEvent $event
     */
    public function __invoke(CategoryEvent $event)
    {
        $category = $event->getCategory();

        if (!$category->image) {
            return;
        }

        unlink("{$this->path}/{$category->image}");
    }
}
