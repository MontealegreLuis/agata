<?php
/**
 * Utility class to generate an image thumbnails.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Images;

use \PHPThumb\GD;

/**
 * Utility class to generate an image thumbnails.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class ThumbnailsManager
{
    /**
     * @var string
     */
    protected $imageName;

    /**
     * @var array
     */
    protected $descriptors;

    /**
     * @param array $descriptors
     */
    public function __construct(array $descriptors)
    {

        $this->descriptors = $descriptors;
    }

    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    public function generateThumbnails()
    {
        $thumbnails = new GD($this->imageName);
        foreach ($this->descriptors as $descriptor) {
            $thumbnails->resize($descriptor->getHeight(), $descriptor->getWidth());
            $imageName = pathinfo($this->imageName, PATHINFO_BASENAME);
            $thumbnails->save("{$descriptor->getPath()}/{$imageName}");
        }
    }
}
