<?php
/**
 * Delete a product's images
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Product;

use \ProductCatalog\Products\ProductEvent;

/**
 * Delete a product's images.
 */
class ProductDeleted
{
    /**
     * @var string
     */
    protected $paths;

    /**
     * @param array $paths
     */
    public function __construct(array $paths)
    {
        $this->paths = $paths;
    }

    /**
     * @param ProductEvent $event
     */
    public function __invoke(ProductEvent $event)
    {
        $product = $event->getProduct();

        if (!$product->image) {
            return;
        }

        foreach ($this->paths as $path) {
            unlink("{$path}/{$product->image}");
        }
    }
}
