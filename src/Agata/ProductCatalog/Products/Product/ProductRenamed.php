<?php
/**
 * Rename a product's images when its model changes.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Product;

use \ProductCatalog\DomainEvents\DomainObjectChanged;
use \ZendApplication\Filter\FriendlyUrlFilter;

/**
 * Rename a product's images when its model changes.
 */
class ProductRenamed
{
    /**
     * @var array
     */
    protected $paths;

    /** @type FriendlyUrlFilter */
    protected $filter;

    /**
     * @param array $paths {
     *     @type string $original  Path to the original product image
     *     @type string $medium    Path to the medium size product image
     *     @type string $thumbnail Path to the thumbnail product image
     * }
     * @param FriendlyUrlFilter $filter
     */
    public function __construct(array $paths, FriendlyUrlFilter $filter)
    {
        $this->paths = $paths;
        $this->filter = $filter;
    }

    /**
     * Rename the image files
     */
    public function __invoke(DomainObjectChanged $event)
    {
        $this->renameImages($event->getOriginalValue(), $event->getNewValue());
    }

    /**
     * @param string $oldName
     * @param string $newName
     */
    protected function renameImages($oldName, $newName)
    {
        $oldImageName = $this->filter->filter($oldName);
        $newImageName = $this->filter->filter($newName);
        foreach ($this->paths as $path) {
            rename("$path/$oldImageName", "$path/$newImageName");
        }
    }
}
