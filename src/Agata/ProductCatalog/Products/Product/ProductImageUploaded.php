<?php
/**
 * Save a product's image and generate the corresponding thumbnails.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Product;

use \Agata\ProductCatalog\Products\Images\ThumbnailsManager;
use \CatalogModule\Forms\ProductForm;
use \ProductCatalog\Products\ProductEvent;

/**
 * Save a product's image and generate the corresponding thumbnails.
 */
class ProductImageUploaded
{
    /** @type string */
    protected $path;

    /** @type ThumbnailsManager */
    protected $thumbnails;

    /** @type ProductForm */
    protected $productForm;

    /**
     * @param string            $path
     * @param ThumbnailsManager $thumbnails
     * @param ProductForm       $productForm
     */
    public function __construct(
        $path, ThumbnailsManager $thumbnails, ProductForm $productForm
    )
    {
        $this->path = $path;
        $this->thumbnails = $thumbnails;
        $this->productForm = $productForm;
    }

    /**
     * @param ProductEvent $event
     */
    public function __invoke(ProductEvent $event)
    {
        $product = $event->getProduct();
        if ($this->productForm->hasImage()) {
            $this->productForm->saveImage("{$this->path}/{$product->image}");
            $this->thumbnails->setImageName("{$this->path}/{$product->image}");
            $this->thumbnails->generateThumbnails();
        }
    }
}
