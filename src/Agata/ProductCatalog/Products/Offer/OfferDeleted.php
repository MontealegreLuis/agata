<?php
/**
 * Delete an offer's images
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Offer;

use \ProductCatalog\Products\OfferEvent;

/**
 * Delete an offer's images.
 */
class OfferDeleted
{
    /** @type string */
    protected $path;

    /**
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @param OfferEvent $event
     */
    public function __invoke(OfferEvent $event)
    {
        $offer = $event->getOffer();

        if (!$offer->image) {
            return;
        }

        unlink("{$this->path}/{$offer->image}");
    }
}
