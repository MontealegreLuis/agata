<?php
/**
 * Event listener triggered by the OfferSaved event that saves an offer image
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ProductCatalog\Products\Offer;

use \CatalogModule\Forms\OfferForm;
use \ProductCatalog\Products\OfferEvent;

/**
 * If present, save an uploaded offer's image
 */
class OfferImageUploaded
{
    /** @type string */
    protected $path;

    /** @type OfferForm */
    protected $offerForm;

    /**
     * @param string    $path
     * @param OfferForm $offerForm
     */
    public function __construct($path, OfferForm $offerForm)
    {
        $this->path = $path;
        $this->offerForm = $offerForm;
    }

    /**
     * @param OfferEvent $event
     */
    public function __invoke(OfferEvent $event)
    {
        $offer = $event->getOffer();

        if ($this->offerForm->hasImage()) {
            $this->offerForm->saveImage("{$this->path}/{$offer->image}");
        }
    }
}
