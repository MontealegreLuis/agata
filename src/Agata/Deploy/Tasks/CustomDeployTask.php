<?php
namespace Agata\Deploy\Tasks;

use \Rocketeer\Traits\Task;
use \RuntimeException;
use \InvalidArgumentException;

class CustomDeployTask extends Task
{
    /**
     * @see \Rocketeer\Traits\Task::getSlug()
     */
    public function getSlug()
    {
        return 'commands-deploy';
    }

    /**
     * @see \Rocketeer\Traits\Task::getDescription()
     */
    public function getDescription()
    {
        return 'Deploy with user-defined shell commands';
    }

    public function execute()
    {
        // Setup if necessary
        if (!$this->isSetup()) {
            $this->command->error('Server is not ready, running Setup task');
            $this->executeTask('Setup');
        }

        // Update current release
        $release = $this->releasesManager->updateCurrentRelease();
        $this->cloneRepository();

        if (!($commands = $this->getOption('commands'))) {
            throw new RuntimeException('You must provide a path to the commands to be run during deployment');
        }

        if (!is_readable($commands)) {
            throw new InvalidArgumentException("File '$commands' does not exist or cannot be read");
        }

        // Run halting commands
        $commands = require $commands;
        foreach ($commands as $command) {
            $this->command->info("Now runnning '$command'");

            if (!$this->runForCurrentRelease($command)) {
                return $this->cancel();
            }
        }

        // Set permissions
        $this->setApplicationPermissions();

        // Synchronize shared folders and files
        $this->syncSharedFolders();

        $this->updateSymlink();

        $this->command->info('Successfully deployed release '.$release);
    }

    /**
     * Cancel deploy
     *
     * @return false
     */
    protected function cancel()
    {
        $this->executeTask('Rollback');

        return false;
    }

    /**
     * Set permissions for the folders used by the application
     *
     * @return void
     */
    protected function setApplicationPermissions()
    {
        $files = (array) $this->rocketeer->getOption('remote.permissions.files');
        foreach ($files as $file) {
            $this->setPermissions($file);
        }
    }
}
