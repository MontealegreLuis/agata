<?php
namespace Agata\Deploy\Commands;

use Symfony\Component\Console\Input\InputOption;
use Rocketeer\Commands\AbstractDeployCommand;

/**
 * Runs the Deploy task and then cleans up deprecated releases
 */
class CustomDeployCommand extends AbstractDeployCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'deploy:deploy-custom';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy the website using a custom set of commands.';

    /**
     * Execute the tasks
     *
     * @return array
     */
    public function fire()
    {
        return $this->fireTasksQueue(array(
            'Agata\Deploy\Tasks\CustomDeployTask',
            'Rocketeer\Tasks\Cleanup',
        ));
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array_merge(parent::getOptions(), array(
            array('commands', 'm', InputOption::VALUE_REQUIRED, 'Path to the commands to be run remotely during deploy'),
            array('clean-all', null, InputOption::VALUE_NONE, 'Cleanup all but the current release on deploy'),
       ));
    }
}
