<?php
/**
 * Plugin to select appropriate language
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Agata\ZendApplication\Controller\Plugin;

use \Zend_Controller_Plugin_Abstract as Plugin;
use \Zend_Controller_Request_Abstract as Request;
use \Zend_Locale as Locale;
use \Zend_Translate as Translator;
use \Zend_Registry as Registry;
use \Zend_Form as Form;
use \Zend_Validate_Abstract as Validation;
use \Zend_Cache_Core as Cache;

/**
 * Plugin to select appropriate language
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class TranslatorPlugin extends Plugin
{
    /**
     * @var Locale
     */
    protected $locale;

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @param Locale $locale
     */
    public function __construct(Locale $locale, Cache $cache)
    {
        $this->locale = $locale;
        $this->cache = $cache;
    }

    /**
     * @param  Request $request
     * @return void
     */
    public function preDispatch(Request $request)
    {
        $languagePath = sprintf(
            '%s/%s/application.php', $this->locale->getLanguage(), $this->locale->getRegion()
        );
        $translator = new Translator(
            'array',
            require "application/configs/languages/{$languagePath}",
            $this->locale->getLanguage()
        );
        Translator::setCache($this->cache);
        Registry::set('Zend_Translate', $translator);
        Form::setDefaultTranslator($translator);
        Validation::setDefaultTranslator($translator);
    }
}
