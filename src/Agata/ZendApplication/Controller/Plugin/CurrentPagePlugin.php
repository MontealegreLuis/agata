<?php
/**
 * Passes the current URL's path to the view
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Agata\ZendApplication\Controller\Plugin;

use \Zend_Controller_Plugin_Abstract as Plugin;
use \Zend_Controller_Request_Abstract as Request;
use \Zend_View_Abstract as View;

/**
 * Passes the current URL's path to the view
 *
 * @package    Mandragora
 * @subpackage Controller_Plugin
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class CurrentPagePlugin extends Plugin
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @param View $view
     */
    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * @param  Request $request
     * @return void
     */
    public function preDispatch(Request $request)
    {
        $this->view->currentPage = $this->view->serverUrl($request->getServer('REQUEST_URI'));
    }
}
