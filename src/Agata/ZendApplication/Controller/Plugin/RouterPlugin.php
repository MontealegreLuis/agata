<?php
/**
 * Select the custom routes according to the current request
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Agata\ZendApplication\Controller\Plugin;

use \Zend_Controller_Plugin_Abstract as Plugin;
use \Zend_Controller_Request_Abstract as Request;
use \Zend_Locale as Locale;
use \Zend_Controller_Front as FrontController;
use \Zend_Config as Config;
use \Zend_Translate as Translator;
use \Zend_Controller_Router_Route as Router;

/**
 * Select the custom routes according to the current request
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class RouterPlugin extends Plugin
{
    /**
     * @var Locale
     */
    protected $locale;

    /**
     * @param Locale $locale
     */
    public function __construct(Locale $locale)
    {
        $this->locale = $locale;
    }

    /**
     * Select the custom routes accordingly
     *
     * @param  Request $request
     * @return void
     */
    public function routeStartup(Request $request)
    {
        $frontController = FrontController::getInstance();
        $frontController->getRouter()
                        ->addConfig(new Config(require 'application/configs/routing.php'));

        $configPath = sprintf(
            'application/configs/languages/%s/%s/routes.php',
            $this->locale->getLanguage(), $this->locale->getRegion()
        );
        $translate = new Translator('array', require $configPath, $this->locale);
        Router::setDefaultTranslator($translate);
    }

    /**
     * Unset translations, use of parameters becomes transparent in controllers
     *
     * @see Zend_Controller_Plugin_Abstract::routeShutdown()
     */
    public function routeShutdown(Request $request)
    {
        $translate = Router::getDefaultTranslator();
        $messages = $translate->getMessages();
        $params = $request->getParams();
        foreach ($params as $key => $value) {
            // Pass non translated keys to the request
            if (($originalKey = array_search($key, $messages))) {
                $request->setParam($originalKey, $value);
                $request->setParam($key, null);
            }
        }
    }
}
