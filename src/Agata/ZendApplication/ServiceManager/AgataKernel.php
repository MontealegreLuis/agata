<?php
/**
 * Application's services and controllers providers configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ZendApplication\ServiceManager;

use \Zend\ServiceManager\ServiceManager;
use \ZendApplication\ServiceManager\ApplicationKernel;
use \ZendApplication\ServiceManager\ServicesProvider;
use \Agata\ZendApplication\ServiceManager\PluginsProvider;
use \CatalogModule\ServiceManager\CatalogModuleProvider;
use \CatalogModule\ServiceManager\CategoryControllerProvider;
use \CatalogModule\ServiceManager\ProductControllerProvider;
use \CatalogModule\ServiceManager\TrademarkControllerProvider;
use \CatalogModule\ServiceManager\OfferControllerProvider;
use \CatalogDoctrine1\CatalogDoctrine1Provider;
use \DefaultModule\ServiceManager\ErrorControllerProvider;
use \DefaultModule\ServiceManager\IndexControllerProvider;
use \DefaultModule\ServiceManager\ProductControllerProvider as DefaultProductControllerProvider;
use \SecurityModule\ServiceManager\SecurityModuleProvider;
use \SecurityModule\ServiceManager\UserControllerProvider;
use \SecurityDoctrine1\SecurityDoctrine1Provider;
use \SecurityZendAuth\SecurityZendAuthProvider;
use \DefaultModule\ServiceManager\DefaultModuleProvider;
use \Doctrine1\Doctrine1Provider;

/**
 * Application's services and controllers providers configuration
 */
class AgataKernel extends ApplicationKernel
{
    /**
     * Initialize service and controllers providers
     */
    public function __construct()
    {
        $this->serviceProviders = [
            new ServicesProvider(),
            new Doctrine1Provider(),
            new PluginsProvider(),
            new DefaultModuleProvider(),
            new CatalogModuleProvider(),
            new CatalogDoctrine1Provider(),
            new SecurityModuleProvider(),
            new SecurityDoctrine1Provider(),
            new SecurityZendAuthProvider(),
            new ApplicationProvider(), // This overrides/initializes keys in previous providers
        ];
        $this->controllerProviders = [
            new CategoryControllerProvider(),
            new ProductControllerProvider(),
            new TrademarkControllerProvider(),
            new OfferControllerProvider(),
            new ErrorControllerProvider(),
            new IndexControllerProvider(),
            new DefaultProductControllerProvider(),
            new UserControllerProvider(),
        ];
    }
}
