<?php
/**
 * Application's service manager configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ZendApplication\ServiceManager;

use \Zend\ServiceManager\Config;
use \Zend\ServiceManager\ServiceManager;

/**
 * Application's service manager configuration
 */
class ApplicationProvider extends Config
{
    /**
     * Add configuration for the shared services and the security module.
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->configureSharedServices();
        $this->configureSecurityModule();
        $this->configureCatalogModule();
        $this->configureDefaultModule();
    }

    /**
     * Register shared services configuration paths
     */
    protected function configureSharedServices()
    {
        $this->config['services'] = [
            'view.configuration' => 'application/configs/services/view.php',
            'cache.configuration' => 'application/configs/services/cache-manager.php',
            'locale.configuration' => 'application/configs/services/locale.php',
            'session.configuration' => 'application/configs/services/session.php',
            'authentication.configuration' => 'application/configs/services/authentication.php',
            'connection.configuration' => 'application/configs/services/doctrine.php',
            'mailer.configuration' => 'application/configs/services/mail.php',
            'logger.configuration' => 'application/configs/services/logger.php',
            'paginator.configuration' => 'application/configs/services/pagination.php',
        ];
    }

    /**
     * Setup security module services
     */
    protected function configureSecurityModule()
    {
        $this->config['services']['user.navigation.configuration'] = 'application/configs/navigation/user.php';
    }

    /**
     * Setup catalog module services
     */
    protected function configureCatalogModule()
    {
        $services = [
            'catalog.menu.configuration' => 'application/configs/navigation/menus/catalog.php',
            'category.navigation.configuration' => 'application/configs/navigation/category.php',
            'category.images.path' => 'uploads/categories',
            'category.actions.configuration' => 'application/configs/navigation/actions/catalog/category.php',
            'trademark.navigation.configuration' => 'application/configs/navigation/trademark.php',
            'trademark.actions.configuration' => 'application/configs/navigation/actions/catalog/trademark.php',
            'product.navigation.configuration' => 'application/configs/navigation/product.php',
            'product.actions.configuration' => 'application/configs/navigation/actions/catalog/product.php',
            'offer.actions.configuration' => 'application/configs/navigation/actions/catalog/offer.php',
            'offer.navigation.configuration' => 'application/configs/navigation/offer.php',
        ];
        $this->config['services'] = array_merge($this->config['services'], $services);
    }

    /**
     * Setup default module services
     */
    protected function configureDefaultModule()
    {
        $services = [
            'product.breadcrumbs.configuration' => require 'application/configs/navigation/breadcrumbs/default.php',
            'default.menu.configuration' => require 'application/configs/navigation/menus/default.php',
        ];
        $this->config['services'] = array_merge($this->config['services'], $services);
    }
}
