<?php
/**
 * Service manager configuration for controller plugins
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Agata\ZendApplication\ServiceManager;

use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\Config;
use \Agata\ZendApplication\Controller\Plugin\RouterPlugin;
use \Agata\ZendApplication\Controller\Plugin\TranslatorPlugin;
use \Agata\ZendApplication\Controller\Plugin\CurrentPagePlugin;

/**
 * Service manager configuration for controller plugins
 */
class PluginsProvider extends Config
{
    /**
     * Add the configuration for the application's controller plugins
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'router.plugin' => function(ServiceLocatorInterface $sl) {
                return new RouterPlugin($sl->get('locale'));
            },
            'language.plugin' => function(ServiceLocatorInterface $sl) {
                return new TranslatorPlugin(
                    $sl->get('locale'), $sl->get('cache.manager')->getCache('default')
                );
            },
            'page.plugin' => function(ServiceLocatorInterface $sl) {
                return new CurrentPagePlugin($sl->get('view'));
            },
        ];
    }
}
