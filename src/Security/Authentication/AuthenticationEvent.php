<?php
namespace Security\Authentication;

use \Zend\EventManager\Event;
use \Security\Identity\AuthenticationResult;

class AuthenticationEvent extends Event
{
    protected $authenticationResult;

    public function setAuthenticationResult(AuthenticationResult $result)
    {
        $this->authenticationResult = $result;
    }

    public function getAuthenticationResult()
    {
        return $this->authenticationResult;
    }
}
