<?php
/**
 * DTO that holds the username and raw password used to authenticate a user
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Security\Authentication;

/**
 * DTO that holds the username and raw password used to authenticate a user
 */
class AuthenticateUserRequest
{
    /** @type string */
    public $username;

    /** @var string */
    public $password;

    /**
     * Gives username and password initial values
     *
     * @param string[] $values
     */
    public function __construct(array $values)
    {
        $this->username = $values['username'];
        $this->password = $values['password'];
    }
}
