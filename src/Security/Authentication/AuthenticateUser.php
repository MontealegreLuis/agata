<?php
/**
 * Use case to authenticate a user.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Security\Authentication;

use \Zend\EventManager\ProvidesEvents;
use \Security\Identity\Firewall;
use \Security\Identity\User;

/**
 * Use case to authenticate a user.
 *
 * It triggers two events one for a successful authentication and another for a failed one.
 */
class AuthenticateUser
{
    use ProvidesEvents;

    /** @type Firewall*/
    protected $firewall;

    /** @type AuthenticationEvent */
    protected $authenticationEvent;

    /**
     * @param Firewall            $firewall
     * @param AuthenticationEvent $event
     */
    public function __construct(Firewall $firewall, AuthenticationEvent $event)
    {
        $this->firewall = $firewall;
        $this->authenticationEvent = $event;
    }

    /**
     * It uses the security firewall to authenticate the user with the given username and raw
     * password.
     *
     * If the validation result returned by the firewall is valid the AuthenticationSucceded event
     * is triggered, otherwise the AuthenticationFailed event is triggered.
     *
     * @see   \Security\Identity\Firewall::authenticate
     *        For more information about the authentication process
     * @param AuthenticateUserRequest $request
     */
    public function login(AuthenticateUserRequest $request)
    {
        $user = new User($request->username, $request->password);

        $result = $this->firewall->authenticate($user);

        $this->authenticationEvent->setAuthenticationResult($result);
        $this->authenticationEvent->setName('AuthenticationFailed');
        if ($result->isValid()) {
            $this->authenticationEvent->setName('AuthenticationSucceeded');
        }

        $this->getEventManager()->trigger($this->authenticationEvent);
    }
}
