<?php
/**
 * Check if a user already has an identity
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Security\Authentication;

use \Security\Identity\Firewall;
use \Zend\EventManager\ProvidesEvents;

/**
 * Check if a user already has an identity
 */
class CheckUserIdentity
{
    use ProvidesEvents;

    /** @type Firewall */
    protected $firewall;

    /**
     * @param Firewall $firewall
     */
    public function __construct(Firewall $firewall)
    {
        $this->firewall = $firewall;
    }

    /**
     * Check if a user already have an identity.
     *
     * If the user already have an identity, the UserAlreadyLogged event is triggered.
     */
    public function isUserLogged()
    {
        if ($this->firewall->hasIdentity()) {
            $this->getEventManager()->trigger('UserAlreadyLogged');
        }
    }
}
