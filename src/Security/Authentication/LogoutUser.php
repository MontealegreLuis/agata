<?php
/**
 * Closes the current's user session
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Security\Authentication;

use \Security\Identity\Firewall;
use \Zend\EventManager\ProvidesEvents;

/**
 * Closes the current's user session
 */
class LogoutUser
{
    use ProvidesEvents;

    /** @type Firewall */
    protected $firewall;

    /**
     * @param Firewall $firewall
     */
    public function __construct(Firewall $firewall)
    {
        $this->firewall = $firewall;
    }

    /**
     * Closes the current's user session.
     *
     * It also triggers the UserLogout event, after closing the session
     */
    public function logoutUser()
    {
        $this->firewall->clearIdentity();
        $this->getEventManager()->trigger('UserLogout');
    }
}
