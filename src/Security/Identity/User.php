<?php
/**
 * User model
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Security\Identity;

/**
 * User model
 *
 * @property integer $id
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
class User
{
    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $roleName;

    /**
     * @param string $username
     * @param string $password
     * @param string $roleName
     */
    public function __construct($username, $password, $roleName = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->roleName = $roleName;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return void
     */
    public function clearPassword()
    {
        $this->password = null;
    }
}
