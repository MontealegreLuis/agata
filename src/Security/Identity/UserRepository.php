<?php
namespace Security\Identity;

interface UserRepository
{
    /**
     * @param  string
     * @return User
     */
    public function findOneByUsername($username);
}
