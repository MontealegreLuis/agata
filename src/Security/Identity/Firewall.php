<?php
/**
 * Firewall to authenticate users
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Security\Identity;

/**
 * Firewall to authenticate users
 */
interface Firewall
{
    /**
     * @param  User                 $user
     * @return AuthenticationResult
     */
    public function authenticate(User $user);

    /**
     * @return boolean
     */
    public function hasIdentity();

    /**
     * @return User
     */
    public function getIdentity();

    /**
     * @return void
     */
    public function clearIdentity();
}
