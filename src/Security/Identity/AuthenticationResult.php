<?php
namespace Security\Identity;

interface AuthenticationResult
{
    /**
     * @return boolean
     */
    public function isValid();

    /**
     * @return array
     */
    public function getMessages();
}
