<?php
/**
 * Controller to save a category's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\CategoryForm;
use \ProductCatalog\Catalog\Categories\SaveCategory;
use \ProductCatalog\Catalog\Categories\SaveCategoryRequest;
use \ProductCatalog\Products\CategoryEvent;

/**
 * Controller to save a category's information
 */
class SaveCategoryController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type SaveCategory */
    protected $useCase;

    /** @type CategoryForm */
    protected $categoryForm;

    /**
     * @param SaveCategory $saveCategory
     */
    public function setUseCase(SaveCategory $saveCategory)
    {
        $this->useCase = $saveCategory;
    }

    /**
     * @param CategoryForm $categoryForm
     */
    public function setCategoryForm(CategoryForm $categoryForm)
    {
        $this->categoryForm = $categoryForm;
    }

    /**
     * Save the information of a new category
     */
    public function saveAction()
    {
        $this->categoryForm->setAction($this->getUrl('category.save'));
        $this->view->categoryForm = $this->categoryForm->prepareForCreating();

        if (!$this->categoryForm->isValid($this->post())) {
            return $this->onCategoryFormValidationError();
        }

        $this->useCase->saveCategory(new SaveCategoryRequest($this->categoryForm->getValues()));
    }

    /**
     * Show the form validation errors
     */
    protected function onCategoryFormValidationError()
    {
        $this->renderScript('category/create.twig');
    }

    /**
     * Redirect to the show category page after saving the category information.
     *
     * @param CategoryEvent $event
     */
    public function onCategorySaved(CategoryEvent $event)
    {
        $this->flash('success')->addMessage('category.created');
        $this->redirectTo($this->getUrl('category.show', ['id' => $event->getCategory()->id]));
    }

    /**
     * Use this controller as listener of the CategorySaved event
     *
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('CategorySaved', [$this, 'onCategorySaved']);
    }
}
