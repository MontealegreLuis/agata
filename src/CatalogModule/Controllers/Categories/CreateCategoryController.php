<?php
/**
 * Controller to show the form to save a category's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\CategoryForm;

/**
 * Controller to show the form to save a category's information
 */
class CreateCategoryController extends ControllerAction
{
    use ProvidesUrls;

    /** @type CategoryForm */
    protected $categoryForm;

    /**
     * @param CategoryForm $categoryForm
     */
    public function setCategoryForm(CategoryForm $categoryForm)
    {
        $this->categoryForm = $categoryForm;
    }

    /**
     * Show the form to capture a new category's information.
     */
    public function createAction()
    {
        $this->categoryForm->setAction($this->getUrl('category.save'));
        $this->view->categoryForm = $this->categoryForm->prepareForCreating();
    }
}
