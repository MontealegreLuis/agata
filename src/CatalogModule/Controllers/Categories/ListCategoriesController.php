<?php
/**
 * Controller to show the list of available categories
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Categories\GetAllCategories;

/**
 * Controller to show the list of available categories
 */
class ListCategoriesController extends ControllerAction
{
    /** @type GetAllCategories */
    protected $useCase;

    /**
     * @param GetAllCategories $getAllCategories
     */
    public function setUseCase(GetAllCategories $getAllCategories)
    {
        $this->useCase = $getAllCategories;
    }

    /**
     * Get all the available categories
     */
    public function listAction()
    {
        $response = $this->useCase->getAllCategories();
        $this->view->items = $response->categories;
    }
}
