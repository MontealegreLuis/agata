<?php
/**
 * Controller to show a given category
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Categories\GetCategory;
use \ProductCatalog\Catalog\Categories\CategoryRequest;

/**
 * Controller to show a given category
 */
class ShowCategoryController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type GetCategory */
    protected $useCase;

    /**
     * @param GetCategory $getCategory
     */
    public function setUseCase(GetCategory $getCategory)
    {
        $this->useCase = $getCategory;
    }

    /**
     * Get all the available categories
     */
    public function showAction()
    {
        $response = $this->useCase->getCategory(new CategoryRequest($this->param('id')));
        $this->view->category = $response->category;
    }

    /**
     * Go to the list of categories and show a proper warning message
     */
    public function onCategoryNotFound()
    {
        $this->flash('error')->addMessage('category.not.found');
        $this->redirectTo($this->getUrl('list.category', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('CategoryNotFound', [$this, 'onCategoryNotFound']);
    }
}
