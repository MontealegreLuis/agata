<?php
/**
 * Controller to delete a category's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Categories\DeleteCategory;
use \ProductCatalog\Catalog\Categories\CategoryRequest;

/**
 * Controller to delete a category's information
 */
class DeleteCategoryController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type DeleteCategory */
    protected $useCase;

    /**
     * @param DeleteCategory $deleteCategory
     */
    public function setUseCase(DeleteCategory $deleteCategory)
    {
        $this->useCase = $deleteCategory;
    }

    /**
     * Delete a given category
     */
    public function deleteAction()
    {
        $this->useCase->deleteCategory(new CategoryRequest($this->param('id')));
    }

    /**
     * Redirect to the list categories page after deleting the category
     */
    public function onCategoryDeleted()
    {
        $this->flash('success')->addMessage('category.deleted');
        $this->redirectTo($this->getUrl('category.list', [$this->view->translate('page') => 1]));
    }

    /**
     * Go to the list of categories and show a proper warning message
     */
    public function onCategoryNotFound()
    {
        $this->flash('error')->addMessage('product.not.found');
        $this->redirectTo($this->getUrl('category.list', [$this->view->translate('page') => 1]));
    }

    /**
     * Use this controller as listener of the CategoryDeleted and CategoryNotFound events
     *
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('CategoryDeleted', [$this, 'onCategoryDeleted']);
        $this->listeners[] = $events->attach('CategoryNotFound', [$this, 'onCategoryNotFound']);
    }
}
