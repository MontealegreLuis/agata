<?php
/**
 * Controller to show the form to edit a category's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Categories\GetCategory;
use \ProductCatalog\Catalog\Categories\CategoryRequest;
use \CatalogModule\Forms\CategoryForm;

/**
 * Controller to show the form to edit a category's information
 */
class EditCategoryController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type GetCategory */
    protected $useCase;

    /** @type CategoryForm */
    protected $categoryForm;

    /**
     * @param GetCategory $getCategory
     */
    public function setUseCase(GetCategory $getCategory)
    {
        $this->useCase = $getCategory;
    }

    /**
     * @param CategoryFormFactory $categoryFormFactory
     */
    public function setCategoryForm(CategoryForm $categoryForm)
    {
        $this->categoryForm = $categoryForm;
    }

    /**
     * Edit the information of a given category
     */
    public function editAction()
    {
        $response = $this->useCase->getCategory(new CategoryRequest($this->param('id')));
        $this->categoryForm->setAction($this->getUrl('category.update'));
        $this->categoryForm->populateFromCategory($response->category);
        $this->view->category = $response->category;
        $this->view->categoryForm = $this->categoryForm;
    }

    /**
     * Go to the list of categories and show a proper warning message
     */
    public function onCategoryNotFound()
    {
        $this->flash('error')->addMessage('category.not.found');
        $this->redirectTo($this->getUrl('category.list', [$this->view->translate('page') => 1]));
    }

    /**
     * Use this controller as listener of the CategoryNotFound event
     *
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('CategoryNotFound', [$this, 'onCategoryNotFound']);
    }
}
