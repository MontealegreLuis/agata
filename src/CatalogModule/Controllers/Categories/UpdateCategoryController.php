<?php
/**
 * Controller to update a category's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Categories;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \Doctrine1\Exception\OptimisticLockException;
use \ProductCatalog\Products\CategoryEvent;
use \ProductCatalog\Catalog\Categories\UpdateCategory;
use \ProductCatalog\Catalog\Categories\UpdateCategoryRequest;
use \ProductCatalog\Products\CategoryDto;
use \ProductCatalog\Products\CategoryDomainException;
use \CatalogModule\Forms\CategoryForm;

/**
 * Controller to update a category's information
 */
class UpdateCategoryController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type UpdateCategory */
    protected $useCase;

    /** @type CategoryForm */
    protected $categoryForm;

    /**
     * @param UpdateCategory $updateCategory
     */
    public function setUseCase(UpdateCategory $updateCategory)
    {
        $this->useCase = $updateCategory;
    }

    /**
     * @param CategoryForm $categoryForm
     */
    public function setCategoryForm(CategoryForm $categoryForm)
    {
        $this->categoryForm = $categoryForm;
    }

    /**
     * Get all the available categories
     */
    public function updateAction()
    {
        $this->categoryForm->setAction($this->getUrl('category.update'));
        $this->view->categoryForm = $this->categoryForm->prepareForEditing();

        if (!$this->categoryForm->isValid($this->post())) {
            return $this->onCategoryFormValidationError($this->categoryForm->getValues());
        }

        try {
            $this->useCase->updateCategory(new UpdateCategoryRequest($this->categoryForm->getValues()));
        } catch (OptimisticLockException $e) {
            $this->onLockingFailure($e->getOriginalRecordValues());
        } catch (CategoryDomainException $e) {
            $this->onInvalidParentCategory($e);
        }
    }

    /**
     * @param array $category
     */
    public function onCategoryFormValidationError(array $category)
    {
        $this->view->category = new CategoryDto($category);
        $this->renderScript('category/edit.twig');
    }

    /**
     * If category information is stale prior to saving abort the operation and load the latest
     * category's information
     *
     * @todo add code to check for this condition
     */
    public function onLockingFailure(array $category)
    {
        $category = new CategoryDto($category);
        $this->categoryForm->populateFromCategory($category);
        $this->categoryForm->addErrorsToElement('version', ['category.optimistic.locking.failure']);
        $this->view->category = $category;
        $this->renderScript('category/edit.twig');
    }

    /**
     * Redirect to the show category page after updating the category information.
     *
     * @param EventInterface $event
     */
    public function onCategoryUpdated(CategoryEvent $event)
    {
        $this->flash('success')->addMessage('category.updated');
        $this->redirectTo($this->getUrl('category.show', ['id' => $event->getCategory()->id]));
    }

    /**
     * Go to the list of categories and show a proper warning message
     */
    public function onCategoryNotFound()
    {
        $this->flash('error')->addMessage('category.not.found');
        $this->redirectTo($this->getUrl('category.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @param CategoryEvent $event
     */
    public function onInvalidParentCategory(CategoryDomainException $e)
    {
        $this->categoryForm->setValidationErrors(['parentCategoryId' => $e->getMessage()]);
        $this->view->category = $e->getCategory();
        $this->renderScript('category/edit.twig');
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('CategoryNotFound', [$this, 'onCategoryNotFound']);
        $this->listeners[] = $events->attach('CategoryUpdated', [$this, 'onCategoryUpdated']);
        $this->listeners[] = $events->attach('InvalidParentCategory', [$this, 'onInvalidParentCategory']);
    }
}
