<?php
/**
 * Controller to show the form to save an Offer's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\OfferForm;

/**
 * Controller to show the form to save an Offer's information
 */
class CreateOfferController extends ControllerAction
{
    use ProvidesUrls;

    /** @type OfferForm */
    protected $offerForm;

    /**
     * @param OfferForm $OfferForm
     */
    public function setOfferForm(OfferForm $offerForm)
    {
        $this->offerForm = $offerForm;
    }

    /**
     * Show the form to create a Offer
     */
    public function createAction()
    {
        $this->offerForm->setAction($this->getUrl('offer.save'));
        $this->view->offerForm = $this->offerForm->prepareForCreating();
    }
}
