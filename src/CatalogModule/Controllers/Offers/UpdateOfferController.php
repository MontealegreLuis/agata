<?php
/**
 * Controller to update an offer's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \Zend\EventManager\EventInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \Doctrine1\Exception\OptimisticLockException;
use \CatalogModule\Forms\OfferForm;
use \ProductCatalog\Catalog\Offers\UpdateOffer;
use \ProductCatalog\Catalog\Offers\UpdateOfferRequest;
use \ProductCatalog\Products\OfferEvent;
use \ProductCatalog\Products\OfferDto;

/**
 * Controller to update an offer's information
 */
class UpdateOfferController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type UpdateOffer */
    protected $useCase;

    /** @type OfferForm */
    protected $offerForm;

    /**
     * @param UpdateOffer $updateOffer
     */
    public function setUseCase(UpdateOffer $updateOffer)
    {
        $this->useCase = $updateOffer;
    }

    /**
     * @param OfferForm $offerForm
     */
    public function setOfferForm(OfferForm $offerForm)
    {
        $this->offerForm = $offerForm;
    }

    /**
     * Update an offer's information
     */
    public function updateAction()
    {
        $this->offerForm->setAction($this->getUrl('offer.update'));
        $this->offerForm->setTypeAsReadOnly();
        $this->view->offerForm = $this->offerForm;

        if (!$this->offerForm->isValid($this->post())) {
            return $this->onOfferFormValidationError($this->offerForm->getValues());
        }

        try {
            $this->useCase->updateOffer(new UpdateOfferRequest($this->offerForm->getValues()));
        } catch (OptimisticLockException $e) {
            $this->onLockingFailure($e->getOriginalRecordValues());
        }
    }

    /**
     * @param array $offer
     */
    protected function onOfferFormValidationError(array $offer)
    {
        $this->view->offer = new OfferDto($offer);
        $this->renderScript('offer/edit.twig');
    }

    /**
     * If offer information is stale prior to saving abort the operation and load the latest
     * offer's information
     */
    public function onLockingFailure(array $offer)
    {
        $offer = new OfferDto($offer);
        $this->offerForm->populateFromOffer($offer);
        $this->offerForm->addErrorsToElement('version', ['offer.optimistic.locking.failure']);
        $this->view->offer = $offer;
        $this->renderScript('offer/edit.twig');
    }

    /**
     * Redirect to the list of offers page after updating the offer information.
     *
     * @param EventInterface $event
     */
    public function onOfferUpdated(OfferEvent $event)
    {
        $this->flash('success')->addMessage('offer.updated');
        $this->redirectTo($this->getUrl('offer.list', ['id' => $event->getOffer()->id]));
    }

    /**
     * Go to the list of offers and show a proper warning message
     */
    public function onOfferNotFound()
    {
        $this->flash('error')->addMessage('offer.not.found');
        $this->redirectTo($this->getUrl('offer.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('OfferNotFound', [$this, 'onOfferNotFound']);
        $this->listeners[] = $events->attach('OfferUpdated', [$this, 'onOfferUpdated']);
    }
}
