<?php
/**
 * Controller to show the list of available offers
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Offers\GetAllOffers;

/**
 * Controller to show the list of available offers
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class ListOffersController extends ControllerAction
{
    /** @type GetAllOffers */
    protected $useCase;

    /**
     * @param GetAllOffers $getAllOffers
     */
    public function setUseCase(GetAllOffers $getAllOffers)
    {
        $this->useCase = $getAllOffers;
    }

    /**
     * Get all the available offers
     */
    public function listAction()
    {
        $response = $this->useCase->getAllOffers();
        $this->view->items = $response->offers;
    }
}
