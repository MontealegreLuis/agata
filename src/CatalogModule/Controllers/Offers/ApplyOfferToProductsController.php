<?php
/**
 * Controller to apply an offer to a set of products
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Offers\ApplyOfferToProducts;
use \ProductCatalog\Catalog\Offers\ApplyOfferToProductsRequest;
use \ProductCatalog\Products\OfferEvent;
use \CatalogModule\Forms\OfferProductsForm;

/**
 * Controller to apply an offer to a set of products
 */
class ApplyOfferToProductsController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type ApplyOfferToProducts */
    protected $useCase;

    /** @type OfferProductsForm */
    protected $offerProductsForm;

    /**
     * @param ApplyOfferToProducts $applyOfferToProducts
     */
    public function setUseCase(ApplyOfferToProducts $applyOfferToProducts)
    {
        $this->useCase = $applyOfferToProducts;
    }

    /**
     * @param OfferProductsForm $offerProductsForm
     */
    public function setOfferProductsForm(OfferProductsForm $offerProductsForm)
    {
        $this->offerProductsForm = $offerProductsForm;
    }

    /**
     * Apply the given offer to a set of products
     */
    public function applyAction()
    {
        $this->offerProductsForm->setAction($this->getUrl('offer.apply'));

        if (!$this->offerProductsForm->isValid($this->post())) {
            return $this->onOfferProductsFormValidationErrors();
        }

        $this->useCase->applyOffer(
            new ApplyOfferToProductsRequest($this->offerProductsForm->getValues())
        );
    }

    /**
     * Show the form with the validation errors
     */
    protected function onOfferProductsFormValidationErrors()
    {
        $this->renderScript('offer/products.twig');
    }

    /**
     * Redirect to the list of offers.
     *
     * @param OfferEvent $event
     */
    public function onOfferApplied()
    {
        $this->flash('success')->addMessage('offer.applied');
        $this->redirectTo($this->getUrl('offer.list'));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('OfferApplied', [$this, 'onOfferApplied']);
    }
}
