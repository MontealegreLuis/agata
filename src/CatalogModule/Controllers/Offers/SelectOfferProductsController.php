<?php
/**
 * Controller to allow the administrator to select which products the current offer will apply
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\OfferProductsForm;

/**
 * Controller to allow the administrator to select which products the current offer will apply
 */
class SelectOfferProductsController extends ControllerAction
{
    use ProvidesUrls;

    /** @type OfferProductsForm */
    protected $offerProductsForm;

    /**
     * @param OfferForm $OfferForm
     */
    public function setOfferProductsForm(OfferProductsForm $offerProductsForm)
    {
        $this->offerProductsForm = $offerProductsForm;
    }

    /**
     * Show the form to create a Offer
     */
    public function productsAction()
    {
        $this->offerProductsForm->setAction($this->getUrl('offer.apply'));
        $this->offerProductsForm->setOfferId($this->param('id'));
        $this->view->offerId = $this->param('id');
        $this->view->offerProductsForm = $this->offerProductsForm;
    }
}
