<?php
/**
 * Controller to delete an offer's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Offers\DeleteOffer;
use \ProductCatalog\Catalog\Offers\OfferRequest;

/**
 * Controller to delete an offer's information
 */
class DeleteOfferController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type DeleteOffer */
    protected $useCase;

    /**
     * @param DeleteOffer $deleteOffer
     */
    public function setUseCase(DeleteOffer $deleteOffer)
    {
        $this->useCase = $deleteOffer;
    }

    /**
     * Delete an offer by it's ID
     */
    public function deleteAction()
    {
        $this->useCase->deleteOffer(new OfferRequest($this->param('id')));
    }

    /**
     * Redirect to the list of offers page after deleting the offer information.
     */
    public function onOfferDeleted()
    {
        $this->flash('success')->addMessage('offer.deleted');
        $this->redirectTo($this->getUrl('offer.list', [$this->view->translate('page') => 1]));
    }

    /**
     * Go to the list of offers and show a proper warning message
     */
    public function onOfferNotFound()
    {
        $this->flash('error')->addMessage('offer.not.found');
        $this->redirectTo($this->getUrl('offer.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('OfferDeleted', [$this, 'onOfferDeleted']);
        $this->listeners[] = $events->attach('OfferNotFound', [$this, 'onOfferNotFound']);
    }
}
