<?php
/**
 * Controller to show the form to edit an offer's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Offers\GetOffer;
use \ProductCatalog\Catalog\Offers\OfferRequest;
use \CatalogModule\Forms\OfferForm;

/**
 * Controller to show the form to edit an offer's information
 */
class EditOfferController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type GetOffer */
    protected $useCase;

    /** @type OfferForm */
    protected $productForm;

    /**
     * @param GetOffer $getOffer
     */
    public function setUseCase(GetOffer $getOffer)
    {
        $this->useCase = $getOffer;
    }

    /**
     * @param OfferForm $offerForm
     */
    public function setOfferForm(OfferForm $offerForm)
    {
        $this->offerForm = $offerForm;
    }

    /**
     * Show the form to edit an offer
     */
    public function editAction()
    {
        $response = $this->useCase->getOffer(new OfferRequest($this->param('id')));
        $this->view->offer = $response->offer;
        $this->offerForm->setAction($this->getUrl('offer.update'));
        $this->offerForm->setTypeAsReadOnly();
        $this->offerForm->populateFromOffer($response->offer);
        $this->view->offerForm = $this->offerForm;
    }

    /**
     * Go to the list of offers and show a proper warning message
     */
    public function onOfferNotFound()
    {
        $this->flash('error')->addMessage('offer.not.found');
        $this->redirectTo($this->getUrl('offer.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('OfferNotFound', [$this, 'onOfferNotFound']);
    }
}
