<?php
/**
 * Controller to save an offer's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Offers;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Products;
use \ProductCatalog\Catalog\Offers\SaveOffer;
use \ProductCatalog\Catalog\Offers\SaveOfferRequest;
use \ProductCatalog\Products\OfferEvent;
use \CatalogModule\Forms\OfferForm;

/**
 * Controller to save an offer's information
 */
class SaveOfferController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type SaveOffer */
    protected $useCase;

    /** @type OfferForm */
    protected $offerForm;

    /**
     * @param SaveOffer $saveOffer
     */
    public function setUseCase(SaveOffer $saveOffer)
    {
        $this->useCase = $saveOffer;
    }

    /**
     * @param OfferForm $offerForm
     */
    public function setOfferForm(OfferForm $offerForm)
    {
        $this->offerForm = $offerForm;
    }

    /**
     * Save a new offer's information
     */
    public function saveAction()
    {
        $this->offerForm->setAction($this->getUrl('offer.save'));
        $this->view->offerForm = $this->offerForm->prepareForCreating();

        if (!$this->offerForm->isValid($this->post())) {
            return $this->onOfferFormValidationErrors();
        }

        $this->useCase->saveOffer(new SaveOfferRequest($this->offerForm->getValues()));
    }

    /**
     * Show the form with the validation errors
     */
    protected function onOfferFormValidationErrors()
    {
        $this->renderScript('offer/create.twig');
    }

    /**
     * Redirect to the show offer page after saving the offer information.
     *
     * @param OfferEvent $event
     */
    public function onOfferSaved(OfferEvent $event)
    {
        $offer = $event->getOffer();
        $this->redirectTo($this->getUrl('offer.products', ['id' => $offer->id]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('OfferSaved', [$this, 'onOfferSaved']);
    }
}
