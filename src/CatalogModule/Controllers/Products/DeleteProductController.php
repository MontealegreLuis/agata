<?php
/**
 * Controller to delete a product's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Products\DeleteProduct;
use \ProductCatalog\Catalog\Products\ProductRequest;

/**
 * Controller to delete a product's information
 */
class DeleteProductController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type DeleteProduct */
    protected $useCase;

    /**
     * @param DeleteProduct $deleteProduct
     */
    public function setUseCase(DeleteProduct $deleteProduct)
    {
        $this->useCase = $deleteProduct;
    }

    /**
     * Delete a product by it's ID
     */
    public function deleteAction()
    {
        $this->useCase->deleteProduct(new ProductRequest($this->param('id')));
    }

    /**
     * Redirect to the list of products page after deleting the product information.
     */
    public function onProductDeleted()
    {
        $this->flash('success')->addMessage('product.deleted');
        $this->redirectTo($this->getUrl('product.list', [$this->view->translate('page') => 1]));
    }

    /**
     * Go to the list of products and show a proper warning message
     */
    public function onProductNotFound()
    {
        $this->flash('error')->addMessage('product.not.found');
        $this->redirectTo($this->getUrl('product.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('ProductDeleted', [$this, 'onProductDeleted']);
        $this->listeners[] = $events->attach('ProductNotFound', [$this, 'onProductNotFound']);
    }
}
