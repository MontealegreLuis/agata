<?php
/**
 * Controller to update a product's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \Zend\EventManager\EventInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \Doctrine1\Exception\OptimisticLockException;
use \CatalogModule\Forms\ProductForm;
use \ProductCatalog\Catalog\Products\UpdateProduct;
use \ProductCatalog\Catalog\Products\UpdateProductRequest;
use \ProductCatalog\Products\ProductEvent;
use \ProductCatalog\Products\ProductDto;

/**
 * Controller to update a product's information
 */
class UpdateProductController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type UpdateProduct */
    protected $useCase;

    /** @type ProductForm */
    protected $productForm;

    /**
     * @param UpdateProduct $updateProduct
     */
    public function setUseCase(UpdateProduct $updateProduct)
    {
        $this->useCase = $updateProduct;
    }

    /**
     * @param ProductForm $productForm
     */
    public function setProductForm(ProductForm $productForm)
    {
        $this->productForm = $productForm;
    }

    /**
     * Update a product's information
     */
    public function updateAction()
    {
        $this->productForm->setAction($this->getUrl('product.update'));
        $this->view->productForm = $this->productForm;

        if (!$this->productForm->isValid($this->post())) {
            return $this->onProductFormValidationError($this->productForm->getValues());
        }

        try {
            $this->useCase->updateProduct(new UpdateProductRequest($this->productForm->getValues()));
        } catch (OptimisticLockException $e) {
            $this->onLockingFailure($e->getOriginalRecordValues());
        }
    }

    /**
     * @param array $product
     */
    protected function onProductFormValidationError(array $product)
    {
        $this->view->product = new ProductDto($product);
        $this->renderScript('product/edit.twig');
    }

    /**
     * If product information is stale prior to saving abort the operation and load the latest
     * product's information
     */
    public function onLockingFailure(array $product)
    {
        $product = new ProductDto($product);
        $this->productForm->populateFromProduct($product);
        $this->productForm->addErrorsToElement('version', ['product.optimistic.locking.failure']);
        $this->view->product = $product;
        $this->renderScript('product/edit.twig');
    }

    /**
     * Redirect to the show product page after updating the product information.
     *
     * @param EventInterface $event
     */
    public function onProductUpdated(ProductEvent $event)
    {
        $this->flash('success')->addMessage('product.updated');
        $this->redirectTo($this->getUrl('product.show', ['id' => $event->getProduct()->id]));
    }

    /**
     * Go to the list of products and show a proper warning message
     */
    public function onProductNotFound()
    {
        $this->flash('error')->addMessage('product.not.found');
        $this->redirectTo($this->getUrl('product.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('ProductNotFound', [$this, 'onProductNotFound']);
        $this->listeners[] = $events->attach('ProductUpdated', [$this, 'onProductUpdated']);
    }
}
