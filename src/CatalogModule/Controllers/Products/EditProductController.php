<?php
/**
 * Controller to show the form to edit a product's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Products\GetProduct;
use \ProductCatalog\Catalog\Products\ProductRequest;
use \CatalogModule\Forms\ProductForm;

/**
 * Controller to show the form to edit a product's information
 */
class EditProductController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type GetProduct */
    protected $useCase;

    /** @type ProductForm */
    protected $productForm;

    /**
     * @param GetProduct $getProduct
     */
    public function setUseCase(GetProduct $getProduct)
    {
        $this->useCase = $getProduct;
    }

    /**
     * @param ProductForm $productForm
     */
    public function setProductForm(ProductForm $productForm)
    {
        $this->productForm = $productForm;
    }

    /**
     * Show the form to edit a product
     */
    public function editAction()
    {
        $response = $this->useCase->getProduct(new ProductRequest($this->param('id')));
        $this->view->product = $response->product;
        $this->productForm->setAction($this->getUrl('product.update'));
        $this->productForm->populateFromProduct($response->product);
        $this->view->productForm = $this->productForm;
    }

    /**
     * Go to the list of products and show a proper warning message
     */
    public function onProductNotFound()
    {
        $this->flash('error')->addMessage('product.not.found');
        $this->redirectTo($this->getUrl('product.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('ProductNotFound', [$this, 'onProductNotFound']);
    }
}
