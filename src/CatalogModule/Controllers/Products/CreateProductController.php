<?php
/**
 * Controller to show the form to save a product's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\ProductForm;

/**
 * Controller to show the form to save a product's information
 */
class CreateProductController extends ControllerAction
{
    use ProvidesUrls;

    /** @type ProductForm */
    protected $productForm;

    /**
     * @param ProductForm $productForm
     */
    public function setProductForm(ProductForm $productForm)
    {
        $this->productForm = $productForm;
    }

    /**
     * Show the form to create a product
     */
    public function createAction()
    {
        $this->productForm->setAction($this->getUrl('product.save'));
        $this->view->productForm = $this->productForm->prepareForCreating();
    }
}
