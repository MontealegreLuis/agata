<?php
/**
 * Controller to save a product's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Products\SaveProduct;
use \ProductCatalog\Catalog\Products\SaveProductRequest;
use \ProductCatalog\Products\ProductEvent;
use \CatalogModule\Forms\ProductForm;

/**
 * Controller to save a product's information
 */
class SaveProductController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type SaveProduct */
    protected $useCase;

    /** @type ProductForm */
    protected $productForm;

    /**
     * @param SaveProduct $saveProduct
     */
    public function setUseCase(SaveProduct $saveProduct)
    {
        $this->useCase = $saveProduct;
    }

    /**
     * @param ProductForm $productForm
     */
    public function setProductForm(ProductForm $productForm)
    {
        $this->productForm = $productForm;
    }

    /**
     * Save a new product's information
     */
    public function saveAction()
    {
        $this->productForm->setAction($this->getUrl('product.save'));
        $this->view->productForm = $this->productForm->prepareForCreating();

        if (!$this->productForm->isValid($this->post())) {
            return $this->onProductFormValidationErrors();
        }

        $this->useCase->saveProduct(new SaveProductRequest($this->productForm->getValues()));
    }

    /**
     * Show the form with the validation errors
     */
    protected function onProductFormValidationErrors()
    {
        $this->renderScript('product/create.twig');
    }

    /**
     * Redirect to the show product page after saving the product information.
     *
     * @param ProductEvent $event
     */
    public function onProductSaved(ProductEvent $event)
    {
        $product = $event->getProduct();
        $this->flash('success')->addMessage('product.created');
        $this->redirectTo($this->getUrl('product.show', ['id' => $product->id]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('ProductSaved', [$this, 'onProductSaved']);
    }
}
