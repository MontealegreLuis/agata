<?php
/**
 * Controller that filters the list of products according to a given criteria
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Products\FilterProducts;
use ProductCatalog\Catalog\Products\FilterProductsRequest;

/**
 * Controller that filters the list of products according to a given criteria
 */
class FilterProductsController extends ControllerAction
{
    /** @type FilterProducts */
    protected $useCase;

    /**
     * @param FilterProducts $filterProducts
     */
    public function setUseCase(FilterProducts $filterProducts)
    {
        $this->useCase = $filterProducts;
    }

    /**
     * Filter the list of products according to a given criteria
     */
    public function filterProductsAction()
    {
        $response = $this->useCase->getProductsThatMeetCriteria(
            new FilterProductsRequest($this->params())
        );
        $this->view->items = $response->products;
    }
}
