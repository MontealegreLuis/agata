<?php
/**
 * Controller to show the list of available products
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Products;

use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Products\GetAllProducts;

/**
 * Controller to show the list of available products
 */
class ListProductsController extends ControllerAction
{
    /** @type GetAllProducts */
    protected $useCase;

    /**
     * @param GetAllProducts $getAllProducts
     */
    public function setUseCase(GetAllProducts $getAllProducts)
    {
        $this->useCase = $getAllProducts;
    }

    /**
     * Get all the available products
     */
    public function listAction()
    {
        $response = $this->useCase->getAllProducts();
        $this->view->items = $response->products;
    }
}
