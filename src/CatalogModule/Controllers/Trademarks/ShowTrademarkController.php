<?php
/**
 * Controller to show a given trademark
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Trademarks\TrademarkRequest;
use \ProductCatalog\Catalog\Trademarks\GetTrademark;

/**
 * Controller to show a given trademark
 */
class ShowTrademarkController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type GetTrademark */
    protected $useCase;

    /**
     * @param GetTrademark $getTrademark
     */
    public function setUseCase(GetTrademark $getTrademark)
    {
        $this->useCase = $getTrademark;
    }

    /**
     * Get a trademark information
     */
    public function showAction()
    {
        $response = $this->useCase->getTrademark(new TrademarkRequest($this->param('id')));
        $this->view->trademark = $response->trademark;
    }

    /**
     * Go to the list of trademarks and show a proper warning message
     */
    public function onTrademarkNotFound()
    {
        $this->flash('error')->addMessage('trademark.not.found');
        $this->redirectTo($this->getUrl('trademark.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('TrademarkNotFound', [$this, 'onTrademarkNotFound']);
    }
}
