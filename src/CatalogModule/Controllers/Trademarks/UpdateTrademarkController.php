<?php
/**
 * Controller to update a trademark's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \Zend\EventManager\EventInterface;
use \Doctrine1\Exception\OptimisticLockException;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\TrademarkForm;
use \ProductCatalog\Catalog\Trademarks\UpdateTrademark;
use \ProductCatalog\Catalog\Trademarks\UpdateTrademarkRequest;
use \ProductCatalog\Products\TrademarkEvent;
use \ProductCatalog\Products\TrademarkDto;

/**
 * Controller to update a trademark's information
 */
class UpdateTrademarkController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type UpdateTrademark */
    protected $useCase;

    /** @type TrademarkForm */
    protected $trademarkForm;

    /**
     * @param UpdateTrademark $updateTrademark
     */
    public function setUseCase(UpdateTrademark $updateTrademark)
    {
        $this->useCase = $updateTrademark;
    }

    /**
     * @param TrademarkForm $trademarkForm
     */
    public function setTrademarkForm(TrademarkForm $trademarkForm)
    {
        $this->trademarkForm = $trademarkForm;
    }

    /**
     * Get all the available trademarks
     */
    public function updateAction()
    {
        $this->trademarkForm->setAction($this->getUrl('trademark.update'));
        $this->view->trademarkForm = $this->trademarkForm;

        if (!$this->trademarkForm->isValid($this->post())) {
            return $this->onTrademarkFormValidationError($this->trademarkForm->getValues());
        }

        try {
            $this->useCase->updateTrademark(new UpdateTrademarkRequest($this->trademarkForm->getValues()));
        } catch (OptimisticLockException $e) {
            $this->onLockingFailure($e->getOriginalRecordValues());
        }
    }

    /**
     * @param array $trademark
     */
    protected function onTrademarkFormValidationError(array $trademark)
    {
        $this->view->trademark = new TrademarkDto($trademark);
        $this->renderScript('trademark/edit.twig');
    }

    /**
     * If product information is stale prior to saving abort the operation and load the latest
     * trademark's information
     */
    public function onLockingFailure(array $trademark)
    {
        $trademark = new TrademarkDto($trademark);
        $this->trademarkForm->populateFromTrademark($trademark);
        $this->trademarkForm->addErrorsToElement('version', ['trademark.optimistic.locking.failure']);
        $this->view->trademark = $trademark;
        $this->renderScript('trademark/edit.twig');
    }

    /**
     * Redirect to the show trademark page after updating the product information.
     *
     * @param EventInterface $event
     */
    public function onTrademarkUpdated(TrademarkEvent $event)
    {
        $this->flash('success')->addMessage('trademark.updated');
        $this->redirectTo($this->getUrl('trademark.show', ['id' => $event->getTrademark()->id]));
    }

    /**
     * Go to the list of trademarks and show a proper warning message
     */
    public function onTrademarkNotFound()
    {
        $this->flash('error')->addMessage('trademark.not.found');
        $this->redirectTo($this->getUrl('trademark.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('TrademarkNotFound', [$this, 'onTrademarkNotFound']);
        $this->listeners[] = $events->attach('TrademarkUpdated', [$this, 'onTrademarkUpdated']);
        $this->listeners[] = $events->attach('LockingFailure', [$this, 'onLockingFailure']);
    }
}
