<?php
/**
 * Controller to show the list of available trademark
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Trademarks\GetAllTrademarks;

/**
 * Controller to show the list of available trademarks
 */
class ListTrademarksController extends ControllerAction
{
    /**
     * @var GetAllTrademarks
     */
    protected $useCase;

    /**
     * @param GetAllTrademarks $getAllTrademarks
     */
    public function setUseCase(GetAllTrademarks $getAllTrademarks)
    {
        $this->useCase = $getAllTrademarks;
    }

    /**
     * Get all the available trademarks
     */
    public function listAction()
    {
        $response = $this->useCase->getAllTrademarks();
        $this->view->items = $response->trademarks;
    }
}
