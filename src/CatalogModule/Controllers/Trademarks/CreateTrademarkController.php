<?php
/**
 * Controller to show the form to save a trademar's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\TrademarkForm;

/**
 * Controller to show the form to save a trademark's information
 */
class CreateTrademarkController extends ControllerAction
{
    use ProvidesUrls;

    /** @type TrademarkForm */
    protected $trademarkForm;

    /**
     * @param TrademarkForm $trademarkForm
     */
    public function setTrademarkForm(TrademarkForm $trademarkForm)
    {
        $this->trademarkForm = $trademarkForm;
    }

    /**
     * Show the form to capture the information of a new trademark
     */
    public function createAction()
    {
        $this->trademarkForm->setAction($this->getUrl('trademark.save'));
        $this->view->trademarkForm = $this->trademarkForm->prepareForCreating();
    }
}
