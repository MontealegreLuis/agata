<?php
/**
 * Controller to delete a trademark's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Trademarks\DeleteTrademark;
use \ProductCatalog\Catalog\Trademarks\TrademarkRequest;

/**
 * Controller to delete a trademark's information
 */
class DeleteTrademarkController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type DeleteTrademark */
    protected $useCase;

    /**
     * @param DeleteTrademark $deleteTrademark
     */
    public function setUseCase(DeleteTrademark $deleteTrademark)
    {
        $this->useCase = $deleteTrademark;
    }

    /**
     * Delete the trademark's information
     */
    public function deleteAction()
    {
        $this->useCase->deleteTrademark(new TrademarkRequest($this->param('id')));
    }

    /**
     * Redirect to the list trademark page after deleting the trademark information.
     */
    public function onTrademarktDeleted()
    {
        $this->flash('success')->addMessage('trademark.deleted');
        $this->redirectTo($this->getUrl('trademark.list', [$this->view->translate('page') => 1]));
    }

    /**
     * Go to the list of trademarks and list a proper warning message
     */
    public function onTrademarkNotFound()
    {
        $this->flash('error')->addMessage('trademark.not.found');
        $this->redirectTo($this->getUrl('trademark.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('TrademarkDeleted', [$this, 'onTrademarktDeleted']);
        $this->listeners[] = $events->attach('TrademarkNotFound', [$this, 'onTrademarkNotFound']);
    }
}
