<?php
/**
 * Controller to show the form to edit a trademar's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \ProductCatalog\Catalog\Trademarks\GetTrademark;
use \ProductCatalog\Catalog\Trademarks\TrademarkRequest;
use \CatalogModule\Forms\TrademarkForm;

/**
 * Controller to show the form to edit a trademark's information
 */
class EditTrademarkController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type GetTrademark */
    protected $useCase;

    /** @type TrademarkForm */
    protected $trademarkForm;

    /**
     * @param GetTrademark $getTrademark
     */
    public function setUseCase(GetTrademark $getTrademark)
    {
        $this->useCase = $getTrademark;
    }

    /**
     * @param ProductForm $trademarkForm
     */
    public function setTrademarkForm(TrademarkForm $trademarkForm)
    {
        $this->trademarkForm = $trademarkForm;
    }

    /**
     * Show the form to edit a trademark's information
     */
    public function editAction()
    {
        $response = $this->useCase->getTrademark(new TrademarkRequest($this->param('id')));
        $this->trademarkForm->setAction($this->getUrl('trademark.update'));
        $this->trademarkForm->populateFromTrademark($response->trademark);
        $this->view->trademark = $response->trademark;
        $this->view->trademarkForm = $this->trademarkForm;
    }

    /**
     * Go to the list of trademarks and show a proper warning message
     */
    public function onTrademarkNotFound()
    {
        $this->flash('error')->addMessage('trademark.not.found');
        $this->redirectTo($this->getUrl('trademark.list', [$this->view->translate('page') => 1]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('TrademarkNotFound', [$this, 'onTrademarkNotFound']);
    }
}
