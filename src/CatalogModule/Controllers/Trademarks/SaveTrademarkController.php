<?php
/**
 * Controller to save a trademark's information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\Controllers\Trademarks;

use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \CatalogModule\Forms\TrademarkForm;
use \ProductCatalog\Catalog\Trademarks\SaveTrademark;
use \ProductCatalog\Catalog\Trademarks\SaveTrademarkRequest;
use \ProductCatalog\Products\TrademarkEvent;

/**
 * Controller to save a trademark's information
 */
class SaveTrademarkController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type SaveTrademark */
    protected $useCase;

    /** @type TrademarkForm */
    protected $trademarkForm;

    /**
     * @param SaveProduct $saveTrademark
     */
    public function setUseCase(SaveTrademark $saveTrademark)
    {
        $this->useCase = $saveTrademark;
    }

    /**
     * @param trademarkForm $trademarkForm
     */
    public function setTrademarkForm(TrademarkForm $trademarkForm)
    {
        $this->trademarkForm = $trademarkForm;
    }

    /**
     * ave a new trademark information
     */
    public function saveAction()
    {
        $this->trademarkForm->setAction($this->getUrl('trademark.save'));
        $this->view->trademarkForm = $this->trademarkForm->prepareForCreating();

        if (!$this->trademarkForm->isValid($this->post())) {
            return $this->onTrademarkFormValidationError();
        }

        $this->useCase->saveTrademark(new SaveTrademarkRequest($this->trademarkForm->getValues()));
    }

    /**
     * Show the form validation errors
     */
    protected function onTrademarkFormValidationError()
    {
        $this->renderScript('trademark/create.twig');
    }

    /**
     * Redirect to the show trademark page after saving the trademarks information.
     *
     * @param EventInterface $event
     */
    public function onTrademarkSaved(TrademarkEvent $event)
    {
        $trademark = $event->getTrademark();
        $this->flash('success')->addMessage('trademark.created');
        $this->redirectTo($this->getUrl('trademark.show', ['id' => $trademark->id]));
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('TrademarkSaved', [$this, 'onTrademarkSaved']);
    }
}
