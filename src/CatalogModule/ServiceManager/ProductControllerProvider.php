<?php
/**
 * Product's controller configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \ZendApplication\Controller\Action\Events\HttpValidationListener;
use \ZendApplication\Controller\Action\Events\FilterInputListener;
use \Doctrine1\QuerySpecifications\OnlyPage;
use \ZendApplication\Paginator\Events\PaginationListener;
use \CatalogModule\Controllers\Products\ListProductsController;
use \CatalogModule\Controllers\Products\ShowProductController;
use \CatalogModule\Controllers\Products\CreateProductController;
use \CatalogModule\Controllers\Products\EditProductController;
use \CatalogModule\Controllers\Products\UpdateProductController;
use \CatalogModule\Controllers\Products\DeleteProductController;
use \CatalogModule\Controllers\Products\SaveProductController;

/**
 * Product's controller configuration
 */
class ProductControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'catalog.product';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('catalog.product.list', function(ServiceLocatorInterface $sl) {
            $controller = new ListProductsController($this->request, $this->response, $this->params);
            $controller->setUseCase($sl->get('product.list'));

            $paginator = $sl->get('paginator');

            $productRepository = $sl->get('product.repository');
            $productRepository->addSpecification(new OnlyPage(
                $this->request->getParam('page', 1), $paginator->getItemCountPerPage()
            ));

            $paginationListener = new PaginationListener($paginator, $productRepository);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['page' => 'int']));
            $eventManager->attach('postDispatch', $paginationListener);
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('product.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.product.show', function(ServiceLocatorInterface $sl) {
            $controller = new ShowProductController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('product.navigation'));

            $showProduct = $sl->get('product.show');
            $controller->setUseCase($showProduct);
            $showProduct->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('product.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.product.create', function(ServiceLocatorInterface $sl) {
            $controller = new CreateProductController($this->request, $this->response, $this->params);
            $controller->setProductForm($sl->get('product.form'));
            $controller->setUrlContainer($sl->get('product.navigation'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('product.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.product.save', function(ServiceLocatorInterface $sl) {
            $controller = new SaveProductController($this->request, $this->response, $this->params);
            $controller->setProductForm($sl->get('product.form'));
            $controller->setUrlContainer($sl->get('product.navigation'));

            $saveProduct = $sl->get('product.save');
            $controller->setUseCase($saveProduct);
            $saveProduct->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('product.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.product.edit', function(ServiceLocatorInterface $sl) {
            $controller = new EditProductController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('product.navigation'));
            $controller->setProductForm($sl->get('product.form'));

            $showProduct = $sl->get('product.show');
            $controller->setUseCase($showProduct);
            $showProduct->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('product.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.product.update', function(ServiceLocatorInterface $sl) {
            $controller = new UpdateProductController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('product.navigation'));
            $controller->setProductForm($sl->get('product.form'));

            $updateProduct = $sl->get('product.update');
            $controller->setUseCase($updateProduct);
            $updateProduct->getEventManager()->attachAggregate($controller);

            $record = $sl->get('product.record');
            $record->setExpectedVersion((integer) $this->request->getPost('version'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('product.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.product.delete', function(ServiceLocatorInterface $sl) {
            $controller = new DeleteProductController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('product.navigation'));

            $deleteProduct = $sl->get('product.delete');
            $controller->setUseCase($deleteProduct);
            $deleteProduct->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));

            return $controller;
        });
    }
}
