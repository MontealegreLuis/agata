<?php
/**
 * Offer's controller configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \ZendApplication\Controller\Action\Events\FilterInputListener;
use \ZendApplication\Controller\Action\Events\HttpValidationListener;
use \ZendApplication\Paginator\Events\PaginationListener;
use \Doctrine1\QuerySpecifications\OnlyPage;
use \Doctrine1\QuerySpecifications\ChainedSpecification;
use \CatalogModule\Controllers\Offers\ListOffersController;
use \CatalogModule\Controllers\Offers\CreateOfferController;
use \CatalogModule\Controllers\Offers\SaveOfferController;
use \CatalogModule\Controllers\Offers\SelectOfferProductsController;
use \CatalogModule\Controllers\Products\FilterProductsController;
use \CatalogModule\Controllers\Offers\ApplyOfferToProductsController;
use \CatalogModule\Controllers\Offers\EditOfferController;
use \CatalogModule\Controllers\Offers\DeleteOfferController;
use \CatalogModule\Controllers\Offers\UpdateOfferController;
use \CatalogDoctrine1\Products\OnlyProductsOfCategoryId;
use \CatalogDoctrine1\Products\OnlyProductsOfTrademarkId;

/**
 * Offer's controller configuration
 */
class OfferControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'catalog.offer';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('catalog.offer.list', function(ServiceLocatorInterface $sl) {
            $controller = new ListOffersController($this->request, $this->response, $this->params);
            $controller->setUseCase($sl->get('offer.list'));

            $paginator = $sl->get('paginator');

            $offerRepository = $sl->get('offer.repository');
            $offerRepository->addSpecification(new OnlyPage(
                $this->request->getParam('page', 1), $paginator->getItemCountPerPage()
            ));

            $paginationListener = new PaginationListener($paginator, $offerRepository);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['page' => 'int']));
            $eventManager->attach('postDispatch', $paginationListener);
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.create', function(ServiceLocatorInterface $sl) {
            $controller = new CreateOfferController($this->request, $this->response, $this->params);
            $controller->setOfferForm($sl->get('offer.form'));
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.save', function(ServiceLocatorInterface $sl) {
            $controller = new SaveOfferController($this->request, $this->response, $this->params);
            $controller->setOfferForm($sl->get('offer.form'));
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $saveOffer = $sl->get('offer.save');
            $controller->setUseCase($saveOffer);
            $saveOffer->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.products', function(ServiceLocatorInterface $sl) {
            $controller = new SelectOfferProductsController($this->request, $this->response, $this->params);

            $formFactory = $sl->get('offerProducts.form.factory');
            $formFactory->setOfferId($this->request->getParam('id'));

            $controller->setOfferProductsForm($formFactory->create());
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.filter-products', function(ServiceLocatorInterface $sl) {
            $controller = new FilterProductsController($this->request, $this->response, $this->params);
            $controller->setUseCase($sl->get('product.filter'));

            $repository = $sl->get('product.repository');
            $repository->addSpecification(new ChainedSpecification([
               new OnlyProductsOfCategoryId(), new OnlyProductsOfTrademarkId()
            ]));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.apply', function(ServiceLocatorInterface $sl) {
            $controller = new ApplyOfferToProductsController($this->request, $this->response, $this->params);
            $applyOffer = $sl->get('offer.apply');
            $controller->setUseCase($applyOffer);

            $repository = $sl->get('offer.repository');
            $repository->addSpecification(new ChainedSpecification([
                new OnlyProductsOfCategoryId(), new OnlyProductsOfTrademarkId()
            ]));

            $controller->setOfferProductsForm($sl->get('offerProducts.form'));
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $applyOffer->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.edit', function(ServiceLocatorInterface $sl) {
            $controller = new EditOfferController($this->request, $this->response, $this->params);
            $controller->setOfferForm($sl->get('offer.form'));
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $getOffer = $sl->get('offer.show');
            $controller->setUseCase($getOffer);
            $getOffer->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.update', function(ServiceLocatorInterface $sl) {
            $controller = new UpdateOfferController($this->request, $this->response, $this->params);
            $controller->setOfferForm($sl->get('offer.form'));
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $updateOffer = $sl->get('offer.update');
            $controller->setUseCase($updateOffer);
            $updateOffer->getEventManager()->attachAggregate($controller);

            $record = $sl->get('offer.record');
            $record->setExpectedVersion((integer) $this->request->getPost('version'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('preDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.offer.delete', function(ServiceLocatorInterface $sl) {
            $controller = new DeleteOfferController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('offer.navigation'));

            $deleteOffer = $sl->get('offer.delete');
            $controller->setUseCase($deleteOffer);
            $deleteOffer->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('offer.actions.listener'));

            return $controller;
        });

    }
}
