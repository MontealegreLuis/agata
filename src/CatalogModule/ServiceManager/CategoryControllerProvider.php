<?php
/**
 * Category's controllers configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \Doctrine1\QuerySpecifications\OnlyPage;
use \ZendApplication\Paginator\Events\PaginationListener;
use \ZendApplication\Controller\Action\Events\FilterInputListener;
use \ZendApplication\Controller\Action\Events\HttpValidationListener;
use \CatalogModule\Controllers\Categories\ListCategoriesController;
use \CatalogModule\Controllers\Categories\CreateCategoryController;
use \CatalogModule\Controllers\Categories\ShowCategoryController;
use \CatalogModule\Controllers\Categories\UpdateCategoryController;
use \CatalogModule\Controllers\Categories\SaveCategoryController;
use \CatalogModule\Controllers\Categories\EditCategoryController;
use \CatalogModule\Controllers\Categories\DeleteCategoryController;

/**
 * Category's controllers configuration
 */
class CategoryControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'catalog.category';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('catalog.category.list', function(ServiceLocatorInterface $sl) {
            $controller = new ListCategoriesController($this->request, $this->response, $this->params);
            $controller->setUseCase($sl->get('category.list'));

            $paginator = $sl->get('paginator');

            $categoryRepository = $sl->get('category.repository');
            $categoryRepository->addSpecification(new OnlyPage(
                $this->request->getParam('page', 1), $paginator->getItemCountPerPage()
            ));

            $paginationListener = new PaginationListener($paginator, $categoryRepository);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['page' => 'int']));
            $eventManager->attach('postDispatch', $paginationListener);
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('category.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.category.create', function(ServiceLocatorInterface $sl) {
            $controller = new CreateCategoryController($this->request, $this->response, $this->params);
            $controller->setCategoryForm($sl->get('category.form'));
            $controller->setUrlContainer($sl->get('category.navigation'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('category.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.category.save', function(ServiceLocatorInterface $sl) {
            $controller = new SaveCategoryController($this->request, $this->response, $this->params);
            $controller->setCategoryForm($sl->get('category.form'));
            $controller->setUrlContainer($sl->get('category.navigation'));

            $saveCategory = $sl->get('category.save');
            $controller->setUseCase($saveCategory);
            $saveCategory->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('category.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.category.show', function(ServiceLocatorInterface $sl) {
            $controller = new ShowCategoryController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('category.navigation'));

            $showCategory = $sl->get('category.show');
            $controller->setUseCase($showCategory);
            $showCategory->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('category.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.category.edit', function(ServiceLocatorInterface $sl) {
            $controller = new EditCategoryController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('category.navigation'));
            $controller->setCategoryForm($sl->get('category.form'));

            $showCategory = $sl->get('category.show');
            $controller->setUseCase($showCategory);
            $showCategory->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('category.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.category.update', function(ServiceLocatorInterface $sl) {
            $controller = new UpdateCategoryController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('category.navigation'));
            $controller->setCategoryForm($sl->get('category.form'));

            $updateCategory = $sl->get('category.update');
            $controller->setEventManager($updateCategory->getEventManager());
            $controller->setUseCase($updateCategory);
            $updateCategory->getEventManager()->attachAggregate($controller);

            $record = $sl->get('category.record');
            $record->setExpectedVersion((integer) $this->request->getPost('version'));

            $hydrator = $sl->get('category.hydrator');
            $hydrator->setRepository($sl->get('category.repository'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('category.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.category.delete', function(ServiceLocatorInterface $sl) {
            $controller = new DeleteCategoryController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('category.navigation'));

            $deleteCategory = $sl->get('category.delete');
            $controller->setUseCase($deleteCategory);
            $deleteCategory->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));

            return $controller;
        });
    }
}
