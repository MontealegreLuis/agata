<?php
/**
 * Service configuration for the catalog module
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\ServiceManager;

use \Zend_Config_Ini as IniConfig;
use \Zend_Filter_StringToLower as StringToLowerFilter;
use \Zend_Filter_Word_SeparatorToDash as SeparatorToDashFilter;
use \Zend_Filter_Alnum as AlphanumericFilter;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\Config;
use \ZendApplication\Navigation\UrlContainer;
use \ZendApplication\Filter\FriendlyUrlFilter;
use \ZendApplication\Filter\AccentsAndSpecialSymbolsFilter;
use \Doctrine1\Listener\DispatchDomainEvents;
use \ZendApplication\Controller\Action\Events\Navigation\MainMenuListener;
use \ZendApplication\Controller\Action\Events\Navigation\ActionsMenuListener;
use \Agata\ProductCatalog\Products\Images\ThumbnailsManager;
use \Agata\ProductCatalog\Products\Images\ThumbnailDescriptor;
use \Agata\ProductCatalog\Products\Category\CategoryImageUploaded;
use \Agata\ProductCatalog\Products\Category\CategoryRenamed;
use \Agata\ProductCatalog\Products\Category\CategoryDeleted;
use \Agata\ProductCatalog\Products\Product\ProductDeleted;
use \Agata\ProductCatalog\Products\Product\ProductRenamed;
use \Agata\ProductCatalog\Products\Product\ProductImageUploaded;
use \Agata\ProductCatalog\Products\Offer\OfferImageUploaded;
use \Agata\ProductCatalog\Products\Offer\OfferDeleted;
use \CatalogModule\Forms\CategoryFormFactory;
use \CatalogModule\Forms\ProductFormFactory;
use \CatalogModule\Forms\TrademarkForm;
use \CatalogModule\Forms\OfferFormFactory;
use \CatalogModule\Forms\OfferProductsFormFactory;
use \CatalogDoctrine1\Products\OfferFactory;
use \ProductCatalog\DomainEvents\DomainEventsBus;
use \ProductCatalog\Catalog\Categories\UpdateCategory;
use \ProductCatalog\Catalog\Categories\GetAllCategories;
use \ProductCatalog\Catalog\Categories\GetCategory;
use \ProductCatalog\Catalog\Categories\SaveCategory;
use \ProductCatalog\Catalog\Categories\DeleteCategory;
use \ProductCatalog\Catalog\Products\GetAllProducts;
use \ProductCatalog\Catalog\Products\GetProduct;
use \ProductCatalog\Catalog\Products\SaveProduct;
use \ProductCatalog\Catalog\Products\DeleteProduct;
use \ProductCatalog\Catalog\Products\UpdateProduct;
use \ProductCatalog\Catalog\Products\FilterProducts;
use \ProductCatalog\Catalog\Trademarks\GetAllTrademarks;
use \ProductCatalog\Catalog\Trademarks\GetTrademark;
use \ProductCatalog\Catalog\Trademarks\SaveTrademark;
use \ProductCatalog\Catalog\Trademarks\UpdateTrademark;
use \ProductCatalog\Catalog\Trademarks\DeleteTrademark;
use \ProductCatalog\Catalog\Offers\GetAllOffers;
use \ProductCatalog\Catalog\Offers\SaveOffer;
use \ProductCatalog\Catalog\Offers\ApplyOfferToProducts;
use \ProductCatalog\Catalog\Offers\GetOffer;
use \ProductCatalog\Catalog\Offers\DeleteOffer;
use \ProductCatalog\Catalog\Offers\UpdateOffer;

/**
 * Service configuration for the catalog module
 */
class CatalogModuleProvider extends Config
{
    /**
     * Initialize service configuration for category, product, trademark and offer entities
     */
    public function __construct($config = [])
    {
        $config['factories']['catalog.menu.listener'] = function(ServiceLocatorInterface $sl) {
            return new MainMenuListener(
                require $sl->get('catalog.menu.configuration'),
                $sl->get('cache.manager')->getCache('default')
            );
        };
        $config['services']['catalog.menu.configuration'] = __DIR__ . '/../Resources/config/navigation/menu.php';
        $this->config = $config;

        $this->configureCategory();
        $this->configureProduct();
        $this->configureTrademark();
        $this->configureOffer();
    }

    /**
     * Register services related to the Category entity
     */
    protected function configureCategory()
    {
        $factories = [
            'category.show' => function(ServiceLocatorInterface $sl) {
                return new GetCategory($sl->get('category.repository'));
            },
            'category.list' => function(ServiceLocatorInterface $sl) {
                return new GetAllCategories($sl->get('category.repository'));
            },
            'category.save' => function(ServiceLocatorInterface $sl) {
                $saveCategory = new SaveCategory($sl->get('category.repository'));

                $saveCategory->getEventManager()
                             ->attach('CategorySaved', $sl->get('category.event.imageUploaded'));

                return $saveCategory;
            },
            'category.update' => function(ServiceLocatorInterface $sl) {
                $updateCategory = new UpdateCategory($sl->get('category.repository'));

                $eventBus = new DomainEventsBus();
                $categoryRenamed = new CategoryRenamed(
                    $sl->get('category.images.path'), $sl->get('category.slugFilter')
                );
                $eventBus->register('CategoryRenamed', $categoryRenamed);

                $record = $sl->get('category.record');
                $record->addListener(new DispatchDomainEvents($eventBus));

                $updateCategory->setEventsBus($eventBus);
                $updateCategory->getEventManager()
                               ->attach('CategoryUpdated', $sl->get('category.event.imageUploaded'));

                return $updateCategory;
            },
            'category.delete' => function(ServiceLocatorInterface $sl) {
                $deleteCategory = new DeleteCategory($sl->get('category.repository'));

                $categoryDeleted = new CategoryDeleted($sl->get('category.images.path'));

                $deleteCategory->getEventManager()->attach('CategoryDeleted', $categoryDeleted);

                return $deleteCategory;
            },
            'category.form.factory' => function(ServiceLocatorInterface $sl) {
                $config = new IniConfig($sl->get('category.form.configuration'), 'category');

                return new CategoryFormFactory($config->toArray(), $sl->get('category.repository'));
            },
            'category.form' => function(ServiceLocatorInterface $sl) {
                $form = $sl->get('category.form.factory')->create();

                return $form;
            },
            'category.navigation' => function(ServiceLocatorInterface $sl) {
                return new UrlContainer(
                    require $sl->get('category.navigation.configuration'), $sl->get('view')
                );
            },
            'category.event.imageUploaded' => function(ServiceLocatorInterface $sl) {
                $imagesPath = $sl->get('category.images.path');

                return new CategoryImageUploaded(
                    $imagesPath,
                    new ThumbnailsManager([new ThumbnailDescriptor($imagesPath, 220, 0)]),
                    $sl->get('category.form')
                );
            },
            'category.slugFilter' => function(ServiceLocatorInterface $sl) {
                return new FriendlyUrlFilter(
                    new StringToLowerFilter(['encoding' => 'utf-8']),
                    new SeparatorToDashFilter(),
                    new AccentsAndSpecialSymbolsFilter(new AlphanumericFilter($alloWhiteSpaces = true)),
                    '.jpg'
                );
            },
            'category.actions.listener' => function(ServiceLocatorInterface $sl) {
                return new ActionsMenuListener(require $sl->get('category.actions.configuration'));
            },
        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);
        $this->config['services'] = [
            'category.form.configuration' => __DIR__ . '/../Resources/config/forms/category.ini',
            'category.navigation.configuration' => __DIR__ . '/../Resources/config/navigation/category.php',
            'category.actions.configuration' => __DIR__ . '/../Resources/config/navigation/actions/category.php',
        ];
    }

    /**
     * Register services related to the Product entity
     */
    protected function configureProduct()
    {
        $factories = [
            'product.list' => function(ServiceLocatorInterface $sl) {
                return new GetAllProducts($sl->get('product.repository'));
            },
            'product.show' => function(ServiceLocatorInterface $sl) {
                return new GetProduct($sl->get('product.repository'));
            },
            'product.save' => function(ServiceLocatorInterface $sl) {
                $saveProduct = new SaveProduct(
                    $sl->get('product.repository'),
                    $sl->get('trademark.repository'),
                    $sl->get('category.repository')
                );
                $imagesPaths = [
                    'original' => 'uploads/products/originals',
                    'medium' => 'uploads/products/detail',
                    'thumbnail' => 'uploads/products/thumbnails',
                ];
                $thumbnails = new ThumbnailsManager([
                    new ThumbnailDescriptor($imagesPaths['medium'], 430, 0),
                    new ThumbnailDescriptor($imagesPaths['thumbnail'], 220, 0),
                ]);

                $productImageUploaded = new ProductImageUploaded(
                    $imagesPaths['original'], $thumbnails, $sl->get('product.form')
                );
                $saveProduct->getEventManager()->attach('ProductSaved', $productImageUploaded);

                return $saveProduct;
            },
            'product.update' => function(ServiceLocatorInterface $sl) {
                $updateProduct = new UpdateProduct(
                    $sl->get('product.repository'),
                    $sl->get('trademark.repository'),
                    $sl->get('category.repository')
                );
                $imagesPaths = [
                    'original' => 'uploads/products/originals',
                    'medium' => 'uploads/products/detail',
                    'thumbnail' => 'uploads/products/thumbnails',
                ];
                $thumbnails = new ThumbnailsManager([
                    new ThumbnailDescriptor($imagesPaths['medium'], 430, 0),
                    new ThumbnailDescriptor($imagesPaths['thumbnail'], 220, 0),
                ]);

                $eventBus = new DomainEventsBus();
                $productRenamed = new ProductRenamed($imagesPaths, $sl->get('product.slugFilter'));
                $eventBus->register('ProductRenamed', $productRenamed);

                $record = $sl->get('product.record');
                $record->addListener(new DispatchDomainEvents($eventBus));

                $imageUploaded = new ProductImageUploaded(
                    $imagesPaths['original'], $thumbnails, $sl->get('product.form')
                );

                $updateProduct->setEventsBus($eventBus);
                $updateProduct->getEventManager()->attach('ProductUpdated', $imageUploaded);

                return $updateProduct;
            },
            'product.delete' => function(ServiceLocatorInterface $sl) {
                $deleteProduct = new DeleteProduct($sl->get('product.repository'));
                $imagesPaths = [
                    'original' => 'uploads/products/originals',
                    'medium' => 'uploads/products/detail',
                    'thumbnail' => 'uploads/products/thumbnails',
                ];
                $productDeleted = new ProductDeleted($imagesPaths);

                $deleteProduct->getEventManager()->attach('ProductDeleted', $productDeleted);

                return $deleteProduct;
            },
            'product.filter' => function(ServiceLocatorInterface $sl) {
                return new FilterProducts($sl->get('product.repository'));
            },
            'product.form.factory' => function(ServiceLocatorInterface $sl) {
                $config = new IniConfig($sl->get('product.form.configuration'), 'product');

                return new ProductFormFactory(
                    $config->toArray(),
                    $sl->get('category.repository'),
                    $sl->get('trademark.repository')
                );
            },
            'product.form' => function(ServiceLocatorInterface $sl) {
                return $sl->get('product.form.factory')->create();
            },
            'product.navigation' => function(ServiceLocatorInterface $sl) {
                return new UrlContainer(
                    require $sl->get('product.navigation.configuration'), $sl->get('view')
                );
            },
            'product.slugFilter' => function(ServiceLocatorInterface $sl) {
                return new FriendlyUrlFilter(
                    new StringToLowerFilter(['encoding' => 'utf-8']),
                    new SeparatorToDashFilter(),
                    new AccentsAndSpecialSymbolsFilter(new AlphanumericFilter($alloWhiteSpaces = true)),
                    '.jpg'
                );
            },
            'product.actions.listener' => function(ServiceLocatorInterface $sl) {
                return new ActionsMenuListener(require $sl->get('product.actions.configuration'));
            },
        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);
        $services = [
            'product.form.configuration' => __DIR__ . '/../Resources/config/forms/product.ini',
            'product.navigation.configuration' => __DIR__ . '/../Resources/config/navigation/product.php',
            'product.actions.configuration' => __DIR__ . '/../Resources/config/navigation/actions/product.php',
        ];
        $this->config['services'] = array_merge($this->config['services'], $services);

    }

    /**
     * Register services related to the Trademark entity
     */
    protected function configureTrademark()
    {
        $factories = [
            'trademark.list' => function(ServiceLocatorInterface $sl) {
                return new GetAllTrademarks($sl->get('trademark.repository'));
            },
            'trademark.show' => function(ServiceLocatorInterface $sl) {
                return new GetTrademark($sl->get('trademark.repository'));
            },
            'trademark.save' => function(ServiceLocatorInterface $sl) {
                $saveTrademark = new SaveTrademark($sl->get('trademark.repository'));

                return $saveTrademark;
            },
            'trademark.update' => function(ServiceLocatorInterface $sl) {
                $updateTrademark = new UpdateTrademark($sl->get('trademark.repository'));

                return $updateTrademark;
            },
            'trademark.delete' => function(ServiceLocatorInterface $sl) {
                return new DeleteTrademark($sl->get('trademark.repository'));
            },
            'trademark.form' => function(ServiceLocatorInterface $sl) {
                $config = new IniConfig($sl->get('trademark.form.configuration'), 'trademark');

                return new TrademarkForm($config->toArray());
            },
            'trademark.navigation' => function(ServiceLocatorInterface $sl) {
                return new UrlContainer(
                    require $sl->get('trademark.navigation.configuration'), $sl->get('view')
                );
            },
            'trademark.actions.listener' => function(ServiceLocatorInterface $sl) {
                return new ActionsMenuListener(require $sl->get('trademark.actions.configuration'));
            },
        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);
        $services = [
            'trademark.navigation.configuration' => __DIR__ . '/../Resources/config/navigation/trademark.php',
            'trademark.form.configuration' => __DIR__ . '/../Resources/config/forms/trademark.ini',
            'trademark.actions.configuration' => __DIR__ . '/../Resources/config/navigation/actions/trademark.php',
        ];
        $this->config['services'] = array_merge($this->config['services'], $services);
    }

    /**
     * Register services related to the Offer entity
     */
    protected function configureOffer()
    {
        $factories = [
            'offer.list' => function(ServiceLocatorInterface $sl) {
                return new GetAllOffers($sl->get('offer.repository'));
            },
            'offer.save' => function(ServiceLocatorInterface $sl) {
                $saveOffer =  new SaveOffer($sl->get('offer.repository'), $sl->get('offer.factory'));

                $offerImageUploaded = new OfferImageUploaded(
                    'uploads/offers', $sl->get('offer.form')
                );
                $saveOffer->getEventManager()->attach('OfferSaved', $offerImageUploaded);

                return $saveOffer;
            },
            'offer.update' => function(ServiceLocatorInterface $sl) {
                $updateOffer =  new UpdateOffer($sl->get('offer.repository'));

                $offerImageUploaded = new OfferImageUploaded(
                    'uploads/offers', $sl->get('offer.form')
                );
                $updateOffer->getEventManager()->attach('OfferUpdated', $offerImageUploaded);

                return $updateOffer;
            },
            'offer.apply' => function(ServiceLocatorInterface $sl) {
                return new ApplyOfferToProducts($sl->get('offer.repository'));
            },
            'offer.actions.listener' => function(ServiceLocatorInterface $sl) {
                return new ActionsMenuListener(require $sl->get('offer.actions.configuration'));
            },
            'offer.show' => function(ServiceLocatorInterface $sl) {
                return  new GetOffer($sl->get('offer.repository'));
            },
            'offer.delete' => function(ServiceLocatorInterface $sl) {
                $deleteOffer = new DeleteOffer($sl->get('offer.repository'));

                $deleteOffer->getEventManager()
                            ->attach('OfferDeleted', new OfferDeleted('uploads/offers'));

                return $deleteOffer;
            },
            'offer.factory' => function(ServiceLocatorInterface $sl) {
                return new OfferFactory($sl->get('offer.repository'));
            },
            'offer.form.factory' => function(ServiceLocatorInterface $sl) {
                $config = new IniConfig($sl->get('offer.form.configuration'), 'offer');

                return new OfferFormFactory($config->toArray());
            },
            'offer.form' => function(ServiceLocatorInterface $sl) {
                return$sl->get('offer.form.factory')->create();
            },
            'offerProducts.form.factory' => function(ServiceLocatorInterface $sl) {
                $config = new IniConfig($sl->get('offerProducts.form.configuration'), 'offer-products');

                return new OfferProductsFormFactory(
                    $config->toArray(),
                    $sl->get('category.repository'),
                    $sl->get('trademark.repository'),
                    $sl->get('product.repository')
                );
            },
            'offerProducts.form' => function(ServiceLocatorInterface $sl) {
                return$sl->get('offerProducts.form.factory')->create();
            },
            'offer.navigation' => function(ServiceLocatorInterface $sl) {
                return new UrlContainer(
                    require $sl->get('offer.navigation.configuration'), $sl->get('view')
                );
            },
        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);
        $services = [
            'offer.form.configuration' => __DIR__ . '/../Resources/config/forms/offer.ini',
            'offerProducts.form.configuration' => __DIR__ . '/../Resources/config/forms/offer-products.ini',
            'offer.navigation.configuration' => __DIR__ . '/../Resources/config/navigation/offer.php',
            'offer.actions.configuration' => __DIR__ . '/../Resources/config/navigation/actions/offer.php',
        ];
        $this->config['services'] = array_merge($this->config['services'], $services);
    }
}
