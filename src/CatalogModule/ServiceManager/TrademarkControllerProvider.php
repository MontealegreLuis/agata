<?php
/**
 * Trademark's controller configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \ZendApplication\Controller\Action\Events\HttpValidationListener;
use \ZendApplication\Controller\Action\Events\FilterInputListener;
use \Doctrine1\QuerySpecifications\OnlyPage;
use \ZendApplication\Paginator\Events\PaginationListener;
use \CatalogModule\Controllers\Trademarks\ListTrademarksController;
use \CatalogModule\Controllers\Trademarks\ShowTrademarkController;
use \CatalogModule\Controllers\Trademarks\CreateTrademarkController;
use \CatalogModule\Controllers\Trademarks\SaveTrademarkController;
use \CatalogModule\Controllers\Trademarks\EditTrademarkController;
use \CatalogModule\Controllers\Trademarks\UpdateTrademarkController;
use \CatalogModule\Controllers\Trademarks\DeleteTrademarkController;

/**
 * Trademark's controller configuration
 */
class TrademarkControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'catalog.trademark';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('catalog.trademark.list', function(ServiceLocatorInterface $sl) {
            $controller = new ListTrademarksController($this->request, $this->response, $this->params);
            $controller->setUseCase($sl->get('trademark.list'));

            $paginator = $sl->get('paginator');

            $productRepository = $sl->get('trademark.repository');
            $productRepository->addSpecification(new OnlyPage(
                $this->request->getParam('page', 1), $paginator->getItemCountPerPage()
            ));

            $paginationListener = new PaginationListener($paginator, $productRepository);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['page' => 'int']));
            $eventManager->attach('postDispatch', $paginationListener);
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.trademark.show', function(ServiceLocatorInterface $sl) {
            $controller = new ShowTrademarkController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('trademark.navigation'));

            $showTrademark = $sl->get('trademark.show');
            $controller->setUseCase($showTrademark);
            $showTrademark->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.trademark.create', function(ServiceLocatorInterface $sl) {
            $controller = new CreateTrademarkController($this->request, $this->response, $this->params);
            $controller->setTrademarkForm($sl->get('trademark.form'));
            $controller->setUrlContainer($sl->get('trademark.navigation'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.trademark.save', function(ServiceLocatorInterface $sl) {
            $controller = new SaveTrademarkController($this->request, $this->response, $this->params);
            $controller->setTrademarkForm($sl->get('trademark.form'));
            $controller->setUrlContainer($sl->get('trademark.navigation'));

            $saveTrademark = $sl->get('trademark.save');
            $controller->setUseCase($saveTrademark);
            $saveTrademark->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('preDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.trademark.edit', function(ServiceLocatorInterface $sl) {
            $controller = new EditTrademarkController($this->request, $this->response, $this->params);
            $controller->setTrademarkForm($sl->get('trademark.form'));
            $controller->setUrlContainer($sl->get('trademark.navigation'));

            $showTrademark = $sl->get('trademark.show');
            $controller->setUseCase($showTrademark);
            $showTrademark->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.trademark.update', function(ServiceLocatorInterface $sl) {
            $controller = new UpdateTrademarkController($this->request, $this->response, $this->params);
            $controller->setTrademarkForm($sl->get('trademark.form'));
            $controller->setUrlContainer($sl->get('trademark.navigation'));

            $updateTrademark = $sl->get('trademark.update');
            $controller->setUseCase($updateTrademark);
            $updateTrademark->getEventManager()->attachAggregate($controller);

            $record = $sl->get('trademark.record');
            $record->setExpectedVersion((integer) $this->request->getPost('version'));

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', new HttpValidationListener('post'));
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('preDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
        $serviceManager->setFactory('catalog.trademark.delete', function(ServiceLocatorInterface $sl) {
            $controller = new DeleteTrademarkController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('trademark.navigation'));

            $deleteTrademark = $sl->get('trademark.delete');
            $controller->setUseCase($deleteTrademark);
            $deleteTrademark->getEventManager()->attachAggregate($controller);

            $eventManager = $controller->getEventManager();
            $eventManager->attach('preDispatch', $sl->get('authentication.listener'));
            $eventManager->attach('preDispatch', new FilterInputListener(['id' => 'id']));
            $eventManager->attach('postDispatch', $sl->get('catalog.menu.listener'));
            $eventManager->attach('postDispatch', $sl->get('trademark.actions.listener'));

            return $controller;
        });
    }
}
