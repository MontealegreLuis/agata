<?php
return [
    'create' => 'crear',
    'save' => 'guardar',
    'edit' => 'editar',
    'update' => 'actualizar',
    'delete' => 'eliminar',
    'show' => 'mostrar',
    'list' => 'listar',
    'filter-products' => 'filtrar-productos',
    'apply' => 'aplicar',
    'page' => 'pagina',
    'category' => 'categoria',
    'trademark' => 'marca',
    'product' => 'producto',
    'offer' => 'oferta',
];
