<?php
return [
    'catalog' => [
       'route' => '/:@controller/:@action/*',
        'defaults' => [
            'controller' => 'product',
            'action' => 'list',
            'module' => 'catalog'
        ],
    ],
];
