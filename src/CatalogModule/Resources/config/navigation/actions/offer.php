<?php
return [
    'list' => [
        'label' => 'offer.action.list',
        'controller' => 'offer',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'create' => [
        'label' => 'offer.action.create',
        'controller' => 'offer',
        'action' => 'create',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'delete' => [
        'label' => 'offer.action.delete',
        'controller' => 'offer',
        'action' => 'delete',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'edit' => [
        'label' => 'offer.action.edit',
        'controller' => 'offer',
        'action' => 'edit',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'products' => [
        'label' => 'offer.action.products',
        'controller' => 'offer',
        'action' => 'products',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
];
