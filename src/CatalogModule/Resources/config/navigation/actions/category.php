<?php
return [
    'list' => [
        'label' => 'category.action.list',
        'controller' => 'category',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'create' => [
        'label' => 'category.action.create',
        'controller' => 'category',
        'action' => 'create',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'delete' => [
        'label' => 'category.action.delete',
        'controller' => 'category',
        'action' => 'delete',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'edit' => [
        'label' => 'category.action.edit',
        'controller' => 'category',
        'action' => 'edit',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'show' => [
        'label' => 'category.action.show',
        'controller' => 'category',
        'action' => 'show',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
];
