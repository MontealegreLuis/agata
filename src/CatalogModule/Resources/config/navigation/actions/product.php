<?php
return [
    'list' => [
        'label' => 'product.action.list',
        'controller' => 'product',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'create' => [
        'label' => 'product.action.create',
        'controller' => 'product',
        'action' => 'create',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'delete' => [
        'label' => 'product.action.delete',
        'controller' => 'product',
        'action' => 'delete',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'edit' => [
        'label' => 'product.action.edit',
        'controller' => 'product',
        'action' => 'edit',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'show' => [
        'label' => 'product.action.show',
        'controller' => 'product',
        'action' => 'show',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
];
