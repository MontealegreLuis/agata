<?php
return [
    'list' => [
        'label' => 'trademark.action.list',
        'controller' => 'trademark',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'create' => [
        'label' => 'trademark.action.create',
        'controller' => 'trademark',
        'action' => 'create',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'delete' => [
        'label' => 'trademark.action.delete',
        'controller' => 'trademark',
        'action' => 'delete',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'edit' => [
        'label' => 'trademark.action.edit',
        'controller' => 'trademark',
        'action' => 'edit',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'show' => [
        'label' => 'trademark.action.show',
        'controller' => 'trademark',
        'action' => 'show',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
];
