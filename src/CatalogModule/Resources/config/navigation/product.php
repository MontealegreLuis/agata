<?php
return [
    'product.create' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'create',
        'route' => 'catalog',
    ],
    'product.save' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'save',
        'route' => 'catalog',
    ],
    'product.edit' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'edit',
        'route' => 'catalog',
    ],
    'product.update' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'update',
        'route' => 'catalog',
    ],
    'product.show' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'show',
        'route' => 'catalog',
    ],
    'product.delete' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'delete',
        'route' => 'catalog',
    ],
    'product.list' => [
        'module' => 'catalog',
        'controller' => 'product',
        'action' => 'list',
        'route' => 'catalog',
    ],
];
