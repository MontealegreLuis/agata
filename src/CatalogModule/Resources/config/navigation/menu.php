<?php
return [
    'products' => [
        'label' => 'Productos',
        'controller' => 'product',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog'
    ],
    'offers' => [
        'label' => 'Ofertas',
        'controller' => 'offer',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog'
    ],
    'category' => [
        'label' => 'Categorías',
        'controller' => 'category',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
    'trademark' => [
        'label' => 'Marcas',
        'controller' => 'trademark',
        'action' => 'list',
        'module' => 'catalog',
        'route' => 'catalog',
    ],
];
