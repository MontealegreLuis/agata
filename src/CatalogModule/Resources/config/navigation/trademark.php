<?php
return [
    'trademark.create' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'create',
        'route' => 'catalog',
    ],
    'trademark.save' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'save',
        'route' => 'catalog',
    ],
    'trademark.edit' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'edit',
        'route' => 'catalog',
    ],
    'trademark.update' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'update',
        'route' => 'catalog',
    ],
    'trademark.show' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'show',
        'route' => 'catalog',
    ],
    'trademark.delete' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'delete',
        'route' => 'catalog',
    ],
    'trademark.list' => [
        'module' => 'catalog',
        'controller' => 'trademark',
        'action' => 'list',
        'route' => 'catalog',
    ],
];
