<?php
return [
    'offer.create' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'create',
        'route' => 'catalog',
    ],
    'offer.save' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'save',
        'route' => 'catalog',
    ],
    'offer.edit' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'edit',
        'route' => 'catalog',
    ],
    'offer.update' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'update',
        'route' => 'catalog',
    ],
    'offer.products' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'products',
        'route' => 'catalog',
    ],
    'offer.apply' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'apply',
        'route' => 'catalog',
    ],
    'offer.delete' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'delete',
        'route' => 'catalog',
    ],
    'offer.list' => [
        'module' => 'catalog',
        'controller' => 'offer',
        'action' => 'list',
        'route' => 'catalog',
    ],
];
