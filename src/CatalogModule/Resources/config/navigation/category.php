<?php
return [
    'category.create' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'create',
        'route' => 'catalog',
    ],
    'category.save' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'save',
        'route' => 'catalog',
    ],
    'category.edit' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'edit',
        'route' => 'catalog',
    ],
    'category.update' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'update',
        'route' => 'catalog',
    ],
    'category.show' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'show',
        'route' => 'catalog',
    ],
    'category.delete' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'delete',
        'route' => 'catalog',
    ],
    'category.list' => [
        'module' => 'catalog',
        'controller' => 'category',
        'action' => 'list',
        'route' => 'catalog',
    ],
];
