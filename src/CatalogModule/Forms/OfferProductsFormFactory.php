<?php
/**
 * Factory for the offer products form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogModule\Forms;

use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\Category;
use \ProductCatalog\Products\CategoryDto;
use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\TrademarkDto;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\Product;
use \ProductCatalog\Products\ProductDto;
use \CatalogDoctrine1\Products\ProductTable;

/**
 * Factory for the offer products form
 *
 * It adds the categories and trademarks options
 */
class OfferProductsFormFactory
{
    /** @type array */
    protected $options;

    /** @type CategoryRepository */
    protected $categories;

    /** @type TrademarkRepository */
    protected $trademarks;

    /** @type ProductTable */
    protected $products;

    /** @type integer */
    protected $offerId;

    /**
     * @param array               $options
     * @param CategoryRepository  $categoryRepository,
     * @param TrademarkRepository $trademarkRepository,
     * @param ProductRepository   $productRepository
     */
    public function __construct(
        array $options,
        CategoryRepository $categoryRepository,
        TrademarkRepository $trademarkRepository,
        ProductTable $productRepository
    )
    {
        $this->options = $options;
        $this->categories = $categoryRepository;
        $this->trademarks = $trademarkRepository;
        $this->products = $productRepository;
    }

    /**
     * @param integer $offerId
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;
    }

    /**
     * @return OfferProductsForm
     */
    public function create()
    {
        $form = new OfferProductsForm($this->options);
        $form->setCategoriesOptions($this->generateCategoryOptions());
        $form->setTrademarksOptions($this->generateTrademarkOptions());
        $form->setProductsOptions($this->generateProductOptions());

        if ($this->offerId) {
            $form->setProducts($this->generateProductValues());
        }

        return $form;
    }

    /**
     * @return array
     */
    protected function generateCategoryOptions()
    {
        $categories = $this->categories->allCategories();

        $options = ['' => 'form.emptyOption'];
        array_walk($categories, function(Category $category) use (&$options) {
            $category = $category->render(new CategoryDto());
            $options[$category->id] = $category->name;
        });

        return $options;
    }

    /**
     * @return array
     */
    protected function generateTrademarkOptions()
    {
        $trademarks = $this->trademarks->allTrademarks();

        $options = ['' => 'form.emptyOption'];
        array_walk($trademarks, function(Trademark $trademark) use (&$options) {
            $trademark = $trademark->render(new TrademarkDto());
            $options[$trademark->id] = $trademark->name;
        });

        return $options;
    }

    /**
     * @return array
     */
    protected function generateProductValues()
    {
        $products = $this->products->productsBelongingToOffer($this->offerId);
        $options = [];
        array_walk($products, function(array $product) use (&$options) {
            $options[] = $product['productId'];
        });

        return $options;
    }

    /**
     * @return array
     */
    protected function generateProductOptions()
    {
        $products = $this->products->allProducts();

        $options = [];
        array_walk($products, function(Product $product) use (&$options) {
            $product = $product->render(new ProductDto());
            $options[$product->id] = $product->model;
        });

        return $options;
    }
}
