<?php
/**
 * Trademark  form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace CatalogModule\Forms;

use \ZendApplication\Form\Form;
use \ProductCatalog\Products\TrademarkDto;

/**
 * Trademark form
 */
class TrademarkForm extends Form
{
/**
     * @return CategoryForm
     */
    public function prepareForCreating()
    {
        $this->removeElement('id');
        $this->removeElement('version');

        return $this;
    }

    /**
     * @return void
     */
    public function prepareForEditing()
    {
        $this->getElement('name')->removeValidator('Db_Doctrine_NoRecordExists');
    }

    /**
     * @param Trademark $trademark
     */
    public function populateFromTrademark(TrademarkDto $trademark)
    {
        $this->populate($trademark->getArrayCopy());
    }
}
