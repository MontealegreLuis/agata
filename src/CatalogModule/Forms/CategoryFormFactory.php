<?php
/**
 * Factory for the category form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogModule\Forms;

use \CatalogDoctrine1\Products\CategoryTable;

/**
 * Factory for the category form
 *
 * It builds the hierarchy tree of categories
 */
class CategoryFormFactory
{
    /** @type array */
    protected $options;

    /** @type CategoryTable */
    protected $categoryTable;

    /**
     * @param array $options
     */
    public function __construct(array $options, CategoryTable $table)
    {
        $this->options = $options;
        $this->categoryTable = $table;
    }

    /**
     * @return \CatalogModule\Forms\CategoryForm
     */
    public function create()
    {
        $form = new CategoryForm($this->options);
        $form->setCategoryTree($this->buildCategoryTree());

        return $form;
    }

    /**
     * @return array
     */
    protected function buildCategoryTree()
    {
        $tree = [];
        $parents = $this->categoryTable->allParentCategories();
        foreach ($parents as $category) {
            $tree[$category['id']]['label'] = $category['name'];
            $tree[$category['id']]['children'] = $this->buildBranch($category['id']);
        }

        return $tree;
    }

    /**
     * @param  int   $categoryId
     * @return array
     */
    protected function buildBranch($categoryId)
    {
        $branch = [];
        $categories = $this->categoryTable->allChildrenOfCategoryId($categoryId);
        foreach ($categories as $childrenCategory) {
            $branch[$childrenCategory['id']]['label'] = $childrenCategory['name'];
            $branch[$childrenCategory['id']]['children'] = $this->buildBranch($childrenCategory['id']);
        }

        return $branch;
    }
}
