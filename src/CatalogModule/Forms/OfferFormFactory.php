<?php
/**
 * Factory for the offer form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogModule\Forms;

use ProductCatalog\Products\OfferType;

/**
 * Factory for the offer form
 *
 * It adds the offer type options
 */
class OfferFormFactory
{
    /** @type array */
    protected $options;

    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }

    /**
     * @return \CatalogModule\Forms\OfferForm
     */
    public function create()
    {
        $form = new OfferForm($this->options);
        $form->setOfferTypes($this->generateOfferTypeOptions());

        return $form;
    }

    /**
     * @param  Category[] $items
     * @return array
     */
    protected function generateOfferTypeOptions()
    {
        return [
            '' => 'form.emptyOption',
            OfferType::FIXED_AMOUNT => 'offer.type.fixed',
            OfferType::PERCENTAGE => 'offer.type.percentage',
            OfferType::SPECIAL_PRICE => 'offer.type.special',
        ];
    }
}
