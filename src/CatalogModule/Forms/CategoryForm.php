<?php
/**
 * Category  form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace CatalogModule\Forms;

use \ZendApplication\Form\Form;
use \RecursiveArrayIterator;
use \RecursiveIteratorIterator;
use \ProductCatalog\Products\CategoryDto;

/**
 * Category form
 */
class CategoryForm extends Form
{
    /** @type CategoryHydrator */
    protected $hydrator;

    /**
     * @return CategoryForm
     */
    public function prepareForCreating()
    {
        $this->removeElement('imageView');
        $this->removeElement('id');
        $this->removeElement('version');

        return $this;
    }

    /**
     * @return CategoryForm
     */
    public function prepareForEditing()
    {
        $this->getElement('name')->removeValidator('Db\Doctrine\NoRecordExists');

        return $this;
    }

    /**
     * @param Category $category
     */
    public function populateFromCategory(CategoryDto $category)
    {
        $category = $category->getArrayCopy();
        $category['imageView'] = "<img src='/images/categories/{$category['image']}' alt='{$category['name']}' />";

        if (!empty($category['parentCategory']) && $category['parentCategory'] instanceof CategoryDto ) {
            $category['parentCategoryId'] = $category['parentCategory']->id;
            unset($category['parentCategory']);
        }

        $this->populate($category);
    }

    /**
     * @return integer
     */
    public function getCategoryId()
    {
        return $this->getElement('id')->getValue();
    }

    /**
     * @param array $categoryTree
     */
    public function setCategoryTree(array $categoryTree)
    {
        $this->getElement('parentCategoryId')->setMultioptions($categoryTree);
        $this->setParentCategoryInArrayValidator($categoryTree);
    }

    /**
     * As options come in the form of a tree, iterate recursively to populate the
     * haystack for the InArray validator
     *
     * @param array $options
     */
    protected function setParentCategoryInArrayValidator(array $categoryTree)
    {
        $haystack = [];
        $it = new RecursiveIteratorIterator(
            new RecursiveArrayIterator($categoryTree), RecursiveIteratorIterator::SELF_FIRST
        );
        foreach ($it as $key => $value) {
            if (!in_array($key, ['label', 'children'])) {
                $haystack[] = $key;
            }
        }
        $this->getElement('parentCategoryId')->getValidator('InArray')->setHaystack($haystack);
    }

    /**
     * @param string $imageHtml
     */
    public function setImageView($imageHtml)
    {
        $this->getElement('imageView')->setValue($imageHtml);
    }

    /**
     * @return boolean
     */
    public function hasImage()
    {
        return null !== $this->getElement('image')->getValue();
    }

    /**
     * @param string $imagePath
     */
    public function saveImage($imagePath)
    {
        $this->saveFile('image', $imagePath);
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->getElement('image')->getValue();
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        return $this->getElement('name')->getValue();
    }

    /**
     * @param  array $errorMessages
     * @return void
     */
    public function setValidationErrors(array $errorMessages)
    {
        foreach ($errorMessages as $name => $message) {
            $this->getElement($name)->setErrors([$message]);
        }
    }
}
