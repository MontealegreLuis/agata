<?php
/**
 * Product  form
 *
 * PHP version 5.3
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace CatalogModule\Forms;

use \ZendApplication\Form\Form;
use \ProductCatalog\Products\ProductDto;

/**
 * Product form
 */
class ProductForm extends Form
{
    /**
     * @return ProductForm
     */
    public function prepareForCreating()
    {
        $this->removeElement('id');
        $this->removeElement('version');

        return $this;
    }

    /**
     * @param Product $product
     */
    public function populateFromProduct(ProductDto $product)
    {
        $product = array_change_key_case($product->getArrayCopy(), CASE_LOWER);
        $product['categoryId'] = $product['category']['id'];
        unset($product['category']);
        $product['trademarkId'] = $product['trademark']['id'];
        unset($product['trademark']);

        $this->populate($product);
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories)
    {
        $category = $this->getElement('categoryId');
        $category->getValidator('InArray')->setHaystack(array_keys($categories));
        $options = ['' => 'form.emptyOption'] + $categories;
        $category->setMultioptions($options);
    }

    /**
     * @param array $trademarks
     */
    public function setTrademarks(array $trademarks)
    {
        $trademark = $this->getElement('trademarkId');
        $trademark->getValidator('InArray')->setHaystack(array_keys($trademarks));
        $options = ['' => 'form.emptyOption'] + $trademarks;
        $trademark->setMultioptions($options);
    }

    /**
     * @return boolean
     */
    public function hasImage()
    {
        return null !== $this->getElement('image')->getValue();
    }

    /**
     * @param string $imagePath
     */
    public function saveImage($imagePath)
    {
        $this->saveFile('image', $imagePath);
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->getElement('image')->getValue();
    }
}
