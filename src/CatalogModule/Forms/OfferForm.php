<?php
/**
 * Offer form
 *
 * PHP version 5.3
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace CatalogModule\Forms;

use \ZendApplication\Form\Form;
use \ProductCatalog\Products\OfferDto;
use \ProductCatalog\Products\OfferType;

/**
 * Offer form
 */
class OfferForm extends Form
{
    /**
     * @return OfferForm
     */
    public function prepareForCreating()
    {
        $this->removeElement('id');
        $this->removeElement('version');

        return $this;
    }

    /**
     * @param Offer $offer
     */
    public function populateFromOffer(OfferDto $offer)
    {
        $offer = $offer->getArrayCopy();
        $offer['startDate'] = $offer['startDate'];
        $offer['stopDate'] = $offer['stopDate'];

        if (OfferType::PERCENTAGE !== (integer) $offer['type']) {
            $offer['amount'] = $offer['amount'] / 100;
        }

        $this->populate($offer);
    }

    /**
     * @param array $offerTypes
     */
    public function setOfferTypes(array $offerTypes)
    {
        $this->getElement('type')->setMultioptions($offerTypes);
    }

    /**
     * @return boolean
     */
    public function hasImage()
    {
        return null !== $this->getElement('image')->getValue();
    }

    /**
     * @param string $imagePath
     */
    public function saveImage($imagePath)
    {
        $this->saveFile('image', $imagePath);
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->getElement('image')->getValue();
    }

    public function setTypeAsReadOnly()
    {
        $this->getElement('type')
             ->setAttrib('disabled', 'disabled')
             ->setRequired(false)
             ->removeValidator('InArray');
    }
}
