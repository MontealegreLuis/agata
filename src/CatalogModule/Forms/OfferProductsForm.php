<?php
/**
 * Offer products form
 *
 * PHP version 5.3
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace CatalogModule\Forms;

use \ZendApplication\Form\Form;

/**
 * Offer products form
 */
class OfferProductsForm extends Form
{
    /**
     * @param array $categories
     */
    public function setCategoriesOptions(array $categories)
    {
        $category = $this->getElement('categoryId');
        $category->getValidator('InArray')->setHaystack(array_keys($categories));
        $options = ['' => 'form.emptyOption'] + $categories;
        $category->setMultioptions($options);
    }

    /**
     * @param array $trademarks
     */
    public function setTrademarksOptions(array $trademarks)
    {
        $trademark = $this->getElement('trademarkId');
        $trademark->getValidator('InArray')->setHaystack(array_keys($trademarks));
        $options = ['' => 'form.emptyOption'] + $trademarks;
        $trademark->setMultioptions($options);
    }

    /**
     * @param array $products
     */
    public function setProductsOptions(array $products)
    {
        $product = $this->getElement('products');
        $product->getValidator('InArray')->setHaystack(array_keys($products));
        $product->setMultioptions($products);
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products)
    {
        $this->getElement('products')->setValue($products);
    }

    /**
     * @param integer $offerId
     */
    public function setOfferId($offerId)
    {
        $this->getElement('id')->setValue($offerId);
    }
}
