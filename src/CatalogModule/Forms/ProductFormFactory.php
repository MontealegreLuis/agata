<?php
/**
 * Factory for the product form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogModule\Forms;

use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\Category;
use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\CategoryDto;
use \ProductCatalog\Products\TrademarkDto;

/**
 * Factory for the product form
 *
 * It adds the options of categories and trademarks.
 */
class ProductFormFactory
{
    /** @type array */
    protected $options;

    /** @type CategoryRepository */
    protected $categoryRepository;

    /** @type TrademarkRepository */
    protected $trademarkRepository;

    /**
     * @param array               $options
     * @param CategoryRepository  $categoryRepository
     * @param TrademarkRepository $trademarkRepository
     */
    public function __construct(
        array $options,
        CategoryRepository $categoryRepository,
        TrademarkRepository $trademarkRepository
    )
    {
        $this->options = $options;
        $this->categoryRepository = $categoryRepository;
        $this->trademarkRepository = $trademarkRepository;
    }

    /**
     * @return \CatalogModule\Forms\ProductForm
     */
    public function create()
    {
        $form = new ProductForm($this->options);
        $form->setCategories(
            $this->generateCategoryOptions($this->categoryRepository->allCategories())
        );
        $form->setTrademarks(
            $this->generateTrademarkOptions($this->trademarkRepository->allTrademarks())
        );

        return $form;
    }

    /**
     * @param  Category[] $items
     * @return array
     */
    protected function generateCategoryOptions(array $items)
    {
        $options = [];
        array_walk($items, function(Category $category) use (&$options) {
            $category = $category->render(new CategoryDto());
            $options[$category->id] = $category->name;
        });

        return $options;
    }

    /**
     * @param  Trademark[] $items
     * @return array
     */
    protected function generateTrademarkOptions(array $items)
    {
        $options = [];
        array_walk($items, function(Trademark $trademark) use (&$options) {
            $trademark = $trademark->render(new TrademarkDto());
            $options[$trademark->id] = $trademark->name;
        });

        return $options;
    }
}
