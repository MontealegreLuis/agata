<?php
return [
    'landing' => [
        'type' => 'Zend_Controller_Router_Route_Static',
        'route' => '/',
        'defaults' => [
            'module' => 'default',
            'controller' => 'index',
            'action' => 'index',
        ],
    ],
    'default' => [
        'route' => '/:@action',
        'defaults' => [
            'module' => 'default',
            'controller' => 'index',
        ],
    ],
    'category' => [
        'route' => '/productos/:category/@list/*',
        'defaults' => [
            'module' => 'default',
            'controller' => 'product',
            'action' => 'find-by-category',
        ],
    ],
    'product' => [
        'route' => '/productos/:category/:trademark/:product',
        'defaults' => [
            'module' => 'default',
            'controller' => 'product',
            'action' => 'find-by-url',
        ],
    ],
];
