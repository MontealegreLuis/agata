<?php
return [
    'categories' => [
        'type' => 'mvc',
        'module' => 'default',
        'controller' => 'index',
        'action' => 'index',
        'route' => 'landing',
        'label' => 'action.categories',
        'pages' => [
            'products' => [
                'type' => 'mvc',
                'module' => 'default',
                'controller' => 'product',
                'action' => 'find-by-category',
                'label' => 'action.listByCategory',
                'route' => 'category',
                'params' => [
                   'category' => 'category',
                ],
                'pages' => [
                   'product' => [
                       'type' => 'mvc',
                       'module' => 'default',
                        'controller' => 'product',
                        'action' => 'find-by-url',
                        'label' => 'action.show',
                        'route' => 'product',
                        'params' => [
                           'product' => 'product',
                           'trademark' => 'trademark',
                           'category' => 'category',
                       ],
                   ],
               ],
           ],
       ],
    ],
];
