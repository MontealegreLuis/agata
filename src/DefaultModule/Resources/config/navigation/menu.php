<?php
return [
    'products' => [
        'label' => 'Básculas',
        'controller' => 'product',
        'action' => 'find-by-category',
        'module' => 'default',
        'route' => 'category',
        'params' => [
            'category' => 'basculas',
          ],
     ],
    'contact' => [
        'label' => 'Contacto',
        'controller' => 'index',
        'action' => 'contact',
        'module' => 'default',
        'route' => 'default',
    ],
    'about' => [
        'label' => 'Nosotros',
        'controller' => 'index',
        'action' => 'about',
        'module' => 'default',
        'route' => 'default',
    ],
];
