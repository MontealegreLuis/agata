<?php
/**
 * Static page that shows Agata's contact information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace DefaultModule\Controllers;

use \ZendApplication\Controller\Action\ControllerAction;

/**
 * Static page that shows Agata's contact information
 */
class ContactController extends ControllerAction
{
    public function contactAction() {}
}
