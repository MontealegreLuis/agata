<?php
/**
 * Application's error controller
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace DefaultModule\Controllers;

use \ZendApplication\Controller\Action\ControllerAction;
use \Zend_Controller_Plugin_ErrorHandler as ErrorHandler;
use \Zend_Log as Logger;
use \ArrayObject;

/**
 * Logs the error to all the registered log writers.
 */
class ErrorController extends ControllerAction
{
    /** @type Logger */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Default action performed whenever an error occurs
     *
     * @return void
     */
    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');
        $this->addErrorInformation($errors);
        switch ($errors->type) {
            case ErrorHandler::EXCEPTION_NO_ROUTE:
            case ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case ErrorHandler::EXCEPTION_NO_ACTION:
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->httpResponseCode = 404;
                $this->view->title = 'No se encontró la página';
                $this->view->message = 'La página que está buscando no existe.';
                break;
            default:
                $this->view->title = 'Ha ocurrido un error';
                if ($errors->exception->getCode() == 403) {
                    $this->getResponse()->setHttpResponseCode(403);
                    $this->view->httpResponseCode = 403;
                    $this->renderScript('error/unauthorized.twig');
                } else {
                    $this->getResponse()->setHttpResponseCode(500);
                    $this->view->httpResponseCode = 500;
                    $this->view->message = 'Esta página está temporalmente fuera de servicio.';
                }

                $this->logger->err($this->view->render('error/mail.twig'));
                break;
        }
    }

    /**
     * @param ArrayObject $errors
     */
    protected function addErrorInformation(ArrayObject $errors)
    {
        $this->view->exceptionClass = get_class($errors->exception);
        $this->view->exception = $errors->exception;
        $this->view->request = $errors->request;
        if ($this->_request->getParam('password')) {
            $this->_request->setParam('password', null);
        }
        $this->view->requestParams = var_export($errors->request->getParams(), true);
    }
}
