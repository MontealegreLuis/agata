<?php
/**
 * Controller to show a product's information given its URL
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace DefaultModule\Controllers;

use \Zend_Controller_Action_Exception as ActionException;
use \Zend\EventManager\EventManagerInterface;
use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\ListenerAggregateTrait;
use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Products\GetProductByUrl;
use \ProductCatalog\Products\ProductEvent;

/**
 * Controller to show a product's information given its URL
 */
class ShowProductController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /** @type GetProductByUrl */
    protected $useCase;

    /**
     * @param GetProductByUrl $getProductByUrl
     */
    public function setUseCase(GetProductByUrl $getProductByUrl)
    {
        $this->useCase = $getProductByUrl;
    }

    /**
     * Get all the available products
     */
    public function findByUrlAction()
    {
        $response = $this->useCase->getProduct($this->param('product'));
        $this->view->product = $response->product;
    }

    /**
     * @throws ActionException
     */
    public function onProductNotFound()
    {
        throw new ActionException("Product with url '{$this->param('url')}' cannot be found", 404);
    }

    /**
     * Configure breadcrumbs
     */
    public function onProductFound(ProductEvent $event)
    {
        $product = $event->getProduct();
        $listCategory = $this->view->breadcrumbs->findByAction('find-by-category');
        $listCategory->setParams(['category' => $this->param('category')]);
        $listCategory->setLabel($product->category->name);
        $productShow = $this->view->breadcrumbs->findByAction('find-by-url');
        $params = [
            'category' => $this->param('category'),
            'trademark' => $this->param('trademark'),
            'product' => $this->param('product'),
        ];
        $productShow->setParams($params);
        $productShow->setLabel($product->model);
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('ProductNotFound', [$this, 'onProductNotFound']);
        $this->listeners[] = $events->attach('ProductFound', [$this, 'onProductFound']);
    }
}
