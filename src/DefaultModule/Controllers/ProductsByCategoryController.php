<?php
/**
 * Controller to show the list of available products
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace DefaultModule\Controllers;

use \ZendApplication\Controller\Action\ControllerAction;
use \ProductCatalog\Catalog\Products\FilterProducts;
use ProductCatalog\Catalog\Products\FilterProductsRequest;

/**
 * Controller to show the list of available products
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class ProductsByCategoryController extends ControllerAction
{
    /** @type FilterProducts */
    protected $useCase;

    /**
     * @param FilterProducts $getAllProducts
     */
    public function setUseCase(FilterProducts $filterProducts)
    {
        $this->useCase = $filterProducts;
    }

    /**
     * Get the products related to the given category
     */
    public function findByCategoryAction()
    {
        $response = $this->useCase->getProductsThatMeetCriteria(
            new FilterProductsRequest($this->params())
        );
        $products = $response->products;
        if (count($products) > 0) {
            $listCategory = $this->view->breadcrumbs->findByAction('find-by-category');
            $listCategory->setParams(['category' => $this->param('category')]);
            $listCategory->setLabel($products[0]->category->name);
        }
        $this->view->items = $products;
    }
}
