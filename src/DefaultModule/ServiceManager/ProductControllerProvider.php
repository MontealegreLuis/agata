<?php
/**
 * Product's controllers configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace DefaultModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \ZendApplication\Paginator\Events\PaginationListener;
use \Doctrine1\QuerySpecifications\OnlyPage;
use \Doctrine1\QuerySpecifications\ChainedSpecification;
use \DefaultModule\Controllers\ProductsByCategoryController;
use \DefaultModule\Controllers\ShowProductController;
use \CatalogDoctrine1\Products\OnlyProductsOfCategoryUrl;
use \ProductCatalog\Catalog\Products\GetProductByUrl;
use \ProductCatalog\Products\ProductEvent;

/**
 * Product's controllers configuration
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class ProductControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'default.product';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('default.product.find-by-category', function(ServiceLocatorInterface $sl) {
            $controller = new ProductsByCategoryController($this->request, $this->response, $this->params);

            $paginator = $sl->get('paginator');
            $onlyPage = new OnlyPage(
                $this->request->getParam('page', 1), $paginator->getItemCountPerPage()
            );
            $specification = new ChainedSpecification([new OnlyProductsOfCategoryUrl(), $onlyPage]);

            $repository = $sl->get('product.repository');
            $repository->addSpecification($specification);
            $filterProducts = $sl->get('product.filter');
            $controller->setUseCase($filterProducts);

            $paginationListener = new PaginationListener($paginator, $repository);
            $controller->getEventManager()->attach('postDispatch', $paginationListener);
            $controller->getEventManager()->attach('postDispatch', $sl->get('default.menu.listener'));
            $controller->getEventManager()->attach('preDispatch', $sl->get('product.breadcrumbs.listener'));

            return $controller;
        });
        $serviceManager->setFactory('default.product.find-by-url', function(ServiceLocatorInterface $sl) {
            $controller = new ShowProductController($this->request, $this->response, $this->params);

            $repository = $sl->get('product.repository');
            $getProductByUrl = new GetProductByUrl($repository, new ProductEvent());
            $controller->setUseCase($getProductByUrl);
            $getProductByUrl->getEventManager()->attachAggregate($controller);

            $controller->getEventManager()->attach('postDispatch', $sl->get('default.menu.listener'));
            $controller->getEventManager()->attach('preDispatch', $sl->get('product.breadcrumbs.listener'));

            return $controller;
        });
    }
}
