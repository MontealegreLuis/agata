<?php
/**
 * Error's controllers configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace DefaultModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \DefaultModule\Controllers\ErrorController;

/**
 * Error's controllers configuration
 */
class ErrorControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'default.error';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('default.error.error', function(ServiceLocatorInterface $sl) {
            $controller = new ErrorController($this->request, $this->response, $this->params);
            $controller->setLogger($sl->get('logger'));

            return $controller;
        });
    }
}
