<?php
/**
 * Index's controllers configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace DefaultModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \DefaultModule\Controllers\AboutController;
use \DefaultModule\Controllers\ContactController;
use \DefaultModule\Controllers\ListCategoriesController;
use \CatalogDoctrine1\Products\OnlyCategoriesWithProducts;
use \ProductCatalog\Catalog\Categories\GetAllCategories;

/**
 * Index's controllers configuration
 */
class IndexControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'default.index';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('default.index.index', function(ServiceLocatorInterface $sl) {
            $controller = new ListCategoriesController($this->request, $this->response, $this->params);

            $repository = $sl->get('category.repository');
            $repository->addSpecification(new OnlyCategoriesWithProducts());
            $getCategories = new GetAllCategories($repository);
            $controller->setUseCase($getCategories);

            $controller->getEventManager()->attach('postDispatch', $sl->get('default.menu.listener'));

            return $controller;
        });
        $serviceManager->setFactory('default.index.about', function(ServiceLocatorInterface $sl) {
            $controller = new AboutController($this->request, $this->response, $this->params);

            $controller->getEventManager()->attach('postDispatch', $sl->get('default.menu.listener'));

            return $controller;
        });
        $serviceManager->setFactory('default.index.contact', function(ServiceLocatorInterface $sl) {
            $controller = new ContactController($this->request, $this->response, $this->params);

            $controller->getEventManager()->attach('postDispatch', $sl->get('default.menu.listener'));

            return $controller;
        });
    }
}
