<?php
/**
 * Service configuration for the catalog module
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace DefaultModule\ServiceManager;

use \Zend\ServiceManager\Config;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\Controller\Action\Events\Navigation\MainMenuListener;
use \ZendApplication\Controller\Action\Events\Navigation\BreadcrumbsListener;

/**
 * Service configuration for the catalog module
 */
class DefaultModuleProvider extends Config
{
    /**
     * Initialize service configuration
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'default.menu.listener' => function(ServiceLocatorInterface $sl) {
                return new MainMenuListener(
                    $sl->get('default.menu.configuration'),
                    $sl->get('cache.manager')->getCache('default')
                );
            },
            'product.breadcrumbs.listener' => function(ServiceLocatorInterface $sl){
                return new BreadcrumbsListener($sl->get('product.breadcrumbs.configuration'));
            },
        ];
        $this->config['services'] = [
            'product.breadcrumbs.configuration' => require __DIR__ . '/../Resources/config/navigation/breadcrumbs.php',
            'default.menu.configuration' => require __DIR__ . '/../Resources/config/navigation/menu.php',
        ];
    }
}
