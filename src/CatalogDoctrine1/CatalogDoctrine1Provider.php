<?php
/**
 * Service configuration for the catalog module
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogDoctrine1;

use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\Config;
use \CatalogDoctrine1\Products\CategoryTable;
use \CatalogDoctrine1\Products\CategoryRecord;
use \CatalogDoctrine1\Products\CategoryHydrator;
use \CatalogDoctrine1\Products\ProductTable;
use \CatalogDoctrine1\Products\ProductRecord;
use \CatalogDoctrine1\Products\ProductHydrator;
use \CatalogDoctrine1\Products\TrademarkTable;
use \CatalogDoctrine1\Products\TrademarkRecord;
use \CatalogDoctrine1\Products\TrademarkHydrator;
use \CatalogDoctrine1\Products\OfferTable;
use \CatalogDoctrine1\Products\OfferRecord;
use \CatalogDoctrine1\Products\OfferHydrator;

/**
 * Service configuration for the catalog module
 */
class CatalogDoctrine1Provider extends Config
{
    /**
     * Initialize service configuration for category, product, trademark and offer entities
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->configureCategory();
        $this->configureProduct();
        $this->configureTrademark();
        $this->configureOffer();
    }

    /**
     * Register services related to the Category entity
     */
    protected function configureCategory()
    {
        $this->config['factories'] = [
            'category.repository' => function(ServiceLocatorInterface $sl) {
                $connection = $sl->get('connection');

                $categoryRepository = new CategoryTable($sl->get('category.record'));
                $hydrator = $sl->get('category.hydrator');
                $hydrator->setRepository($categoryRepository);
                $categoryRepository->setHydrator($hydrator);

                return $categoryRepository;
            },
            'category.record' => function(ServiceLocatorInterface $sl){
                return new CategoryRecord();
            },
            'category.hydrator' => function(ServiceLocatorInterface $sl) {
                return new CategoryHydrator();
            },
        ];
    }

    /**
     * Register services related to the Product entity
     */
    protected function configureProduct()
    {
        $factories = [
            'product.repository' => function(ServiceLocatorInterface $sl) {
                $connection = $sl->get('connection');
                $repository = new ProductTable($sl->get('product.record'));
                $repository->setHydrator($sl->get('product.hydrator'));

                return $repository;
            },
            'product.record' => function(ServiceLocatorInterface $sl){
                return new ProductRecord();
            },
            'product.hydrator' => function(ServiceLocatorInterface $sl) {
                $productHydrator = new ProductHydrator();
                $productHydrator->setTrademarkRepository($sl->get('trademark.repository'));
                $productHydrator->setTrademarkHydrator($sl->get('trademark.hydrator'));
                $productHydrator->setCategoryRepository($sl->get('category.repository'));
                $productHydrator->setCategoryHydrator($sl->get('category.hydrator'));

                return $productHydrator;
            },

        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);

    }

    /**
     * Register services related to the Trademark entity
     */
    protected function configureTrademark()
    {
        $factories = [
            'trademark.repository' => function(ServiceLocatorInterface $sl) {
                $connection = $sl->get('connection');

                $repository = new TrademarkTable($sl->get('trademark.record'));
                $repository->setHydrator($sl->get('trademark.hydrator'));

                return $repository;
            },
            'trademark.record' => function(ServiceLocatorInterface $sl){
                return new TrademarkRecord();
            },
            'trademark.hydrator' => function(ServiceLocatorInterface $sl) {
                return new TrademarkHydrator();
            },
        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);
    }

    /**
     * Register services related to the Offer entity
     */
    protected function configureOffer()
    {
        $factories = [
            'offer.repository' => function(ServiceLocatorInterface $sl) {
                $connection = $sl->get('connection');
                $repository = new OfferTable($sl->get('offer.record'));
                $repository->setHydrator($sl->get('offer.hydrator'));

                return $repository;
            },
            'offer.hydrator' => function(ServiceLocatorInterface $sl) {
                return new OfferHydrator($sl->get('offer.factory'));
            },
            'offer.record' => function(ServiceLocatorInterface $sl){
                return new OfferRecord();
            },
        ];
        $this->config['factories'] = array_merge($this->config['factories'], $factories);
    }
}
