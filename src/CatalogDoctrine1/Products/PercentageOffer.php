<?php
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \ProductCatalog\Products\DateRange;
use \ProductCatalog\Products\OfferType;
use \ProductCatalog\Products\Percentage;
use \ProductCatalog\Products\OfferDto;
use \ProductCatalog\Products\PercentageOffer as BasePercentageOffer;

class PercentageOffer extends BasePercentageOffer
{
    /** @type integer */
    protected $version;

    /** @type Percentage */
    protected $percentage;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param Percentage  $price
     * @param SplFileInfo $image
     * @param integer     $id
     * @param integer     $version
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        Percentage $percentage,
        SplFileInfo $image = null,
        $id = null,
        $version = null
    )
    {
        parent::__construct($duration, $type, $description, $percentage, $image, $id);
        $this->version = $version;
    }

    /**
     * @param  OfferDto $view
     * @return OfferDto
     */
    public function render(OfferDto $view)
    {
        $view = parent::render($view);
        $view->amount = $this->percentage->getPercentage();
        $view->version = $this->version;

        return $view;
    }
}
