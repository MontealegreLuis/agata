<?php
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \ProductCatalog\Products\DateRange;
use \ProductCatalog\Products\OfferType;
use \ProductCatalog\Products\OfferFactory as FactoryBase;

class OfferFactory extends FactoryBase
{
    /**
     * @param  DateRange          $duration
     * @param  OfferType          $type
     * @param  string             $description
     * @param  Money | Percentage $amount
     * @param  SplFileInfo        $image
     * @param  integer            $id
     * @param  integer            $version
     * @return Offer
     */
    public function create(
        DateRange $duration,
        OfferType $type,
        $description,
        $amount,
        SplFileInfo $image = null,
        $id = null,
        $version = null
    )
    {
        if ($type->getType() === OfferType::FIXED_AMOUNT) {
            return new AmountOffer($duration, $type, $description, $amount, $image, $id, $version);
        }
        if ($type->getType() === OfferType::PERCENTAGE) {
            return new PercentageOffer($duration, $type, $description, $amount, $image, $id, $version);
        }

        return new SpecialPriceOffer($duration, $type, $description, $amount, $image, $id, $version);
    }
}
