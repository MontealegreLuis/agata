<?php
/**
 * Product hydrator
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \Doctrine1\Hydrator\ReflectionHydrator;
use \Doctrine1\Hydrator\Hydrator;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\CategoryRepository;
use \CatalogDoctrine1\Products\CategoryHydrator;
use \CatalogDoctrine1\Products\TrademarkHydrator;
use \CatalogDoctrine1\Products\NullTrademark;
use \CatalogDoctrine1\Products\NullCategory;
use \Money\Money;
use \Money\Currency;

/**
 * Product hydrator
 */
class ProductHydrator extends ReflectionHydrator implements Hydrator
{
    /** @type CategoryHydrator */
    protected $categoryHydrator;

    /** @type TrademarkHydrator */
    protected $trademarkHydrator;

    /** @type TrademarkRepository */
    protected $trademarkRepository;

    /** @type CategoryRepository */
    protected $categoryRepository;

    /**
     * @param TrademarkRepository $trademarkRepository
     */
    public function setTrademarkRepository(TrademarkRepository $trademarkRepository)
    {
        $this->trademarkRepository = $trademarkRepository;
    }

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function setCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param CategoryHydrator
     */
    public function setCategoryHydrator(CategoryHydrator $categoryHydrator)
    {
        $this->categoryHydrator = $categoryHydrator;
    }

    /**
     * @param TrademarkHydrator $trademarkHydrator
     */
    public function setTrademarkHydrator(TrademarkHydrator $trademarkHydrator)
    {
        $this->trademarkHydrator = $trademarkHydrator;
    }

    /**
     * @see \ZendApplication\Hydrator\Hydrator::hydrate()
     */
    public function hydrate(array $data)
    {
        $trademark = $this->hydrateTrademark($data);
        $category = $this->hydrateCategory($data);

        $url = null;
        if (!empty($data['url'])) {
            $url = $data['url'];
        }
        $image = null;
        if (!empty($data['image'])) {
            $image = new SplFileInfo($data['image']);
        }
        $id = null;
        if (!empty($data['id'])) {
            $id = $data['id'];
        }
        $version = null;
        if (!empty($data['version'])) {
            $version = $data['version'];
        }
        $product = new Product(
            $data['model'],
            $data['features'],
            new Money((integer) $data['price'], new Currency($data['currency'])),
            $trademark,
            $category,
            $image,
            $url,
            $id,
            $version
        );

        return $product;
    }

    /**
     * @param  array                   $product
     * @return Trademark|NullTrademark
     */
    protected function hydrateTrademark(array $product)
    {
        if (isset($product['Trademark'])) {
            return $this->trademarkHydrator->hydrate($product['Trademark']);
        }
        if ($trademark = $this->trademarkRepository->trademarkOfId($product['trademarkId'])) {
            return $trademark;
        }

        return new NullTrademark();
    }

    /**
     * @param  array                 $product
     * @return Category|NullCategory
     */
    protected function hydrateCategory(array $product)
    {
        if (isset($product['Category'])) {
            return $this->categoryHydrator->hydrate($product['Category']);
        }
        if ($category = $this->categoryRepository->categoryOfId($product['categoryId'])) {
            return $category;
        }

        return new NullCategory();
    }

    /**
     * @see \ZendApplication\Hydrator\Hydrator::extract()
     */
    public function extract($product)
    {
        $product = parent::extract($product);
        $trademark = $this->trademarkHydrator->extract($product['trademark']);
        $category = $this->categoryHydrator->extract($product['category']);

        $product['trademarkId'] = $trademark['id'];
        $product['categoryId'] = $category['id'];
        $product['currency'] = $product['price']->getCurrency()->getName();
        $product['price'] = $product['price']->getAmount();

        unset($product['trademark']);
        unset($product['category']);
        unset($product['eventsBus']);

        return $product;
    }
}
