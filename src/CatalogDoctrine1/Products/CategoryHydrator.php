<?php
/**
 * Hydrator for Category entities.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \Doctrine1\Hydrator\Hydrator;
use \Doctrine1\Hydrator\ReflectionHydrator;
use \ProductCatalog\Products\CategoryRepository;

/**
 * Hydrator for Category entities.
 */
class CategoryHydrator extends ReflectionHydrator implements Hydrator
{
    /** @type CategoryRepository */
    protected $repository;

    /**
     * @param CategoryRepository $repository
     */
    public function setRepository(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @see \Doctrine1\Hydrator\Hydrator::hydrate()
     */
    public function hydrate(array $data)
    {
        $url = null;
        if (!empty($data['url'])) {
            $url = $data['url'];
        }
        $image = null;
        if (!empty($data['image'])) {
            $image =new SplFileInfo($data['image']);
        }
        $parentCategory = null;
        if (!empty($data['ParentCategory'])) {
            $parentCategory = $this->hydrate($data['ParentCategory']);
        } elseif (!empty($data['parentCategoryId'])) {
            $parentCategory = $this->repository->categoryOfId($data['parentCategoryId']);
        }
        $id = null;
        if (!empty($data['id'])) {
            $id = $data['id'];
        }
        $version = null;
        if (!empty($data['version'])) {
            $version = $data['version'];
        }
        $productCount = null;
        if (!empty($data['productCount'])) {
            $productCount = $data['productCount'];
        }

        $category = new Category(
            $data['name'], $image, $parentCategory, $url, $productCount, $id, $version
        );

        return $category;
    }

    /**
     * @return Category
     * @see \Doctrine1\Hydrator\Hydrator::extract()
     */
    public function extract($category)
    {
        $category = parent::extract($category);
        if (!empty($category['image'])) {
            $category['image'] = $category['image']->getBaseName();
        }
        if (!empty($category['parentCategory'])) {
            $parentCategory = parent::extract($category['parentCategory']);
            $category['parentCategoryId'] = $parentCategory['id'];
        } else {
            $category['parentCategoryId'] = null;
        }
        unset($category['parentCategory']);
        unset($category['productCount']);
        unset($category['events']);

        return $category;
    }
}
