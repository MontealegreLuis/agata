<?php
/**
 * ProductRecord
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author    LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Record as Record;
use \Doctrine_Manager as Manager;
use \ZendApplication\Filter\ProvidesSlugBuilders;
use \Doctrine1\Listener\UpdateVersion;
use \Doctrine1\Listener\BuildSlug;
use \Doctrine1\Record\HasVersion;
use \Doctrine1\Record\ProvidesVersionNumber;

Manager::getInstance()->bindComponent('CatalogDoctrine1\Products\ProductRecord', 'doctrine');

/**
 * ProductRecord
 *
 * @property integer $id
 * @property string $model
 * @property string $features
 * @property integer $trademarkId
 * @property integer $categoryId
 * @property integer $version
 * @property string $image
 * @property CatalogDoctrine1\Products\CategoryRecord  $Category
 * @property CatalogDoctrine1\Products\TrademarkRecord $Trademark
 */
class ProductRecord extends Record implements HasVersion
{
    use ProvidesSlugBuilders, ProvidesVersionNumber;

    /**
     * @see Doctrine_Record_Abstract::setTableDefinition()
     */
    public function setTableDefinition()
    {
        $this->setTableName('product');
        $this->hasColumn('id', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
        ]);
        $this->hasColumn('model', 'string', 100, [
             'type' => 'string',
             'length' => 100,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('url', 'string', 100, [
             'type' => 'string',
             'length' => 100,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('features', 'string', null, [
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('image', 'string', 100, [
             'type' => 'string',
             'length' => 100,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
        ]);
        $this->hasColumn('price', 'integer', 4, [
            'type' => 'integer',
            'length' => 4,
            'fixed' => false,
            'unsigned' => false,
            'primary' => false,
            'autoincrement' => false,
            'notnull' => true,
        ]);
        $this->hasColumn('currency', 'string', 3, [
            'type' => 'string',
            'length' => 3,
            'fixed' => true,
            'unsigned' => false,
            'primary' => false,
            'notnull' => true,
            'autoincrement' => false,
            'default' => 'MXN',
        ]);
        $this->hasColumn('trademarkId', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('categoryId', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('version', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
    }

    /**
     * @see Doctrine_Record::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        $this->hasOne('CatalogDoctrine1\Products\CategoryRecord as Category', [
             'local' => 'categoryId',
             'foreign' => 'id'
        ]);

        $this->hasOne('CatalogDoctrine1\Products\TrademarkRecord as Trademark', [
             'local' => 'trademarkId',
             'foreign' => 'id'
        ]);
        $this->addListener(new BuildSlug('url', 'model', $this->createSlugBuilder()));
        $this->addListener(new BuildSlug('image', 'model', $this->createSlugBuilder('.jpg')));
        $this->addListener(new UpdateVersion('version'));
    }
}
