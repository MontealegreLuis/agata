<?php
/**
 * Gateway for Offer model objects
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Core as Doctrine;
use \Doctrine_Collection as Collection;
use \Doctrine1\TableGateway\Table;
use \Doctrine1\Hydrator\ProvidesHydration;
use \Countable;
use \ProductCatalog\Products\OfferRepository;
use \ProductCatalog\Products\Offer;

/**
 * Gateway for Offer model objects
 */
class OfferTable extends Table implements OfferRepository, Countable
{
    use ProvidesHydration;

    /**
     * @param  Offer $offer
     * @return Offer
     */
    public function persist(Offer $offer)
    {
        $offer = $this->extract($offer);

        if (!empty($offer['id'])) {
            $offer = $this->update($offer, ['id' => $offer['id']]);
        } else {
            $offer = $this->insert($offer);
        }

        return $this->hydrate($offer);
    }

    /**
     * @param  integer $offerId
     * @return Offer
     */
    public function offerOfId($offerId)
    {
        $query = $this->createQuery('o');
        $query->where('o.id = :id');

        $offer = $query->fetchOne([':id' => (int) $offerId], Doctrine::HYDRATE_ARRAY);

        if ($offer) {
            return $this->hydrate($offer);
        }
    }

    /**
     * @return Offer[]
     */
    public function allOffers()
    {
        $query = $this->createQuery('o');

        $this->match($query);

        return $this->hydrateAll($query->fetchArray());
    }

    /**
     * @see Countable::count()
     */
    public function count()
    {
        $query = $this->createQuery('o');
        $query->select('COUNT(o.id)');

        return $query->fetchOne([], Doctrine::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * @param array   $criteria
     * @param integer $offerId
     */
    public function applyToProductsThatMeet(array $criteria, $offerId)
    {
        $this->cleanUpOfferProducts($offerId);

        $query = $this->createQuery();
        $query->select('p.id')
              ->from('CatalogDoctrine1\Products\ProductRecord p')
              ->innerJoin('p.Trademark t')
              ->innerJoin('p.Category c');

        $this->match($query, $criteria);

        $products = $query->fetchArray($criteria);
        array_walk($products, function(array &$product) use ($offerId) {
            $product = [
                'productId' => $product['id'],
                'offerId' => $offerId,
            ];
        });

        $collection = new Collection('CatalogDoctrine1\Products\OfferProductRecord');
        foreach ($products as $offerProduct) {
            $record = new OfferProductRecord();
            $record->productId = $offerProduct['productId'];
            $record->offerId = $offerProduct['offerId'];

            $collection->add($record);
        }

        $collection->save();
    }

    /**
     * @param array   $products
     * @param integer $offerId
     */
    public function applyToProducts(array $products, $offerId)
    {
        $this->cleanUpOfferProducts($offerId);

        $collection = new Collection('CatalogDoctrine1\Products\ProductRecord');
        foreach ($products as $product) {
            $record = new OfferProductRecord();
            $record->productId = (integer) $product;
            $record->offerId = $offerId;

            $collection->add($record);
        }

        $collection->save();
    }

    /**
     * @param  Offer $offer
     */
    public function remove(Offer $offer)
    {
        $offer = $this->hydrator->extract($offer);
        $this->cleanUpOfferProducts($offer['id']);
        $this->delete(['id' => $offer['id']]);
    }

    /**
     * @param integer $offerId
     */
    protected function cleanUpOfferProducts($offerId)
    {
        $query = $this->createQuery();
        $query->delete('CatalogDoctrine1\Products\OfferProductRecord pr')
              ->where('pr.offerId = :offerId');
        $query->execute([':offerId' => $offerId]);
    }
}
