<?php
/**
 * Gateway for Trademark model objects
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Core as Doctrine;
use \Doctrine1\TableGateway\Table;
use \Doctrine1\Hydrator\ProvidesHydration;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\Trademark;
use \Countable;

/**
 * Gateway for Trademark model objects
 */
class TrademarkTable extends Table implements TrademarkRepository, Countable
{
    use ProvidesHydration;

    /**
     * @return array
     */
    public function allTrademarks()
    {
        $query = $this->createQuery('t');

        return $this->hydrateAll($query->fetchArray());
    }

    /**
     * @see Countable::count()
     */
    public function count()
    {
        $query = $this->createQuery('c');
        $query->select('COUNT(c.id)');

        return $query->fetchOne([], Doctrine::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * @return array
     */
    public function trademarkOfId($trademarkId)
    {
        $query = $this->createQuery('t');
        $query->where('t.id = :id');
        $trademark = $query->fetchOne([':id' => (int) $trademarkId], Doctrine::HYDRATE_ARRAY);

        if ($trademark) {
            return $this->hydrate($trademark);
        }
    }

    /**
     * @see \ProductCatalog\Trademark\TrademarkRepository::persist()
     */
    public function persist(Trademark $trademark)
    {
        $trademark = $this->extract($trademark);

        if (!empty($trademark['id'])) {
            $trademark = $this->update($trademark, ['id' => $trademark['id']]);
        } else {
            $trademark = $this->insert($trademark);
        }

        return $this->hydrate($trademark);
    }

    /**
     * @see \ProductCatalog\Trademark\TrademarkRepositor::remove()
     */
    public function remove(Trademark $trademark)
    {
        $trademark = $this->extract($trademark);

        $this->delete(['id' => $trademark['id']]);
    }
}
