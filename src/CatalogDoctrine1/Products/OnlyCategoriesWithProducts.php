<?php
/**
 * Only retrieve categories with at least one product
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine1\QuerySpecifications\Specification;
use \Doctrine_Query as Query;

/**
 * Only retrieve categories with at least one product
 */
class OnlyCategoriesWithProducts implements Specification
{
    /**
     * Only retrieve categories with at least one product
     *
     * @see \Doctrine1\QuerySpecifications\Specification::match()
     */
    public function match(Query $query, array $criteria = [])
    {
        $query->select('c.*, p.id, COUNT(p.id) AS productCount')
              ->leftJoin('c.Product p')
              ->groupBy('c.id')
              ->having('productCount > 0');

        return $query;
    }
}
