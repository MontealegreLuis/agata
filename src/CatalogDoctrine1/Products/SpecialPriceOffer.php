<?php
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \Money\Money;
use \ProductCatalog\Products\DateRange;
use \ProductCatalog\Products\OfferType;
use \ProductCatalog\Products\OfferDto;
use \ProductCatalog\Products\SpecialPriceOffer as BaseSpecialPriceOffer;

class SpecialPriceOffer extends BaseSpecialPriceOffer
{
    /** @type integer */
    protected $version;

    /** @type Money */
    protected $price;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param Money       $price
     * @param SplFileInfo $image
     * @param integer     $id
     * @param integer     $version
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        Money $price,
        SplFileInfo $image = null,
        $id = null,
        $version = null
    )
    {
        parent::__construct($duration, $type, $description, $price, $image, $id);
        $this->version = $version;
    }

    /**
     * @param  OfferDto $view
     * @return OfferDto
     */
    public function render(OfferDto $view)
    {
        $view = parent::render($view);
        $view->amount = $this->price->getAmount();
        $view->version = $this->version;

        return $view;
    }
}
