<?php
/**
 * OfferProductRecord
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Record as Record;
use \Doctrine_Manager as Manager;

Manager::getInstance()->bindComponent('CatalogDoctrine1\Products\OfferProductRecord', 'doctrine');

/**
 * OfferProductRecord
 *
 * @property integer $offerId
 * @property integer $productId
 * @property CatalogDoctrine1\Products\ProductRecord $Product
 * @property CatalogDoctrine1\Products\OfferRecord $Offer
 */
class OfferProductRecord extends Record
{
    /**
     * @see Doctrine_Record_Abstract::setTableDefinition()
     */
    public function setTableDefinition()
    {
        $this->setTableName('offer_product');
        $this->hasColumn('offerId', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('productId', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
        ]);
    }

    /**
     * @see Doctrine_Record::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        $this->hasOne('CatalogDoctrine1\Products\ProductRecord as Product', [
             'local' => 'productId',
             'foreign' => 'id'
        ]);
        $this->hasOne('CatalogDoctrine1\Products\OfferRecord as Offer', [
             'local' => 'offerId',
             'foreign' => 'id'
        ]);
    }
}
