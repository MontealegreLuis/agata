<?php
/**
 * Trademark hydrator
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine1\Hydrator\Hydrator;
use \Doctrine1\Hydrator\ReflectionHydrator;

/**
 * Trademark hydrator
 */
class TrademarkHydrator extends ReflectionHydrator implements Hydrator
{
    /**
     * @see \CatalogDoctrine1\Hydrator\Hydrator::hydrate()
     */
    public function hydrate(array $data)
    {
        $id = null;
        if (!empty($data['id'])) {
            $id = $data['id'];
        }
        $url = null;
        if (!empty($data['url'])) {
            $url = $data['url'];
        }
        $version = null;
        if (!empty($data['version'])) {
            $version = $data['version'];
        }
        $trademark = new Trademark($data['name'], $url, $id, $version);

        return $trademark;
    }
}
