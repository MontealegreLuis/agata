<?php
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \Money\Money;
use \ProductCatalog\Products\DateRange;
use \ProductCatalog\Products\OfferType;
use \ProductCatalog\Products\OfferDto;
use \ProductCatalog\Products\AmountOffer as BaseAmountOffer;

class AmountOffer extends BaseAmountOffer
{
    /** @type integer */
    protected $version;

    /** @type Money */
    protected $amount;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param Money       $amount
     * @param SplFileInfo $image
     * @param integer     $id
     * @param integer     $version
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        Money $amount,
        SplFileInfo $image = null,
        $id = null,
        $version = null
    )
    {
        parent::__construct($duration, $type, $description, $amount, $image, $id);
        $this->version = $version;
    }

    /**
     * @param  OfferDto $view
     * @return OfferDto
     */
    public function render(OfferDto $view)
    {
        $view = parent::render($view);
        $view->version = $this->version;
        $view->amount = $this->amount->getAmount();

        return $view;
    }
}
