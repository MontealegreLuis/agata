<?php
/**
 * Only retrieve products of a given category URL
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine1\QuerySpecifications\Specification;
use \Doctrine_Query as Query;

/**
 * Only retrieve products of a given category URL
 */
class OnlyProductsOfCategoryUrl implements Specification
{
    /**
     * @see \Doctrine1\QuerySpecifications\Specification::match()
     */
    public function match(Query $query, array $criteria = [])
    {
        $query->where('c.url = :categoryUrl');

        return $query;
    }
}
