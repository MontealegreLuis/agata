<?php
/**
 * Only retrieve products of a given category ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine1\QuerySpecifications\Specification;
use \Doctrine_Query as Query;

/**
 * Only retrieve products of a given category ID
 */
class OnlyProductsOfCategoryId implements Specification
{
    /**
     * @see \Doctrine1\QuerySpecifications\Specification::match()
     */
    public function match(Query $query, array $criteria = [])
    {
        if (empty($criteria[':categoryId'])) {
            return;
        }

        $query->andWhere('c.id = :categoryId');

        return $query;
    }
}
