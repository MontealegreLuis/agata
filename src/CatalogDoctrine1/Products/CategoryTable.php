<?php
/**
 * Category repository
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Core as Doctrine;
use \Doctrine1\TableGateway\Table;
use \Doctrine1\Hydrator\ProvidesHydration;
use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\Category;
use \Countable;

/**
 * Category repository
 */
class CategoryTable extends Table implements CategoryRepository, Countable
{
    use ProvidesHydration;

    /**
     * @return Category[]
     */
    public function allCategories()
    {
        $query = $this->createQuery('c');

        $this->match($query);

        return $this->hydrateAll($query->fetchArray());
    }

    /**
     * @see Countable::count()
     */
    public function count()
    {
        $query = $this->createQuery('c');
        $query->select('COUNT(c.id)');

        return $query->fetchOne([], Doctrine::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * Retrieve only parent categories
     *
     * @return array
     */
    public function allParentCategories()
    {
        $query = $this->createQuery('c');
        $query->where('c.parentCategoryId IS NULL');

        return $query->fetchArray();
    }

    /**
     * Retrieve all the children of a given parent category
     *
     * @param  int   $categoryId
     * @return array
     */
    public function allChildrenOfCategoryId($categoryId)
    {
        $query = $this->createQuery('c');
        $query->where('c.parentCategoryId = :parentCategoryId');

        return $query->fetchArray([':parentCategoryId' => $categoryId]);
    }

    /**
     * @return Category
     */
    public function categoryOfId($categoryId)
    {
        $query = $this->createQuery('c');
        $query->where('c.id = :id');
        $query->leftJoin('c.ParentCategory p');

        $category = $query->fetchOne([':id' => (int) $categoryId], Doctrine::HYDRATE_ARRAY);

        if ($category) {
            return $this->hydrate($category);
        }
    }

    /**
     * @see \ProductCatalog\Products\CategoryRepository::persist()
     */
    public function persist(Category $category)
    {
        $category = $this->extract($category);

        if (!empty($category['id'])) {
            $category = $this->update($category, ['id' => $category['id']]);
        } else {
            $category = $this->insert($category);
        }

        return $this->hydrate($category);
    }

    /**
     * @see \ProductCatalog\Products\CategoryRepository::remove()
     */
    public function remove(Category $category)
    {
        $category = $this->extract($category);

        $this->delete(['id' => $category['id']]);
    }
}
