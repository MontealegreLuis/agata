<?php
/**
 * Gateway for Product model objects
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Core as Doctrine;
use \Doctrine1\TableGateway\Table;
use \Doctrine1\Hydrator\ProvidesHydration;
use \ProductCatalog\Products\ProductRepository;
use \ProductCatalog\Products\Product;
use \Countable;

/**
 * Gateway for Product model objects
 */
class ProductTable extends Table implements ProductRepository, Countable
{
    use ProvidesHydration;

    /**
     * @return Product
     */
    public function allProducts(array $params = [])
    {
        $query = $this->createQuery('p');
        $query->innerJoin('p.Category c')
              ->innerJoin('p.Trademark t')
              ->orderBy('p.model');

        $this->match($query);

        return $this->hydrateAll($query->fetchArray($params));
    }

    /**
     * @return integer
     */
    public function count(array $params = [])
    {
        $query = $this->createQuery('p');

        $query->select('COUNT(p.id)')->innerJoin('p.Category c');

        $this->match($query);

        if (!empty($params['category'])) {
            $params = ['categoryUrl' => $params['category']];
        }

        return $query->fetchOne($params, Doctrine::HYDRATE_SINGLE_SCALAR);
    }

    /**
     * @return Product
     */
    public function productOfId($id)
    {
        $query = $this->createQuery('p');
        $query->innerJoin('p.Category c')
              ->innerJoin('p.Trademark t')
              ->where('p.id = :id');

        $product = $query->fetchOne([':id' => (int) $id], Doctrine::HYDRATE_ARRAY);

        if ($product) {
            return $this->hydrate($product);
        }
    }

    /**
     * @param  string $url
     * @return array
     */
    public function productOfUrl($url)
    {
        $query = $this->createQuery('p');
        $query->innerJoin('p.Category c')
              ->where('p.url = :url');

        return $this->hydrate($query->fetchOne(['url' => $url], Doctrine::HYDRATE_ARRAY));
    }

    /**
     * @param  array          $criteria
     * @return Doctrine_Query
     */
    public function productsThatMeetCriteria(array $criteria)
    {
        $query = $this->createQuery('p');

        $query->innerJoin('p.Category c')->innerJoin('p.Trademark t');

        $this->match($query, $criteria);

        return $this->hydrateAll($query->fetchArray($criteria));
    }

    /**
     * @see \ProductCatalog\Products\ProductRepository::persist()
     */
    public function persist(Product $product)
    {
        $product = $this->extract($product);
        if (!empty($product['id'])) {
            $product = $this->update($product, ['id' => $product['id']]);
        } else {
            $product = $this->insert($product);
        }

        return $this->hydrate($product);
    }

    /**
     * @see \ProductCatalog\Products\ProductRepository::remove()
     */
    public function remove(Product $product)
    {
        $product = $this->extract($product);

        $this->delete(['id' => $product['id']]);
    }

    /**
     * @param  integer $offerId
     * @return array
     */
    public function productsBelongingToOffer($offerId)
    {
        $query = $this->createQuery();
        $query->select('pr.productId')
              ->from('CatalogDoctrine1\Products\OfferProductRecord pr')
              ->where('pr.offerId = :offerId');

        return $query->fetchArray([':offerId' => $offerId]);
    }
}
