<?php
/**
 * Only retrieve products of a given trademark ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine1\QuerySpecifications\Specification;
use \Doctrine_Query as Query;

/**
 * Only retrieve products of a given trademark ID
 */
class OnlyProductsOfTrademarkId implements Specification
{
    /**
     * @see \Doctrine1\QuerySpecifications\Specification::match()
     */
    public function match(Query $query, array $criteria = [])
    {
        if (empty($criteria[':trademarkId'])) {
            return;
        }

        $query->andWhere('t.id = :trademarkId');

        return $query;
    }
}
