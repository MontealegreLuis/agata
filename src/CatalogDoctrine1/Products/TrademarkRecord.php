<?php
/**
 * Trademark record that handles persistence of trademark entities.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Record as Record;
use \Doctrine_Manager as Manager;
use \ZendApplication\Filter\ProvidesSlugBuilders;
use \Doctrine1\Listener\UpdateVersion;
use \Doctrine1\Listener\BuildSlug;
use \Doctrine1\Record\HasVersion;
use \Doctrine1\Record\ProvidesVersionNumber;

Manager::getInstance()->bindComponent('CatalogDoctrine1\Products\TrademarkRecord', 'doctrine');

/**
 * Trademark record that handles persistence of trademark entities.
 *
 * It generates the trademark url based on the category name. It also updates the version column.
 *
 * @property integer $id
 * @property string $name
 * @property integer $version
 * @property Doctrine_Collection $Product
 */
class TrademarkRecord extends Record implements HasVersion
{
    use ProvidesSlugBuilders, ProvidesVersionNumber;

    /**
     * @see Doctrine_Record_Abstract::setTableDefinition()
     */
    public function setTableDefinition()
    {
        $this->setTableName('trademark');
        $this->hasColumn('id', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
        ]);
        $this->hasColumn('name', 'string', 45, [
             'type' => 'string',
             'length' => 45,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('url', 'string', 45, [
             'type' => 'string',
             'length' => 45,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('version', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
    }

    /**
     * @see Doctrine_Record::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CatalogDoctrine1\Products\ProductRecord as Product', [
            'local' => 'id',
            'foreign' => 'trademarkId'
        ]);
        $this->addListener(new BuildSlug('url', 'name', $this->createSlugBuilder()));
        $this->addListener(new UpdateVersion('version'));
    }
}
