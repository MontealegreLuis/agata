<?php
/**
 * Trademark entity
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \ProductCatalog\Products\Trademark as BaseTrademark;
use \ProductCatalog\Products\TrademarkDto;

/**
 * Trademark entity
 */
class Trademark extends BaseTrademark
{
    /** @type integer */
    protected $version;

    /**
     * @param string  $name
     * @param string  $url
     * @param integer $id
     * @param integer $version
     */
    public function __construct($name, $url = null, $id = null, $version = null)
    {
        parent::__construct($name, $url, $id);
        $this->version = $version;
    }

    /**
     * @param  TrademarkDto $view
     * @return TrademarkDto
     */
    public function render(TrademarkDto $view)
    {
        parent::render($view);
        $view->version = $this->version;

        return $view;
    }
}
