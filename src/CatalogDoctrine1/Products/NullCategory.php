<?php
/**
 * Null object for Category entities
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

/**
 * Null object for Category entities
 */
class NullCategory extends Category
{
    public function __construct() {}
}
