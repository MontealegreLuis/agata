<?php
/**
 * Hydrator for Offer objects
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author    LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine1\Hydrator\Hydrator;
use \Doctrine1\Hydrator\ReflectionHydrator;
use \ProductCatalog\Products\DateRange;
use \ProductCatalog\Products\OfferType;
use \ProductCatalog\Products\Percentage;
use \Money\Money;
use \Money\Currency;
use \DateTime;
use \SplFileInfo;

/**
 * Hydrator for Offer objects
 */
class OfferHydrator extends ReflectionHydrator implements Hydrator
{
    /** @type OfferFactory */
    protected $factory;

    /**
     * @param OfferFactory $factory
     */
    public function __construct(OfferFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @see \Doctrine1\Hydrator\Hydrator::hydrate()
     */
    public function hydrate(array $data)
    {
        $duration = new DateRange(new DateTime($data['startDate']), new DateTime($data['stopDate']));

        $type = new OfferType((integer) $data['type']);
        if (OfferType::PERCENTAGE === $type->getType()) {
            $amount = new Percentage($data['amount']);
        } else {
            $amount = new Money((integer) $data['amount'], new Currency('MXN'));
        }

        $image = null;
        if (!empty($data['image'])) {
            $image = new SplFileInfo($data['image']);
        }
        $id = null;
        if (!empty($data['id'])) {
            $id = $data['id'];
        }
        $version = null;
        if (!empty($data['version'])) {
            $version = $data['version'];
        }

        return $this->factory->create(
            $duration, $type, $data['description'], $amount, $image, $id, $version
        );
    }

    /**
     * @see \Doctrine1\Hydrator\Hydrator::extract()
     */
    public function extract($offer)
    {
        $offer = parent::extract($offer);

        $offer['startDate'] = $offer['duration']->getStartDate()->format('Y-m-d');
        $offer['stopDate'] = $offer['duration']->getStopDate()->format('Y-m-d');
        $offer['type'] = $offer['type']->getType();

        if (!empty($offer['price'])) {
            $offer['amount'] = $offer['price']->getAmount();
            unset($offer['price']);
        } elseif (!empty($offer['amount'])) {
            $offer['amount'] = $offer['amount']->getAmount();
        } else {
            $offer['amount'] = $offer['percentage']->getPercentage();
            unset($offer['percentage']);
        }

        unset($offer['duration']);

        return $offer;
    }
}
