<?php
/**
 * OfferRecord
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \Doctrine_Record as Record;
use \Doctrine_Manager as Manager;
use \Doctrine1\Record\HasVersion;
use \Doctrine1\Record\ProvidesVersionNumber;
use \Doctrine1\Listener\UpdateVersion;
use \Doctrine1\Listener\BuildUniqueId;

Manager::getInstance()->bindComponent('CatalogDoctrine1\Products\OfferRecord', 'doctrine');

/**
 * OfferRecord
 *
 * @property integer $id
 * @property date $startDate
 * @property date $stopDate
 * @property integer $type
 * @property string $image
 * @property string $description
 * @property Doctrine_Collection $OfferProduct
 */
class OfferRecord extends Record implements HasVersion
{
    use ProvidesVersionNumber;

    /**
     * @see Doctrine_Record_Abstract::setTableDefinition()
     */
    public function setTableDefinition()
    {
        $this->setTableName('offer');
        $this->hasColumn('id', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
        ]);
        $this->hasColumn('startDate', 'date', null, [
             'type' => 'date',
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('stopDate', 'date', null, [
             'type' => 'date',
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('type', 'integer', 1, [
             'type' => 'integer',
             'length' => 1,
             'fixed' => false,
             'unsigned' => true,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('amount', 'integer', 1, [
            'type' => 'integer',
            'length' => 4,
            'fixed' => false,
            'unsigned' => true,
            'primary' => false,
            'notnull' => true,
            'autoincrement' => false,
        ]);
        $this->hasColumn('image', 'string', 50, [
             'type' => 'string',
             'length' => 50,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
        ]);
        $this->hasColumn('description', 'string', null, [
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
        ]);
        $this->hasColumn('version', 'integer', 4, [
            'type' => 'integer',
            'length' => 4,
            'fixed' => false,
            'unsigned' => false,
            'primary' => false,
            'notnull' => true,
            'autoincrement' => false,
        ]);
    }

    /**
     * @see Doctrine_Record::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CatalogDoctrine1\Products\OfferProductRecord as OfferProduct', [
             'local' => 'id',
             'foreign' => 'offerId'
        ]);
        $this->addListener(new BuildUniqueId('image', '.jpg'));
        $this->addListener(new UpdateVersion('version'));
    }
}
