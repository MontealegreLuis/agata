<?php
/**
 * Category
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \SplFileInfo;
use \ProductCatalog\Products\Category as BaseCategory;
use \ProductCatalog\Products\CategoryDto;

/**
 * Category entity handles optimistic locking through a 'version' column.
 */
class Category extends BaseCategory
{
    /** @type integer */
    protected $version;

    /**
     * @param string      $name
     * @param SplFileInfo $image
     * @param Category    $parentCategory
     * @param string      $url
     * @param integer     $productCount
     * @param integer     $id
     * @param integer     $version
     */
    public function __construct(
        $name,
        SplFileInfo $image = null,
        Category $parentCategory = null,
        $url = null,
        $productCount = null,
        $id = null,
        $version = null
    )
    {
        parent::__construct($name, $image, $parentCategory, $url, $productCount, $id);
        $this->version = $version;
    }

    /**
     * Pass the version number to the DTO
     *
     * @see \ProductCatalog\Products\Category::render()
     */
    public function render(CategoryDto $view)
    {
        parent::render($view);
        $view->version = $this->version;

        return $view;
    }
}
