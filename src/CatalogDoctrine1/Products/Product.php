<?php
/**
 * Product
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace CatalogDoctrine1\Products;

use \ProductCatalog\Products\Product as BaseProduct;
use \ProductCatalog\Products\ProductDto;
use \SplFileInfo;
use \Money\Money;

/**
 * Product
 */
class Product extends BaseProduct
{
    /**
     * @var integer
     */
    protected $version;

    /**
     * @param string      $model
     * @param string      $features
     * @param Trademark   $trademark
     * @param Category    $category
     * @param SplFileInfo $image
     * @param integer     $version
     */
    public function __construct(
        $model,
        $features,
        Money $price,
        Trademark $trademark,
        Category $category,
        SplFileInfo $image = null,
        $url = null,
        $id = null,
        $version = null
    )
    {
        parent::__construct($model, $features, $price, $trademark, $category, $image, $url, $id);
        $this->version = $version;
    }

    /**
     * @param  ProductDto $view
     * @return ProductDto
     */
    public function render(ProductDto $view)
    {
        $view = parent::render($view);
        $view->version = $this->version;

        return $view;
    }
}
