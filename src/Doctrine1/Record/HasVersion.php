<?php
/**
 * Helper interface to implement optimistic locking with a version number on entities.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\Record;

/**
 * Helper interface to implement optimistic locking with a version number on entities.
 */
interface HasVersion
{
    /**
     * @return integer
     */
    public function getExpectedVersion();
}
