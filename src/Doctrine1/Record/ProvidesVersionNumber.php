<?php
/**
 * Trait to set an expected version number on entities that implement optimistic locking with a
 * version number.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\Record;

/**
 * Trait to set an expected version number on entities that implement optimistic locking with a
 * version number.
 */
trait ProvidesVersionNumber
{
    /** @type integer */
    protected $expectedVersion;

    /**
     * @param integer $version
     */
    public function setExpectedVersion($version)
    {
        $this->expectedVersion = $version;
    }

    /**
     * @return integer
     */
    public function getExpectedVersion()
    {
        return $this->expectedVersion;
    }
}
