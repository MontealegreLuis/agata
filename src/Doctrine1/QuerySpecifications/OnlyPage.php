<?php
/**
 * Adds pagination conditions (limit/offset) to the given query
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\QuerySpecifications;

use \Doctrine_Query as Query;

/**
 * Adds pagination conditions (limit/offset) to the given query
 */
class OnlyPage implements Specification
{
    /** @type integer */
    protected $itemsPerPage;

    /** @type integer */
    protected $page;

    /**
     * @param integer $page
     * @param integer $itemsPerPage
     */
    public function __construct($page, $itemsPerPage = 10)
    {
        $this->page = $page;
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * @see \ZendApplication\Doctrine\QuerySpecifications\Specification::match()
     */
    public function match(Query $query, array $criteria = [])
    {
        if ($this->isQueryCount($query)) {
            return;
        }

        return $query->limit($this->itemsPerPage)->offset($this->offset());
    }

    /**
     * @param  Query   $query
     * @return boolean
     */
    protected function isQueryCount(Query $query)
    {
        $select = $query->getDqlPart('select');

        foreach ($select as $selectPart) {
            if (preg_match('/COUNT/i', $selectPart)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return integer
     */
    protected function offset()
    {
        return ($this->page - 1) * $this->itemsPerPage;
    }
}
