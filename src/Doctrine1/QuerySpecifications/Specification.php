<?php
/**
 * Modify the given Query object if it matches the conditions specfied
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\QuerySpecifications;

use \Doctrine_Query as Query;

/**
 * Modify the given Query object if it matches the conditions specfied
 */
interface Specification
{
    /**
     * @param Query $query
     * @param array $criteria
     */
    public function match(Query $query, array $criteria = []);
}
