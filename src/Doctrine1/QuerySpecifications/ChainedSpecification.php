<?php
/**
 * Runs several query specifications
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\QuerySpecifications;

use \Doctrine_Query as Query;

/**
 * Runs several query specifications
 */
class ChainedSpecification implements Specification
{
    /** @type Specification[] */
    protected $specifications;

    /**
     * @param Specification[] $specifications
     */
    public function __construct(array $specifications)
    {
        $this->specifications = $specifications;
    }

    /**
     * @see \ZendApplication\Doctrine\QuerySpecifications\Specification::match()
     */
    public function match(Query $query, array $criteria = [])
    {
        foreach ($this->specifications as $specification) {
            $specification->match($query, $criteria);
        }
    }
}
