<?php
/**
 * Trait to allow the hydration of one or more entities/aggregates
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\Hydrator;

/**
 * Trait to allow the hydration of one or more entities/aggregates
 */
trait ProvidesHydration
{
    /** @type Hydrator */
    protected $hydrator;

    /**
     * @param Hydrator $hydrator
     */
    public function setHydrator(Hydrator $hydrator)
    {
        $this->hydrator = $hydrator;
    }

    /**
     * @param  array  $data
     * @param  object $object
     * @return object
     */
    public function hydrate(array $data)
    {
        return $this->hydrator->hydrate($data);
    }

    /**
     * @param  object $object
     * @return array
     */
    public function extract($object)
    {
        return $this->hydrator->extract($object);
    }

    /**
     * @param  object $object
     * @return array
     */
    public function hydrateAll(array $objects)
    {
        $array = [];
        foreach ($objects as $object) {
            $array[] = $this->hydrate($object);
        }

        return $array;
    }
}
