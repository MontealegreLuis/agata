<?php
/**
 * Extracts an object values using reflection
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\Hydrator;

use \ReflectionClass;

/**
 * Extracts an object values using reflection
 */
abstract class ReflectionHydrator implements Hydrator
{
    /**
     * @param  object $object
     * @return array
     */
    public function extract($object)
    {
        $values = [];
        $className = get_class($object);
        $reflectionClass = new ReflectionClass($className);
        $properties = $reflectionClass->getProperties() ;
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $values[$property->getName()] = $property->getValue($object);
        }

        return $values;
    }

    /**
     * @param  array  $data
     * @return object
     */
    abstract public function hydrate(array $data);
}
