<?php
/**
 * Interface to hydrate entities/aggregates after retrieving the from the database
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\Hydrator;

/**
 * Interface to hydrate entities/aggregates after retrieving the from the database
 */
interface Hydrator
{
    /**
     * @param  object $object
     * @return array
     */
    public function extract($object);

    /**
     * @param  array  $data
     * @return object
    */
    public function hydrate(array $data);
}
