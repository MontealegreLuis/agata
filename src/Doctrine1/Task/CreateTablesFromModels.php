<?php
/**
 * CLI task to create tables from models
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\Task;

use \Doctrine_Task as Task;
use \Doctrine_Core as Doctrine;
use \Doctrine_Manager as DoctrineManager;

/**
 * CLI task to create tables from models
 */
class CreateTablesFromModels extends Task
{
    /** @type string */
    public $description       = 'Create tables for all existing database connections. If table exists nothing happens.';

    /** @type string[] */
    public $requiredArguments = ['models_path' => 'Specify path to your models directory.'];

    /** @type string[] */
    public $optionalArguments = [];

    /**
     * Export the database scheme using the loaded models
     *
     * @see Doctrine_Task::execute()
     */
    public function execute()
    {
        $this->loadModels($this->getArgument('models_path'));
        DoctrineManager::connection()->export->exportSchema();

        $this->notify('Created tables successfully');
    }

    /**
     * @param array $models
     */
    protected function loadModels(array $models)
    {
        foreach ($models as $class) {
            Doctrine::loadModel($class);
        }
    }

    /**
     * @return string $taskName
     */
    public function getTaskName()
    {
        return 'create-tables-from-models';
    }
}
