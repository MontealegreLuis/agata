<?php
/**
 * Task for loading records from a SQL file
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\Task;

use \Doctrine_Task as Task;
use \Doctrine_Manager as DoctrineManager;

/**
 * Task for loading records from a SQL file
 */
class LoadSqlFile extends Task
{
    /** @type string */
    public $description =   'Load data from a .sql file.';

    /** @type array */
    public $requiredArguments =   array(
        'sql_file_path' =>  'Specify the complete path to load the .sql file.',
    );

    /**
     * Load the file and execute its SQL commands
     *
     * @see Doctrine_Task::execute()
     */
    public function execute()
    {
        $connection = DoctrineManager::getInstance()->getCurrentConnection();

        $sql = file_get_contents($this->getArgument('sql_file_path'));
        $this->notify('The following SQL will be executed');
        $this->notify($sql);

        $connection->execute($sql);
        $this->notify('Data was successfully loaded');
    }

    /**
     * @return string $taskName
     */
    public function getTaskName()
    {
        return 'load-sql-file';
    }
}
