<?php
/**
 * Configure the Doctrine connection
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1;

use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\Config;
use \Doctrine_Manager as Manager;
use \Doctrine_Core as Core;

/**
 * Configure the Doctrine connection
 */
class Doctrine1Provider extends Config
{
    /**
     * Register shared application services
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'connection' => function (ServiceLocatorInterface $sl) {
                $manager = Manager::getInstance();
                $manager->setAttribute(Core::ATTR_AUTO_ACCESSOR_OVERRIDE, true);

                $config = require $sl->get('connection.configuration');
                $connection = Manager::connection($config['dsn'], 'doctrine');
                $connection->setAttribute(Core::ATTR_USE_NATIVE_ENUM, true);
                $connection->setCharset('UTF8');

                return $connection;
            },
        ];
    }
}
