<?php
/**
 * Exception thrown when an optimistic locking error occurs
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Doctrine1\Exception;

use \RuntimeException;

/**
 * Exception thrown when an optimistic locking error occurs
 */
class OptimisticLockException extends RuntimeException
{
    /** @type string[] */
    protected $originalRecordValues;

    /**
     * @param  string[]                $values
     * @return OptimisticLockException
     */
    public function setOriginalRecordValues(array $values)
    {
        $this->originalRecordValues = $values;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getOriginalRecordValues()
    {
        return $this->originalRecordValues;
    }
}
