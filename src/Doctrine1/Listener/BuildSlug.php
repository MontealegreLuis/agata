<?php
/**
 * Event listener that builds a slug based on the given record's columns
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\Listener;

use \Doctrine_Record_Listener as RecordListener;
use \Doctrine_Event as Event;
use \ZendApplication\Filter\FriendlyUrlFilter;

/**
 * Event listener that builds a slug based on the given record's columns
 */
class BuildSlug extends RecordListener
{
    /** @type string */
    protected $slugColumn;

    /** @type string */
    protected $sourceColumn;

    /** @type FriendlyUrlFilter */
    protected $filter;

    /**
     * @param string            $slugColumn
     * @param string            $sourceColumn
     * @param FriendlyUrlFilter $filter
     */
    public function __construct($slugColumn, $sourceColumn, FriendlyUrlFilter $filter)
    {
        $this->slugColumn = $slugColumn;
        $this->sourceColumn = $sourceColumn;
        $this->filter = $filter;
    }

    /**
     * @see Doctrine_Record_Listener::preInsert()
     */
    public function preInsert(Event $event)
    {
        $record = $event->getInvoker();
        $record->{$this->slugColumn} = $this->filter->filter($record->{$this->sourceColumn});
    }

    /**
     * @see Doctrine_Record_Listener::preUpdate()
     */
    public function preUpdate(Event $event)
    {
        $record = $event->getInvoker();
        $record->{$this->slugColumn} = $this->filter->filter($record->{$this->sourceColumn});
    }
}
