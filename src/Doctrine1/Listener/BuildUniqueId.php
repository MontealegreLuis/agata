<?php
namespace Doctrine1\Listener;

use \Doctrine_Record_Listener as RecordListener;
use \Doctrine_Event as Event;

class BuildUniqueId extends RecordListener
{
    /** @type string */
    protected $column;

    /** @type string */
    protected $suffix;

    /**
     * @param string $column
     * @param string $suffix
     */
    public function __construct($column, $suffix)
    {
        $this->column = $column;
        $this->suffix = $suffix;
    }
    /**
     * @see Doctrine_Record_Listener::preInsert()
     */
    public function preInsert(Event $event)
    {
        $record = $event->getInvoker();
        $record->{$this->column} = uniqid() . $this->suffix;
    }
}
