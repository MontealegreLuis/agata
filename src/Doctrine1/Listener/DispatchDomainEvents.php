<?php
/**
 * Listener that triggers the domain events related to an entity after updating it.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace Doctrine1\Listener;

use \Doctrine_Record_Listener as RecordListener;
use \Doctrine_Event as Event;
use \ProductCatalog\DomainEvents\DomainEventsBus;

/**
 * Listener that triggers the domain events related to an entity after updating it.
 */
class DispatchDomainEvents extends RecordListener
{
    /** @type DomainEventsBus */
    protected $eventsBus;

    /**
     * @param DomainEventsBus $eventBus
     */
    public function __construct(DomainEventsBus $eventBus)
    {
        $this->eventsBus = $eventBus;
    }

    /**
     * Dispatch all the domain events related to the associated entity
     *
     * @see Doctrine_Record::postUpdate()
     */
    public function postUpdate(Event $event)
    {
        $this->eventsBus->dispatchEvents();
    }
}
