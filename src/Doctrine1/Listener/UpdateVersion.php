<?php
/**
 * Event listener that increases by 1 the 'version' column value
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace Doctrine1\Listener;

use \Doctrine_Record_Listener as RecordListener;
use \Doctrine_Event as Event;
use \Doctrine1\Exception\OptimisticLockException;
use \Doctrine1\Record\HasVersion;

/**
 * Event listener that increases by 1 the 'version' column value
 */
class UpdateVersion extends RecordListener
{
    /** @type string */
    protected $versionColumn;

    /**
     * @param string $versionColumn
     */
    public function __construct($versionColumn)
    {
        $this->versionColumn = $versionColumn;
    }

    /**
     * @see Doctrine_Record_Listener::preInsert()
     */
    public function preInsert(Event $event)
    {
        $record = $event->getInvoker();
        $record->{$this->versionColumn} += 1;
    }

    /**
     * @see Doctrine_Record_Listener::preUpdate()
     */
    public function preUpdate(Event $event)
    {
        $record = $event->getInvoker();

        $this->checkOptimisticLock($record);

        $record->{$this->versionColumn} += 1;
    }

    /**
     * @param  HasVersion              $record
     * @throws OptimisticLockException
     */
    protected function checkOptimisticLock(HasVersion $record)
    {
        if ($record->{$this->versionColumn} != $record->getExpectedVersion()) {
            $record->refresh();

            $exception = new OptimisticLockException('Someone has modified this record.');
            $exception->setOriginalRecordValues($record->toArray());

            throw $exception;
        }
    }
}
