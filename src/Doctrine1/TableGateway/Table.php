<?php
/**
 * Base class for Doctrine table gateway objects
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\TableGateway;

use \Doctrine_Record as Record;
use \Doctrine_Query as Query;
use \Doctrine1\QuerySpecifications\Specification;

/**
 * Base class for Doctrine table gateway objects
 */
abstract class Table
{
    /** @type Record */
    protected $record;

    /** @type Specification */
    protected $specification;

    /**
     * @param Record $record
     */
    public function __construct(Record $record)
    {
      $this->record = $record;
    }

    /**
     * @param Specification $specification
     */
    public function addSpecification(Specification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param  array $values
     * @return array
     */
    public function insert(array $values)
    {
        return $this->save($values);
    }

    /**
     * @param array $values
     * @param array $identifier
     */
    public function update(array $values, array $identifier)
    {
        $this->record->assignIdentifier($identifier);

        return $this->save($values);
    }

    /**
     * @param  array $values
     * @return array
     */
    protected function save(array $values)
    {
        $this->record->synchronizeWithArray($values);
        $this->record->refreshRelated();
        $this->record->save();

        return $this->record->toArray();
    }

    /**
     * @param  array                         $identifier
     * @throws Doctrine_Connection_Exception
     */
    public function delete(array $identifier)
    {
        $this->record->assignIdentifier($identifier);
        $this->record->delete();
    }

    /**
     * @param  string $alias = ''
     * @return Query
     */
    public function createQuery($alias = '')
    {
        return $this->record->getTable()->createQuery($alias);
    }

    /**
     * @param Query $query
     * @param array $criteria
     */
    protected function match(Query $query, array $criteria = [])
    {
        if (!$this->specification) {
            return;
        }

        $this->specification->match($query, $criteria);
    }
}
