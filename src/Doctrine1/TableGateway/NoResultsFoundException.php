<?php
/**
 * Custom exception used when a query returns zero results
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace Doctrine1\TableGateway;

use \RuntimeException;

/**
 * Custom exception used when a query returns zero results
 */
class NoResultsFoundException extends RuntimeException {}
