<?php
/**
 * Gateway for User model objects
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace SecurityDoctrine1\Identity;

use \Doctrine_Core as Doctrine;
use \Doctrine1\TableGateway\NoResultsFoundException;
use \Doctrine1\TableGateway\Table;
use \Doctrine1\Hydrator\ProvidesHydration;
use \Security\Identity\UserRepository;

/**
 * Gateway for User model objects
 */
class UserTable extends Table implements UserRepository
{
    use ProvidesHydration;

    /**
     * @param  string                  $username
     * @return User
     * @throws NoResultsFoundException
     */
    public function findOneByUsername($username)
    {
        $query = $this->createQuery('u');
        $query->where('u.username = :username');
        $user = $query->fetchOne([':username' => $username], Doctrine::HYDRATE_ARRAY);

        if (!$user) {
            throw new NoResultsFoundException("User with username '$username' cannot be found");
        }

        return $this->hydrate($user);
    }
}
