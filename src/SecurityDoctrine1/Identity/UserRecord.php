<?php
/**
 * User record that handles persistence of User entities.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityDoctrine1\Identity;

use \Doctrine_Record as Record;
use \Doctrine_Manager as Manager;

Manager::getInstance()->bindComponent('SecurityDoctrine1\Identity\UserRecord', 'doctrine');

/**
 * User record that handles persistence of User entities.
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property enum $state
 * @property string $confirmationKey
 * @property integer $version
 */
class UserRecord extends Record
{
    /**
     * @see Doctrine_Record_Abstract::setTableDefinition()
     */
    public function setTableDefinition()
    {
        $this->setTableName('user');
        $this->hasColumn('id', 'integer', 4, [
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
        ]);
        $this->hasColumn('username', 'string', 100, [
             'type' => 'string',
             'length' => 100,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('password', 'string', 255, [
             'type' => 'string',
             'length' => 45,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        /*
        $this->hasColumn('state', 'enum', 11, [
             'type' => 'enum',
             'length' => 11,
             'fixed' => false,
             'unsigned' => false,
             'values' => [
                  0 => 'active',
                  1 => 'unconfirmed',
                  2 => 'inactive',
                  3 => 'banned',
             ],
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('confirmationKey', 'string', 45, [
             'type' => 'string',
             'length' => 45,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
        ]);
        */
        $this->hasColumn('roleName', 'string', 10, [
             'type' => 'string',
             'length' => 10,
             'fixed' => false,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
        ]);
        $this->hasColumn('version', 'integer', 8, [
             'type' => 'integer',
             'length' => 8,
             'fixed' => false,
             'unsigned' => true,
             'primary' => false,
             'default' => '1',
             'notnull' => true,
             'autoincrement' => false,
        ]);
    }

    /**
     * @see Doctrine_Record::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        /*$this->hasOne('App_Model_Dao_Role as Role', [
             'local' => 'roleName',
             'foreign' => 'name'
        ]);*/
    }
}
