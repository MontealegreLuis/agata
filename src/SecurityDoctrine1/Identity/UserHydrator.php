<?php
/**
 * Hydrator for User entities
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityDoctrine1\Identity;

use \Security\Identity\User;
use \Doctrine1\Hydrator\Hydrator;
use \Doctrine1\Hydrator\ReflectionHydrator;

/**
 * Hydrator for User entities
 */
class UserHydrator extends ReflectionHydrator implements Hydrator
{
    /**
     * @return User
     */
    public function hydrate(array $user)
    {
        $role = null;
        if (!empty($user['roleName'])) {
            $role = $user['roleName'];
        }

        return new User($user['username'], $user['password'], $role);
    }
}
