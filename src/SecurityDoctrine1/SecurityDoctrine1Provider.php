<?php
/**
 * Service manager configuration for Security's Doctrine 1 persistence port
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityDoctrine1;

use \Zend\ServiceManager\Config;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \SecurityDoctrine1\Identity\UserTable;
use \SecurityDoctrine1\Identity\UserRecord;

/**
 * Service manager configuration for Security's Doctrine 1 persistence port
 */
class SecurityDoctrine1Provider extends Config
{
    /**
     * Add configuration for the shared services and the security module.
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'user.repository' => function(ServiceLocatorInterface $sl) {
                $connection = $sl->get('connection');
                $table = new UserTable(new UserRecord());
                $table->setHydrator($sl->get('user.hydrator'));

                return $table;
            },
        ];
        $this->config['invokables'] = ['user.hydrator' => 'SecurityDoctrine1\Identity\UserHydrator'];
    }
}
