<?php
return [
    'login' => [
       'module' => 'security',
       'controller' => 'user',
       'action' => 'login',
       'route' => 'auth',
    ],
    'authenticate' => [
        'module' => 'security',
        'controller' => 'user',
        'action' => 'authenticate',
        'route' => 'auth',
    ],
    'logout' => [
        'module' => 'security',
        'controller' => 'user',
        'action' => 'logout',
        'route' => 'auth',
    ],
];
