<?php
return [
    'login.legend' => 'Iniciar sesión',
    'user.field.username' => 'Nombre de usuario',
    'user.field.password' => 'Contraseña',
    'user.field.login' => 'Ingresar',
    'user.username.isEmpty' => 'Ingrese su nombre de usuario',
    'user.username.strlenShort' => 'El nombre de usuario debe tener mínimo \'%min%\' caracteres',
    'user.username.emailAddressInvalidFormat' => 'El nombre de usuario debe ser una dirección de correo electrónico válida',
    'user.password.isEmpty' => 'Ingrese su password',
    'auth.userNotFound' => 'El usuario que ingresó no existe',
    'auth.wrongPassword' => 'La contraseña es incorrecta',
    'bad.token' => 'Hubo un problema al ingresar a tu cuenta, por favor vuelve a intentar',
];
