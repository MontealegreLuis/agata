<?php
return [
    'auth' => [
        'route' => '/:@action',
        'defaults' => [
            'controller' => 'user',
            'module' => 'security'
        ],
    ],
];
