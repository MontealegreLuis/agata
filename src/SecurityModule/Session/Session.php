<?php
/**
 * Wrapper of a Zend_Session_Namespace object with convenient methods to handle the last URL
 * requested by an authenticated user
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\Session;

use \Zend_Session_Namespace as SessionNamespace;

/**
 * Wrapper of a Zend_Session_Namespace object with convenient methods to handle the last URL
 * requested by an authenticated user
 */
class Session
{
    /** @type SessionNamespace */
    protected $session;

    /**
     * @param SessionNamespace $session
     */
    public function __construct(SessionNamespace $session)
    {
        $this->session = $session;
    }

    /**
     * @return string
     */
    public function getLastRequestedUrl()
    {
        $url = $this->session->requestUrl;
        unset($this->session->requestUrl);

        return $url;
    }

    /**
     * @param string $url
     */
    public function setLastRequestedUrl($url)
    {
        $this->session->requestUrl = $url;
    }

    /**
     * @return boolean
     */
    public function hasLastRequestedUrl()
    {
        return isset($this->session->requestUrl);
    }
}
