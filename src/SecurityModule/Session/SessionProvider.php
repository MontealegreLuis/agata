<?php
/**
 * Configure the application security session
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace SecurityModule\Session;

use \Zend_Session as ZendSession;
use \Zend_Session_Namespace as SessionNamespace;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\FactoryInterface;
use \ZendApplication\Config\CanBeConfigured;

/**
 * Configure the application security session
 */
class SessionProvider extends CanBeConfigured implements FactoryInterface
{
    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        ZendSession::setOptions($this->options);

        $sessionNamespace = new SessionNamespace($this->getOption('name'));
        $sessionNamespace->setExpirationSeconds($this->getOption('gc_maxlifetime'));

        return new Session($sessionNamespace);
    }
}
