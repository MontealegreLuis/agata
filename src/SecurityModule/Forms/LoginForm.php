<?php
/**
 * User's login form
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
namespace SecurityModule\Forms;

use \ZendApplication\Form\Form;

/**
 * User's login form
 */
class LoginForm extends Form
{
    /**
     * @param  array $errorMessages
     * @return void
     */
    public function setAuthenticationErrors(array $errorMessages)
    {
        foreach ($errorMessages as $name => $message) {
            $this->getElement($name)->setErrors([$message]);
        }
    }
}
