<?php
/**
 * Controller to login a user
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\Controllers;

use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \SecurityModule\Session\Session;
use \SecurityModule\Forms\LoginForm;
use \Security\Authentication\AuthenticateUser;
use \Security\Authentication\AuthenticationEvent;
use \Security\Authentication\AuthenticateUserRequest;

/**
 * Controller to login a user
 */
class AuthenticateUserController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type LoginForm */
    protected $loginForm;

    /** @type AuthenticateUser */
    protected $useCase;

    /** @var Session */
    protected $session;

    /**
     * @param LoginForm $loginForm
     */
    public function setLoginForm(LoginForm $loginForm)
    {
        $this->loginForm = $loginForm;
    }

    /**
     * @param AuthenticateUser $authenticateUser
     */
    public function setUseCase(AuthenticateUser $authenticateUser)
    {
        $this->useCase = $authenticateUser;
    }

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Authenticate a user
     *
     * If the form validation fails, it shows the form with the validations errors, otherwise the
     * login attempt is performed
     */
    public function authenticateAction()
    {
        if ($this->loginForm->isValid($this->post())) {
            $this->useCase->login(new AuthenticateUserRequest($this->loginForm->getValues()));
        }
        $this->onFormValidationFailure();
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('AuthenticationSucceeded', [$this, 'onAuthenticationSuccess']);
        $this->listeners[] = $events->attach('AuthenticationFailed', [$this, 'onAuthenticationFailure']);
    }

    /**
     * Redirect to the last requested URL or to the initial page of the restricted area
     */
    public function onAuthenticationSuccess()
    {
        if ($this->session->hasLastRequestedUrl()) {
            $this->redirectTo($this->session->getLastRequestedUrl());
        }
        $this->redirectTo($this->getUrl('restricted.home'));
    }

    /**
     * Pass the authentication error messages to the login form
     */
    public function onAuthenticationFailure(AuthenticationEvent $event)
    {
        $this->loginForm->setAuthenticationErrors($event->getAuthenticationResult()->getMessages());
    }

    /**
     * Render the login form again with the error messages
     */
    public function onFormValidationFailure()
    {
        $this->view->loginForm = $this->loginForm;
        $this->renderScript('user/login.twig');
    }
}
