<?php
/**
 * Controller to close a user's session
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\Controllers;

use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \Security\Authentication\LogoutUser;

/**
 * Controller to close a user's session
 */
class LogoutUserController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /** @type LogoutUser */
    protected $useCase;

    /**
     * @param LogoutUser $logoutUser
     */
    public function setUseCase(LogoutUser $logoutUser)
    {
        $this->useCase = $logoutUser;
    }

    /**
     * End the current user's session and redirect to the login page.
     */
    public function logoutAction()
    {
        $this->useCase->logoutUser();
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('UserLogout', [$this, 'onUserLogout']);
    }

    /**
     * Redirect to the login page
     */
    public function onUserLogout()
    {
        $this->redirectTo($this->getUrl('login'));
    }
}
