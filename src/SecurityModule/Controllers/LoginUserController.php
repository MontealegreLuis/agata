<?php
/**
 * Controller to perform a user login attempt
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author    LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\Controllers;

use \Zend\EventManager\ListenerAggregateInterface;
use \Zend\EventManager\ListenerAggregateTrait;
use \Zend\EventManager\EventManagerInterface;
use \ZendApplication\Controller\Action\ControllerAction;
use \ZendApplication\Navigation\ProvidesUrls;
use \SecurityModule\Forms\LoginForm;
use \SecurityModule\Session\Session;
use \Security\Authentication\CheckUserIdentity;

/**
 * Controller to perform a user login attempt
 */
class LoginUserController extends ControllerAction implements ListenerAggregateInterface
{
    use ListenerAggregateTrait, ProvidesUrls;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var LoginForm
     */
    protected $loginForm;

    /**
     * @var CheckUserIdentity
     */
    protected $useCase;

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param LoginForm $loginForm
     */
    public function setLoginForm(LoginForm $loginForm)
    {
        $this->loginForm = $loginForm;
    }

    /**
     * @param CheckUserIdentity $checkUserIdentity
     */
    public function setUseCase(CheckUserIdentity $checkUserIdentity)
    {
        $this->useCase = $checkUserIdentity;
    }

    /**
     * Show login form
     */
    public function loginAction()
    {
        if (!$this->useCase->isUserLogged()) {
            $this->loginForm->setAction($this->getUrl('authenticate'));
            $this->view->loginForm = $this->loginForm;
        }
    }

    /**
     * @see \Zend\EventManager\ListenerAggregateInterface::attach()
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach('UserAlreadyLogged', [$this, 'onUserAlreadyLogged']);
    }

    /**
     * Redirect to the last requested URL or to the initial page of the restricted area
     */
    public function onUserAlreadyLogged()
    {
        if ($this->session->hasLastRequestedUrl()) {
            $this->redirectTo($this->session->getLastRequestedUrl());
        }
        $this->redirectTo($this->getUrl('restricted.home'));
    }
}
