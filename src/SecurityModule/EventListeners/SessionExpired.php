<?php
/**
 * HttpActionEvent listener that checks if the current user's session has expired.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\EventListeners;

use \Zend_Controller_Request_Http as Request;
use \Zend_Controller_Response_Http as Response;
use \Zend_View_Abstract as View;
use \ZendApplication\Controller\Action\Events\HttpActionEvent;
use \ZendApplication\Navigation\ProvidesUrls;
use \SecurityModule\Session\Session;
use \Security\Identity\Firewall;

/**
 * HttpActionEvent listener that checks if the current user's session has expired.
 *
 * This event listener will be registered in the preDispatch event of controllers that need an
 * authenticated user to perform its action.
 */
class SessionExpired
{
    use ProvidesUrls;

    /** @type Firewall */
    protected $firewall;

    /** @type Session */
    protected $session;

    /**
     * @param Firewall $firewall
     * @param Session  $session
     */
    public function __construct(Firewall $firewall, Session $session)
    {
        $this->firewall = $firewall;
        $this->session = $session;
    }

    /**
     * Check wether the user's session has expired or not.
     */
    public function __invoke(HttpActionEvent $event)
    {
        $this->isUserAuthenticated($event->getRequest(), $event->getResponse(), $event->getView());
    }

    /**
     * If the user's session has expired redirect the user to the login page and save the last
     * requested URL.
     *
     * The last requested URL will be used after a successful login attempt
     *
     * @see \SecurityModule\Controllers\AuthenticateUserController::onAuthenticationSuccess
     */
    public function isUserAuthenticated(Request $request, Response $response, View $view)
    {
        if (!$this->firewall->hasIdentity()) {
            $this->setLastRequestedUrl($request);
            $response->setRedirect($this->getUrl('login'));
            $response->sendHeaders();
            exit();
        }
    }

    /**
     * Only set the last request URL if the current request is a GET request
     *
     * @param Request $request
     */
    protected function setLastRequestedUrl(Request $request)
    {
        if ($request->isGet()) {
            $this->session->setLastRequestedUrl($request->getRequestUri());
        }
    }
}
