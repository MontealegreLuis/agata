<?php
/**
 * User controllers configuration
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\ServiceManager;

use \Zend\ServiceManager\ConfigInterface;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \ZendApplication\ServiceManager\ControllerProvider;
use \ZendApplication\Controller\Action\Events\HttpValidationListener;
use \SecurityModule\Controllers\LoginUserController;
use \SecurityModule\Controllers\AuthenticateUserController;
use \SecurityModule\Controllers\LogoutUserController;

/**
 * User controllers configuration
 */
class UserControllerProvider extends ControllerProvider implements ConfigInterface
{
    public function __construct()
    {
        $this->key = 'security.user';
    }

    /**
     * @see \Zend\ServiceManager\ConfigInterface::configureServiceManager()
     */
    public function configureServiceManager(ServiceManager $serviceManager)
    {
        $serviceManager->setFactory('security.user.login', function(ServiceLocatorInterface $sl) {
            $controller = new LoginUserController($this->request, $this->response, $this->params);
            $controller->setSession($sl->get('session'));
            $controller->setLoginForm($sl->get('form.login'));
            $controller->setUrlContainer($sl->get('user.navigation'));

            $useCase = $sl->get('user.check.identity');
            $controller->setUseCase($useCase);
            $useCase->getEventManager()->attachAggregate($controller);

            return $controller;
        });
        $serviceManager->setFactory('security.user.authenticate', function(ServiceLocatorInterface $sl) {
            $controller = new AuthenticateUserController( $this->request, $this->response, $this->params);
            $controller->setSession($sl->get('session'));
            $controller->setLoginForm($sl->get('form.login'));
            $controller->setUrlContainer($sl->get('user.navigation'));
            $controller->getEventManager()->attach('preDispatch', new HttpValidationListener('post'));

            $useCase = $sl->get('user.authenticate');
            $controller->setUseCase($useCase);
            $useCase->getEventManager()->attachAggregate($controller);

            return $controller;
        });
        $serviceManager->setFactory('security.user.logout', function(ServiceLocatorInterface $sl) {
            $controller = new LogoutUserController($this->request, $this->response, $this->params);
            $controller->setUrlContainer($sl->get('user.navigation'));

            $useCase = $sl->get('user.logout');
            $controller->setUseCase($useCase);
            $useCase->getEventManager()->attachAggregate($controller);

            return $controller;
        });
    }
}
