<?php
/**
 * Service configuration for the security module
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace SecurityModule\ServiceManager;

use \Zend_Config_Ini as IniConfig;
use \Zend\ServiceManager\ServiceManager;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Zend\ServiceManager\Config;
use \ZendApplication\Navigation\UrlContainer;
use \SecurityModule\Forms\LoginForm;
use \Security\Authentication\CheckUserIdentity;
use \Security\Authentication\AuthenticateUser;
use \Security\Authentication\LogoutUser;
use \Security\Authentication\AuthenticationEvent;
use \SecurityModule\Session\SessionProvider;
use \SecurityModule\EventListeners\SessionExpired;

/**
 * Service configuration for the security module
 */
class SecurityModuleProvider extends Config
{
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->config['factories'] = [
            'form.login' => function(ServiceLocatorInterface $sl) {
                $config = new IniConfig($sl->get('form.login.configuration'), 'user');

                return new LoginForm($config->toArray());
            },
            'user.check.identity' => function(ServiceLocatorInterface $sl) {
                return new CheckUserIdentity($sl->get('security.firewall'));
            },
            'user.authenticate' => function(ServiceLocatorInterface $sl) {
                $firewall = $sl->get('security.firewall');
                $firewall->setAdapter($sl->get('user.auth.adapter'));

                return new AuthenticateUser($firewall, new AuthenticationEvent());
            },
            'user.logout' => function(ServiceLocatorInterface $sl) {
                $session = $sl->get('session');

                return new LogoutUser($sl->get('security.firewall'));
            },
            'user.navigation' => function(ServiceLocatorInterface $sl) {
                return new UrlContainer(
                    require $sl->get('user.navigation.configuration'), $sl->get('view')
                );
            },
            'session' => function(ServiceLocatorInterface $sl) {
                $session = new SessionProvider(require $sl->get('session.configuration'));

                return $session->createService($sl);
            },
            'authentication.listener' => function(ServiceLocatorInterface $sl) {
                //Start the session before using the firewall, $_SESSION namespace handling seems to overlap if not done in this order
                $session = $sl->get('session');
                $firewall = $sl->get('security.firewall');
                $sessionExpired = new SessionExpired($firewall, $session);
                $sessionExpired->setUrlContainer($sl->get('user.navigation'));

                return $sessionExpired;
            },
        ];
        $this->config['services'] = [
            'form.login.configuration' => __DIR__ . '/../Resources/config/forms/login.ini',
            'user.navigation.configuration' => __DIR__ . '/../Resources/config/navigation.php'
        ];
    }
}
