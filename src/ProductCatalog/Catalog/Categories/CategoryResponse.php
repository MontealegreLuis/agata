<?php
/**
 * DTO that contains a given category information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \ProductCatalog\Products\Category;
use \ProductCatalog\Products\CategoryDto;

/**
 * DTO that contains a given category information
 */
class CategoryResponse
{
    /** @type CategoryDto */
    public $category;

    /**
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category->render(new CategoryDto());
    }
}
