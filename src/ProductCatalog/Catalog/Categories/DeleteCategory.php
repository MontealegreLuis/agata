<?php
/**
 * Use case to delete a category by its ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\CategoryEvent;
use \Zend\EventManager\ProvidesEvents;

/**
 * Use case to delete a category by its ID
 */
class DeleteCategory
{
    use ProvidesEvents;

    /** @type CategoryRepository */
    protected $repository;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CategoryRequest $request
     */
    public function deleteCategory(CategoryRequest $request)
    {
        $category = $this->repository->categoryOfId($request->categoryId);

        if (!$category) {
            return $this->getEventManager()->trigger('CategoryNotFound');
        }

        $this->repository->remove($category);

        $this->getEventManager()->trigger(
            (new CategoryEvent('CategoryDeleted'))->setCategory($category)
        );
    }
}
