<?php
/**
 * Use case to retrieve all the available categories
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use ProductCatalog\Products\CategoryRepository;

/**
 * Use case to retrieve all the available categories
 */
class GetAllCategories
{
    /** @type CategoryRepository */
    protected $repository;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return GetAllCategoriesResponse
     */
    public function getAllCategories()
    {
        return new GetAllCategoriesResponse($this->repository->allCategories());
    }
}
