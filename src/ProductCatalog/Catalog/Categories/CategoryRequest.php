<?php
/**
 * DTO for use cases that retrieve the information of a category by its ID.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

/**
 * DTO for use cases that retrieve the information of a category by its ID.
 */
class CategoryRequest
{
    /** @type integer */
    public $categoryId;

    /**
     * @param integer $categoryId
     */
    public function __construct($categoryId)
    {
        $this->categoryId = $categoryId;
    }
}
