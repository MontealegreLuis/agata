<?php
/**
 * DTO that contains a given category information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \ProductCatalog\Products\Category;

/**
 * DTO that contains a given category information
 */
class UpdateCategoryRequest extends SaveCategoryRequest
{
    /** @type integer */
    public $categoryId;

    /**
     * @param mixed[] $values
     */
    public function __construct(array $values)
    {
        parent::__construct($values);
        $this->categoryId = $values['id'];
    }
}
