<?php
/**
 * DTO that contains a given category information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \ProductCatalog\Products\Category;
use \SplFileInfo;

/**
 * DTO that contains a given category information
 */
class SaveCategoryRequest
{
    /** @type string */
    public $name;

    /** @type integer */
    public $parentCategoryId;

    /** @type SplFileInfo */
    public $image;

    /**
     * @param mixed[] $values
     */
    public function __construct(array $values)
    {
        $this->name = $values['name'];
        $this->parentCategoryId = null;
        if (is_array($values['parentCategoryId'])) {
            $this->parentCategoryId = array_pop($values['parentCategoryId']);
        }
        if (!empty($values['image'])) {
            $this->image = new SplFileInfo($values['image']);
        }
    }
}
