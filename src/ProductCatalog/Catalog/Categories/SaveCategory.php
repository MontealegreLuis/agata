<?php
/**
 * Use case to save a new category information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\Category;
use \ProductCatalog\Products\CategoryEvent;

/**
 * Use case to save a new category information
 */
class SaveCategory
{
    use ProvidesEvents;

    /** @type CategoryRepository */
    protected $repository;

    /**
     * @param CategoryRepository $repository
     * @param CategoryEvent      $categoryEvent
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SaveCategoryRequest $request
     */
    public function saveCategory(SaveCategoryRequest $request)
    {
        $parentCategory = null;
        if ($request->parentCategoryId) {
            $parentCategory = $this->repository->categoryOfId($request->parentCategoryId);
        }

        $category = new Category($request->name, $request->image, $parentCategory);

        $category = $this->repository->persist($category);

        $this->getEventManager()->trigger(
            (new CategoryEvent('CategorySaved'))->setCategory($category)
        );
    }
}
