<?php
/**
 * Use case to retrieve a category by its ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\CategoryRepository;

/**
 * Use case to retrieve a category by its ID
 */
class GetCategory
{
    use ProvidesEvents;

    /** @type CategoryRepository */
    protected $repository;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  CategoryRequest  $request
     * @return CategoryResponse
     */
    public function getCategory(CategoryRequest $request)
    {
        $category = $this->repository->categoryOfId($request->categoryId);

        if (!$category) {
            $this->getEventManager()->trigger('CategoryNotFound');
        }

        return new CategoryResponse($category);
    }
}
