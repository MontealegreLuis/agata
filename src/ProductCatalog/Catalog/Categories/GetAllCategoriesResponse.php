<?php
/**
 * DTO that contains all available categories information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \ProductCatalog\Products\Category;
use \ProductCatalog\Products\CategoryDto;

/**
 * DTO that contains all available categories information
 */
class GetAllCategoriesResponse
{
    /** @type CategoryDto[] */
    public $categories;

    /**
     * @param Category[] $categories
     */
    public function __construct(array $categories)
    {
        array_walk($categories, function(Category &$category){
            $category = $category->render(new CategoryDto());
        });
        $this->categories = $categories;
    }
}
