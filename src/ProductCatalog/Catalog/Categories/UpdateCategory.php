<?php
/**
 * Use case to update a category information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Categories;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\DomainEvents\ProvidesDomainEvents;
use \ProductCatalog\DomainEvents\DomainEventsProvider;
use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\Category;
use \ProductCatalog\Products\CategoryEvent;

/**
 * Use case to update a category information
 */
class UpdateCategory implements DomainEventsProvider
{
    use ProvidesEvents, ProvidesDomainEvents;

    /** @type CategoryRepository */
    protected $repository;

    /**
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param UpdateCategoryRequest $request
     */
    public function updateCategory(UpdateCategoryRequest $request)
    {
        $category = $this->repository->categoryOfId($request->categoryId);

        if (!$category) {
            $this->getEventManager()->trigger('CategoryNotFound');

            return;
        }
        $category->setEventsBus($this->eventsBus);

        $parentCategory = null;
        if ($request->parentCategoryId) {
            $parentCategory = $this->repository->categoryOfId($request->parentCategoryId);
        }

        $category->edit($request->name, $request->image, $parentCategory);

        $category = $this->repository->persist($category);

        $this->getEventManager()->trigger(
            (new CategoryEvent('CategoryUpdated'))->setCategory($category)
        );
    }
}
