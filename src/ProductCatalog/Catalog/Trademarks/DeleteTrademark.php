<?php
/**
 * Delete the trademark with the given ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\Trademark;

/**
 * Delete the trademark with the given ID
 *
 * Triggers the TrdemarkNotFound event in case the trademark cannot be fount by its ID, triggers
 * TrademarkDelete if the trademark is successfully deleted.
 */
class DeleteTrademark
{
    use ProvidesEvents;

    /** @type TrademarkRepository */
    protected $repository;

    /**
     * @param TrademarkRepository $repository
     */
    public function __construct(TrademarkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TrademarkRequest $request
     */
    public function deleteTrademark(TrademarkRequest $request)
    {
        $trademark = $this->repository->trademarkOfId($request->trademarkId);

        if (!$trademark) {
            return $this->getEventManager()->trigger('TrademarkNotFound');
        }

        $this->repository->remove($trademark);

        $this->getEventManager()->trigger('TrademarkDeleted');
    }
}
