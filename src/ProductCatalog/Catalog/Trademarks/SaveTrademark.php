<?php
/**
 * Save the information of a new trademark
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\TrademarkEvent;

/**
 * Save the information of a new trademark
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
class SaveTrademark
{
    use ProvidesEvents;

    /** @type TrademarkRepository */
    protected $repository;

    /**
     * @param TrademarkRepository $repository
     */
    public function __construct(TrademarkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SaveTrademarkRequest $request
     */
    public function saveTrademark(SaveTrademarkRequest $request)
    {
        $trademark = new Trademark($request->name);

        $trademark = $this->repository->persist($trademark);

        $this->getEventManager()->trigger(
            (new TrademarkEvent('TrademarkSaved'))->setTrademark($trademark)
        );
    }
}
