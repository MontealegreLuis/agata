<?php
/**
 * DTO that contains all available trademarks information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\TrademarkDto;

/**
 * DTO that contains all available trademarks information
 */
class GetAllTrademarksResponse
{
    /** @type TrademarkDto[] */
    public $trademarks;

    /** @param Trademark[] $trademarks */
    public function __construct(array $trademarks)
    {
        array_walk($trademarks, function(Trademark &$trademark){
            $trademark = $trademark->render(new TrademarkDto());
        });
        $this->trademarks = $trademarks;
    }
}
