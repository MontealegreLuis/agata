<?php
/**
 * Update the information of a given trademark
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\TrademarkEvent;

/**
 * Update the information of a given trademark
 */
class UpdateTrademark
{
    use ProvidesEvents;

    /** @type TrademarkRepository */
    protected $repository;

    /**
     * @param TrademarkRepository $repository
     */
    public function __construct(TrademarkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  UpdateTrademarkRequest $request
     * @return TrademarkResponse
     */
    public function updateTrademark(UpdateTrademarkRequest $request)
    {
        $trademark = $this->repository->trademarkOfId($request->trademarkId);
        $eventManager = $this->getEventManager();

        if (!$trademark) {
            $eventManager->trigger('TrademarkNotFound');

            return;
        }

        $trademark->rename($request->name);

        $this->repository->persist($trademark);

        $eventManager->trigger((new TrademarkEvent('TrademarkUpdated'))->setTrademark($trademark));

        return new TrademarkResponse($trademark);
    }
}
