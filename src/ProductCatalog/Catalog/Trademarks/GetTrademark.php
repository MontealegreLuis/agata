<?php
/**
 * Get the trademark with the given ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \ProductCatalog\Products\TrademarkRepository;
use \Zend\EventManager\ProvidesEvents;

/**
 * Get the trademark with the given ID
 */
class GetTrademark
{
    use ProvidesEvents;

    /** @type TrademarkRepository */
    protected $repository;

    /**
     * @param TrademarkRepository $repository
     */
    public function __construct(TrademarkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  TrademarkRequest  $request
     * @return TrademarkResponse
     */
    public function getTrademark(TrademarkRequest $request)
    {
        $trademark = $this->repository->trademarkOfId($request->trademarkId);

        if (!$trademark) {
            $this->getEventManager()->trigger('TrademarkNotFound');
        }

        return new TrademarkResponse($trademark);
    }
}
