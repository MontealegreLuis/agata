<?php
/**
 * DTO for use cases that retrieve the information of a trademark by its ID.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

/**
 * DTO for use cases that retrieve the information of a trademark by its ID.
 */
class TrademarkRequest
{
    /** @type integer */
    public $trademarkId;

    /**
     * @param integer $trademarkId
     */
    public function __construct($trademarkId)
    {
        $this->trademarkId = $trademarkId;
    }
}
