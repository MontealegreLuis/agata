<?php
/**
 * DTO that contains a given trademark information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \ProductCatalog\Products\Trademark;

/**
 * DTO that contains a given trademark information
 */
class SaveTrademarkRequest
{
    /**
     * @var string
     */
    public $name;

    /**
     * @param string[] $values
     */
    public function __construct(array $values)
    {
        $this->name = $values['name'];
    }
}
