<?php
/**
 * Get all the available trademarks
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use ProductCatalog\Products\TrademarkRepository;

/**
 * Get all the available trademarks
 */
class GetAllTrademarks
{
    /** @type TrademarkRepository */
    protected $repository;

    /**
     * @param TrademarkRepository $repository
     */
    public function __construct(TrademarkRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return GetAllTrademarksResponse
     */
    public function getAllTrademarks()
    {
        return new GetAllTrademarksResponse($this->repository->allTrademarks());
    }
}
