<?php
/**
 * DTO that contains a given trademark information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Trademarks;

use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\TrademarkDto;

/**
 * DTO that contains a given trademark information
 */
class TrademarkResponse
{
    /** @type TrademarkDto */
    public $trademark;

    /**
     * @param Trademark $trademark
     */
    public function __construct(Trademark $trademark)
    {
        $this->trademark = $trademark->render(new TrademarkDto());
    }
}
