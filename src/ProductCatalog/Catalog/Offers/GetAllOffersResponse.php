<?php
/**
 * DTO that contains all available offers information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\Offer;
use \ProductCatalog\Products\OfferDto;

/**
 * DTO that contains all available offers information
 */
class GetAllOffersResponse
{
    /** @type OfferDto[] */
    public $offers;

    /**
     * @param Offer[] $offers
     */
    public function __construct(array $offers)
    {
        array_walk($offers, function(Offer &$offer){
            $offer = $offer->render(new OfferDto());
        });
        $this->offers = $offers;
    }
}
