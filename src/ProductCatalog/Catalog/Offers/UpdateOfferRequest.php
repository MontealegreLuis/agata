<?php
/**
 * DTO that contains a given offer information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

/**
 * DTO that contains a given offer information
 */
class UpdateOfferRequest extends SaveOfferRequest
{
    /** @type integer */
    public $offerId;

    /**
     * @param mixed[] $values
     */
    public function __construct(array $values)
    {
        parent::__construct($values);
        $this->offerId = $values['id'];
    }
}
