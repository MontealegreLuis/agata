<?php
namespace ProductCatalog\Catalog\Offers;

class ApplyOfferToProductsRequest
{
    /** @type integer */
    public $offerId;

    /** @type boolean */
    public $allProducts;

    /** @type integer[] */
    public $criteria;

    /** @type integer[] */
    public $products;

    public function __construct(array $values)
    {
        $this->offerId = $values['id'];

        $criteria = [];
        if (!empty($values['categoryId'])) {
            $criteria[':categoryId']  = $values['categoryId'];
        }
        if (!empty($values['trademarkId'])) {
            $criteria[':trademarkId']  = $values['trademarkId'];
        }
        $this->criteria = $criteria;

        $this->allProducts = false;
        if (!empty($values['allProducts'])) {
            $this->allProducts = true;
        }

        $this->products = [];
        if (!empty($values['products'])) {
            $this->products = $values['products'];
        }
    }
}