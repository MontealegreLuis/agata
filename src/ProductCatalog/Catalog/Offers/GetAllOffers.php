<?php
/**
 * Use case to get all the available offers information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\OfferRepository;

/**
 * Use case to get all the available offers information
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
class GetAllOffers
{
    /** @type OfferRepository */
    protected $repository;

    /**
     * @param OfferRepository $repository
     */
    public function __construct(OfferRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \ProductCatalog\Products\Offer[]
     */
    public function getAllOffers()
    {
        return new GetAllOffersResponse($this->repository->allOffers());
    }
}
