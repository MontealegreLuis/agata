<?php
namespace ProductCatalog\Catalog\Offers;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\OfferRepository;

class ApplyOfferToProducts
{
    use ProvidesEvents;

    /** @type OfferRepository */
    protected $repository;

    /**
     * @param OfferRepository $repository
     */
    public function __construct(OfferRepository $repository)
    {
        $this->repository = $repository;
    }

    public function applyOffer(ApplyOfferToProductsRequest $request)
    {
        if ($request->allProducts) {
            $this->repository->applyToProductsThatMeet($request->criteria, $request->offerId);
        } else {
            $this->repository->applyToProducts($request->products, $request->offerId);
        }

        $this->getEventManager()->trigger('OfferApplied');
    }
}
