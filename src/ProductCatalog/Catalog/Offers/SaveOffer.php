<?php
/**
 * Save a new offer information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\OfferRepository;
use \ProductCatalog\Products\OfferFactory;
use \ProductCatalog\Products\OfferEvent;
use \ProductCatalog\Products\Offer;
use \Zend\EventManager\ProvidesEvents;

/**
 * Save a new offer information
 */
class SaveOffer
{
    use ProvidesEvents;

    /** @type OfferRepository */
    protected $repository;

    /** @type OfferFactory */
    protected $factory;

    /**
     * @param OfferRepository $repository
     * @param OfferFactory    $factory
     */
    public function __construct(OfferRepository $repository, OfferFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param SaveOfferRequest $request
     */
    public function saveOffer(SaveOfferRequest $request)
    {
        $offer = $this->factory->create(
            $request->duration,
            $request->type,
            $request->description,
            $request->amount,
            $request->image
        );

        $offer = $this->repository->persist($offer);

        $this->getEventManager()->trigger((new OfferEvent('OfferSaved'))->setOffer($offer));
    }
}
