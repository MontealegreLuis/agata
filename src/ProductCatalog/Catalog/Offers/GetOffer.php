<?php
/**
 * Get an offer information by its ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\OfferRepository;
use \Zend\EventManager\ProvidesEvents;

/**
 * Get an offer information by its ID
 */
class GetOffer
{
    use ProvidesEvents;

    /** @type OfferRepository */
    protected $allOffers;

    /**
     * @param OfferRepository $allOffers
     */
    public function __construct(OfferRepository $allOffers)
    {
        $this->allOffers = $allOffers;
    }

    /**
     * @param  OfferRequest  $request
     * @return OfferResponse
     */
    public function getOffer(OfferRequest $request)
    {
        $offer = $this->allOffers->offerOfId($request->offerId);

        if (!$offer) {
            $this->getEventManager()->trigger('OfferNotFound');
        }

        return new OfferResponse($offer);
    }
}
