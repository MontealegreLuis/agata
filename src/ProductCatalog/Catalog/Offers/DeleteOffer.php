<?php
/**
 * Delete an offer by its ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\OfferRepository;
use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\Offer;
use \ProductCatalog\Products\OfferEvent;

/**
 * Delete an offer by its ID
 */
class DeleteOffer
{
    use ProvidesEvents;

    /** @type OfferRepository*/
    protected $allOffers;

    /**
     * @param OfferRepository $allOffers
     */
    public function __construct(OfferRepository $allOffers)
    {
        $this->allOffers = $allOffers;
    }

    /**
     * @param OfferRequest $request
     */
    public function deleteOffer(OfferRequest $request)
    {
        $offer = $this->allOffers->offerOfId($request->offerId);

        if (!$offer) {
            return $this->getEventManager()->trigger('OfferNotFound');
        }

        $this->allOffers->remove($offer);

        $this->getEventManager()->trigger((new OfferEvent('OfferDeleted'))->setOffer($offer));
    }
}
