<?php
/**
 * DTO that contains a given offer information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\Offer;
use \ProductCatalog\Products\DateRange;
use \ProductCatalog\Products\OfferType;
use \Money\Money;
use \Money\Currency;
use \SplFileInfo;
use \DateTime;
use \DateTimeZone;
use ProductCatalog\Products\Percentage;

/**
 * DTO that contains a given offer information
 */
class SaveOfferRequest
{
    /** @type DateTime */
    public $duration;

    /** @type OfferType */
    public $type;

    /** @type Percentage | Money */
    public $amount;

    /** @type SplFileInfo */
    public $image;

    /** @type string */
    public $description;

    /**
     * @param mixed[] $values
     */
    public function __construct(array $values)
    {
        $this->description = $values['description'];

        $timezone = new DateTimeZone('America/Mexico_city');
        $startDate = DateTime::createFromFormat('Y-m-d', $values['startDate'], $timezone);
        $stopDate = DateTime::createFromFormat('Y-m-d', $values['stopDate'], $timezone);
        $this->duration = new DateRange($startDate, $stopDate);

        $this->type = new OfferType((integer) $values['type']);

        if ($this->type->getType() === OfferType::PERCENTAGE) {
            $this->amount = new Percentage($values['amount']);
        } else {
            $this->amount = new Money(
                (integer) (number_format(str_replace(',', '', $values['amount']), 2, '.', '') * 100),
                new Currency('MXN')
            );
        }

        $this->image = null;
        if (!empty($values['image'])) {
            $this->image = new SplFileInfo($values['image']);
        }
    }
}
