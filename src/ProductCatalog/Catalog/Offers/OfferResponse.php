<?php
/**
 * DTO that contains a given offer information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \ProductCatalog\Products\Offer;
use \ProductCatalog\Products\OfferDto;

/**
 * DTO that contains a given offer information
 */
class OfferResponse
{
    /** @type OfferDto */
    public $offer;

    /**
     * @param Offer $offer
     */
    public function __construct(Offer $offer)
    {
        $this->offer = $offer->render(new OfferDto());
    }
}
