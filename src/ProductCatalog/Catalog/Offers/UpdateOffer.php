<?php
/**
 * Update an offer information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\OfferRepository;
use \ProductCatalog\Products\OfferEvent;

/**
 * Update an offer information
 */
class UpdateOffer
{
    use ProvidesEvents;

    /** @type OfferRepository */
    protected $allOffers;

    /**
     * @param OfferRepository $allOffers
     */
    public function __construct(OfferRepository $allOffers)
    {
        $this->allOffers = $allOffers;
    }

    /**
     * @param UpdateProductRequest $request
     */
    public function updateOffer(UpdateOfferRequest $request)
    {
        $offer = $this->allOffers->offerOfId($request->offerId);

        if (!$offer) {
            return $this->getEventManager()->trigger('OfferNotFound');
        }

        $offer->edit($request->duration, $request->description, $request->amount);

        $offer = $this->allOffers->persist($offer);

        $this->getEventManager()->trigger((new OfferEvent('OfferUpdated'))->setOffer($offer));
    }
}
