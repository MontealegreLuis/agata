<?php
/**
 * DTO for use cases that retrieve the information of an offer by its ID.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Offers;

/**
 * DTO for use cases that retrieve the information of an offer by its ID.
 */
class OfferRequest
{
    /** @type integer */
    public $offerId;

    /**
     * @param integer $offerId
     */
    public function __construct($offerId)
    {
        $this->offerId = $offerId;
    }
}
