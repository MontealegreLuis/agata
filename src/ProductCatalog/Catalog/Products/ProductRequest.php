<?php
/**
 * DTO for use cases that retrieve the information of a product by its ID.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

/**
 * DTO for use cases that retrieve the information of a product by its ID.
 */
class ProductRequest
{
    /** @type integer */
    public $productId;

    /**
     * @param integer $productId
     */
    public function __construct($productId)
    {
        $this->productId = $productId;
    }
}
