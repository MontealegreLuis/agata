<?php
/**
 * Delete a product by its ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\ProductRepository;
use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\Product;
use ProductCatalog\Products\ProductEvent;

/**
 * Delete a product by its ID
 */
class DeleteProduct
{
    use ProvidesEvents;

    /** @type ProductRepository*/
    protected $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ProductRequest $request
     */
    public function deleteProduct(ProductRequest $request)
    {
        $product = $this->repository->productOfId($request->productId);

        if (!$product) {
            return $this->getEventManager()->trigger('ProductNotFound');
        }

        $this->repository->remove($product);

        $this->getEventManager()->trigger(
            (new ProductEvent('ProductDeleted'))->setProduct($product)
        );
    }
}
