<?php
/**
 * Get all the available products
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\ProductRepository;

/**
 * Get all the available products
 */
class GetAllProducts
{
    /** @type ProductRepository */
    protected $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return GetAllProductsResponse
     */
    public function getAllProducts(array $params = [])
    {
        return new GetAllProductsResponse($this->repository->allProducts($params));
    }
}
