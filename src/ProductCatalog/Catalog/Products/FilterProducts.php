<?php
/**
 * Filters the list of products by a given criteria
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\ProductRepository;

/**
 * Filters the list of products by a given criteria
 */
class FilterProducts
{
    /** @type ProductRepository */
    protected $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return GetAllProductsResponse
     */
    public function getProductsThatMeetCriteria(FilterProductsRequest $request)
    {
        $products = $this->repository->productsThatMeetCriteria($request->criteria);

        return new GetAllProductsResponse($products);
    }
}
