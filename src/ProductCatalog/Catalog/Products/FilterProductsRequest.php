<?php
namespace ProductCatalog\Catalog\Products;

class FilterProductsRequest
{
    /** @type array */
    public $criteria;

    /**
     * Only use valid criteria values
     *
     * @param array $criteria
     */
    public function __construct(array $criteria)
    {
        $this->criteria = [];
        if (!empty($criteria['category'])) {
            $this->criteria[':categoryUrl'] = $criteria['category'];
        }
        if (!empty($criteria['categoryId'])) {
            $this->criteria[':categoryId'] = $criteria['categoryId'];
        }
        if (!empty($criteria['trademarkId'])) {
            $this->criteria[':trademarkId'] = $criteria['trademarkId'];
        }
    }
}
