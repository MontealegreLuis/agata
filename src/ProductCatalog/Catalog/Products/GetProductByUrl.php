<?php
/**
 * Retrieve a given product by its slug
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ProductCatalog\Catalog\Products;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\ProductEvent;
use \ProductCatalog\Products\ProductRepository;

/**
 * Retrieve a given product by its slug
 */
class GetProductByUrl
{
    use ProvidesEvents;

    /** @type ProductRepository */
    protected $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  string          $url
     * @return ProductResponse
     */
    public function getProduct($url)
    {
        $product = $this->repository->productOfUrl($url);

        if (!$product) {
            $this->getEventManager()->trigger('ProductNotFound');
        }

        $this->getEventManager()->trigger((new ProductEvent('ProductFound'))->setProduct($product));

        return new ProductResponse($product);
    }
}
