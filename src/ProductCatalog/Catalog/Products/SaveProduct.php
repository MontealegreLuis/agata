<?php
/**
 * Save a new product information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\Products\ProductRepository;
use \ProductCatalog\Products\Product;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\CategoryRepository;
use \ProductCatalog\Products\ProductEvent;

/**
 * Save a new product information
 */
class SaveProduct
{
    use ProvidesEvents;

    /** @type ProductRepository */
    protected $repository;

    /** @type TrademarkRepository */
    protected $trademarkRepository;

    /** @type CategoryRepository */
    protected $categoryRepository;

    /**
     * @param ProductRepository   $repository
     * @param TrademarkRepository $trademarkRepository
     * @param CategoryRepository  $categoryRepository
     */
    public function __construct(
        ProductRepository $repository,
        TrademarkRepository $trademarkRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->repository = $repository;
        $this->trademarkRepository = $trademarkRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param array $productValues
     */
    public function saveProduct(SaveProductRequest $request)
    {
        $trademark = $this->trademarkRepository->trademarkOfId($request->trademarkId);
        $category = $this->categoryRepository->categoryOfId($request->categoryId);

        $product = new Product(
            $request->model,
            $request->features,
            $request->price,
            $trademark,
            $category,
            $request->image
        );

        $product = $this->repository->persist($product);

        $this->getEventManager()->trigger((new ProductEvent('ProductSaved'))->setProduct($product));
    }
}
