<?php
/**
 * DTO that contains a given product information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

/**
 * DTO that contains a given product information
 */
class UpdateProductRequest extends SaveProductRequest
{
    /** @type integer */
    public $productId;

    /**
     * @param mixed[] $values
     */
    public function __construct(array $values)
    {
        parent::__construct($values);
        $this->productId = $values['id'];
    }
}
