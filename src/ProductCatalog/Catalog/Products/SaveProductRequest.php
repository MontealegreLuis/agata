<?php
/**
 * DTO that contains a given product information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\Product;
use \Money\Money;
use \Money\Currency;
use \SplFileInfo;

/**
 * DTO that contains a given product information
 */
class SaveProductRequest
{
    /** @type string */
    public $model;

    /** @type string */
    public $features;

    /** @type Money */
    public $price;

    /** @type integer */
    public $trademarkId;

    /** @type integer */
    public $categoryId;

    /** @type SplFileInfo */
    public $image;

    /**
     * @param mixed[] $values
     */
    public function __construct(array $values)
    {
        $this->model = $values['model'];
        $this->features = $values['features'];
        $this->trademarkId = $values['trademarkId'];
        $this->categoryId = $values['categoryId'];

        $this->price = new Money(
            (integer) (number_format(str_replace(',', '', $values['price']), 2, '.', '') * 100), new Currency('MXN')
        );

        $this->image = null;
        if (is_array($values['image'])) {
            $this->image = $values['image'];
        }
    }
}
