<?php
/**
 * DTO that contains a given product information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\Product;
use \ProductCatalog\Products\ProductDto;

/**
 * DTO that contains a given roduct information
 */
class ProductResponse
{
    /** @type ProductDto */
    public $product;

    /**
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product->render(new ProductDto());
    }
}
