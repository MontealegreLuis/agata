<?php
/**
 * Get a product information by its ID
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\ProductRepository;
use \Zend\EventManager\ProvidesEvents;

/**
 * Get a product information by its ID
 */
class GetProduct
{
    use ProvidesEvents;

    /** @type ProductRepository */
    protected $repository;

    /**
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param  ProductRequest  $request
     * @return ProductResponse
     */
    public function getProduct(ProductRequest $request)
    {
        $product = $this->repository->productOfId($request->productId);

        if (!$product) {
            $this->getEventManager()->trigger('ProductNotFound');
        }

        return new ProductResponse($product);
    }
}
