<?php
/**
 * Update a product information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \Zend\EventManager\ProvidesEvents;
use \ProductCatalog\DomainEvents\ProvidesDomainEvents;
use \ProductCatalog\DomainEvents\DomainEventsProvider;
use \ProductCatalog\Products\ProductRepository;
use \ProductCatalog\Products\Product;
use \ProductCatalog\Products\ProductEvent;
use \ProductCatalog\Products\TrademarkRepository;
use \ProductCatalog\Products\CategoryRepository;

/**
 * Update a product information
 */
class UpdateProduct implements DomainEventsProvider
{
    use ProvidesEvents, ProvidesDomainEvents;

    /** @type ProductRepository */
    protected $repository;

    /** @type TrademarkRepository */
    protected $trademarkRepository;

    /** @type CategoryRepository */
    protected $categoryRepository;

    /**
     * @param ProductRepository   $repository
     * @param TrademarkRepository $trademarkRepository
     * @param CategoryRepository  $categoryRepository
     */
    public function __construct(
        ProductRepository $repository,
        TrademarkRepository $trademarkRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->repository = $repository;
        $this->trademarkRepository = $trademarkRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param UpdateProductRequest $request
     */
    public function updateProduct(UpdateProductRequest $request)
    {
        $product = $this->repository->productOfId($request->productId);

        if (!$product) {
            return $this->getEventManager()->trigger('ProductNotFound');
        }
        $product->setEventsBus($this->eventsBus);

        $trademark = $this->trademarkRepository->trademarkOfId($request->trademarkId);
        $category = $this->categoryRepository->categoryOfId($request->categoryId);

        $product->edit($request->model, $request->features, $request->price, $trademark, $category);

        $product = $this->repository->persist($product);

        $this->getEventManager()->trigger((new ProductEvent('ProductUpdated'))->setProduct($product));
    }
}
