<?php
/**
 * DTO that contains all available products information
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Catalog\Products;

use \ProductCatalog\Products\Product;
use \ProductCatalog\Products\ProductDto;

/**
 * DTO that contains all available products information
 */
class GetAllProductsResponse
{
    /** @type ProductDto[] */
    public $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products)
    {
        array_walk($products, function(Product &$product){
            $product = $product->render(new ProductDto());
        });
        $this->products = $products;
    }
}
