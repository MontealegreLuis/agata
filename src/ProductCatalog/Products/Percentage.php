<?php
namespace ProductCatalog\Products;

use \InvalidArgumentException;

class Percentage
{
    /** @type integer */
    protected $percentage;

    /**
     * @param  integer                  $percentage
     * @throws InvalidArgumentException
     */
    public function __construct($percentage)
    {
        if ($percentage < 0 || $percentage > 100) {
            throw new InvalidArgumentException(
                "Percentage must be a number between 0 and 100, $percentage given"
            );
        }
        $this->percentage = $percentage;
    }

    /**
     * @return integer
     */
    public function getPercentage()
    {
        return $this->percentage;
    }
}
