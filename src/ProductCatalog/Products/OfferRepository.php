<?php
namespace ProductCatalog\Products;

interface OfferRepository
{
    /**
     * @param Offer $offer
     */
    public function persist(Offer $offer);

    /**
     * @param  integer $offerId
     * @return Offer
     */
    public function offerOfId($offerId);

    /**
     * @return Offer[]
     */
    public function allOffers();

    /**
     * @param array   $criteria
     * @param integer $offerId
     */
    public function applyToProductsThatMeet(array $criteria, $offerId);

    /**
     * @param array   $products
     * @param integer $offerId
     */
    public function applyToProducts(array $products, $offerId);

    /**
     * @param Offer $offer
     */
    public function remove(Offer $offer);
}
