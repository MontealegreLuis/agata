<?php
/**
 * Category repository interface
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

/**
 * Category repository interface
 */
interface CategoryRepository
{
    /**
     * @return Category[]
     */
    public function allCategories();

    /**
     * @param  integer  $categoryId
     * @return Category
     */
    public function categoryOfId($categoryId);

    /**
     * @param  Category $category
     * @return Category
     */
    public function persist(Category $category);

    /**
     * @param Category $category
     */
    public function remove(Category $category);
}
