<?php
namespace ProductCatalog\Products;

interface TrademarkRepository
{
    /**
     * @return Trademark[]
     */
    public function allTrademarks();

    /**
     * @param  int       $trademarkId
     * @return Trademark
     */
    public function trademarkOfId($trademarkId);

    /**
     * @param Trademark $trademark
     */
    public function persist(Trademark $trademark);

    /**
     * @param Trademark $trademark
     */
    public function remove(Trademark $trademark);
}
