<?php
namespace ProductCatalog\Products;

use \SplFileInfo;
use \Money\Money;

class SpecialPriceOffer extends Offer
{
    /** @type Money */
    protected $price;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param SplFileInfo $image
     * @param integer     $id
     * @param Money       $price
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        Money $price,
        SplFileInfo $image = null,
        $id = null
    )
    {
        parent::__construct($duration, $type, $description, $image, $id);
        $this->price = $price;
    }

    /**
     * @param DateRange $duration
     * @param OfferType $type
     * @param string    $description
     * @param Money     $price
     */
    public function edit(DateRange $duration, $description, Money $price= null)
    {
        parent::edit($duration, $description);
        $this->price = $price;
    }
}
