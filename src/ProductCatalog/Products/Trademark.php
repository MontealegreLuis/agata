<?php
/**
 * Trademark
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

/**
 * Trademark
 */
class Trademark
{
    /** @type integer */
    protected $id;

    /** @type string */
    protected $name;

    /** @type string */
    protected $url;

    /**
     * @param string  $name
     * @param integer $url
     * @param integer $id
     */
    public function __construct($name, $url = null, $id = null)
    {
        $this->name = $name;
        $this->url = $url;
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function rename($name)
    {
        $this->name = $name;
    }

    /**
     * @param  TrademarkDto $view
     * @return TrademarkDto
     */
    public function render(TrademarkDto $view)
    {
        $view->name = $this->name;
        $view->url = $this->url;
        $view->id = $this->id;

        return $view;
    }
}
