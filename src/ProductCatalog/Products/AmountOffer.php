<?php
namespace ProductCatalog\Products;

use \SplFileInfo;
use \Money\Money;

class AmountOffer extends Offer
{
    /** @type Money */
    protected $amount;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param SplFileInfo $image
     * @param integer     $id
     * @param Money       $amount
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        Money $amount,
        SplFileInfo $image = null,
        $id = null
    )
    {
        parent::__construct($duration, $type, $description, $image, $id);
        $this->amount = $amount;
    }

    /**
     * @param DateRange $duration
     * @param OfferType $type
     * @param string    $description
     * @param Money     $amount
     */
    public function edit(DateRange $duration, $description, Money $amount = null)
    {
        parent::edit($duration, $description);
        $this->amount = $amount;
    }
}
