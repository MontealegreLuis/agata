<?php
/**
 * Offer event
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \Zend\EventManager\Event;

/**
 * Offer event
 */
class OfferEvent extends Event
{
    /** @type OfferDto */
    protected $offer;

    /**
     * @param  Offer      $offer
     * @return OfferEvent
     */
    public function setOffer(Offer $offer)
    {
        $this->offer = $offer->render(new OfferDto());

        return $this;
    }

    /**
     * @return OfferDto
     */
    public function getOffer()
    {
        return $this->offer;
    }
}
