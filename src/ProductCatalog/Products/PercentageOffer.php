<?php
namespace ProductCatalog\Products;

use \SplFileInfo;

class PercentageOffer extends Offer
{
    /** @type Percentage */
    protected $percentage;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param SplFileInfo $image
     * @param integer     $id
     * @param Percentage  $price
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        Percentage $percentage,
        SplFileInfo $image = null,
        $id = null
    )
    {
        parent::__construct($duration, $type, $description, $image, $id);
        $this->percentage = $percentage;
    }

    /**
     * @param DateRange  $duration
     * @param OfferType  $type
     * @param string     $description
     * @param Percentage $percentage
     */
    public function edit(DateRange $duration, $description, Percentage $percentage = null)
    {
        parent::edit($duration, $description);
        $this->percentage = $percentage;
    }
}
