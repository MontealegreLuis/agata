<?php
/**
 * DTO for Product entities
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \ArrayObject;
use \SplFileInfo;

/**
 * DTO for Product entities
 */
class ProductDto extends ArrayObject
{
    /** @type integer */
    protected $id;

    /** @type string */
    protected $model;

    /** @type string */
    protected $features;

    /** @type Trademark */
    protected $trademark;

    /** @type Category */
    protected $category;

    /** @type string */
    protected $url;

    /** @type SplFileInfo */
    protected $image;

    /**
     * Set array as properties flag by default
     */
    public function __construct($array = [])
    {
        if (!empty($array['image']) && $array['image'] instanceof SplFileInfo) {
            $array['image'] = $array['image']->getBasename();
        }
        parent::__construct($array);
        $this->setFlags(parent::ARRAY_AS_PROPS);
    }
}
