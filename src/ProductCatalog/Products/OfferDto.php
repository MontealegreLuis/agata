<?php
/**
 * DTO for Offer entities
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \ArrayObject;
use \DateTime;
use \SplFileInfo;

/**
 * DTO for Offer entities
 *
 * @property DateTime $startDate
 * @property DateTime $stopDate
 * @property string   $type
 * @property string   $description
 * @property string   $image
 */
class OfferDto extends ArrayObject
{
    /** @type integer */
    protected $id;

    /** @type DateTime */
    protected $startDate;

    /** @type DateTime */
    protected $stopDate;

    /** @type integer */
    protected $type;

    /** @type string */
    protected $description;

    /** @type string */
    protected $image;

    /** @type number */
    protected $amount;

    /**
     * Set array as properties flag by default
     */
    public function __construct($array = [])
    {
        if (!empty($array['image']) && $array['image'] instanceof SplFileInfo) {
            $array['image'] = $array['image']->getBasename();
        }
        parent::__construct($array);
        $this->setFlags(parent::ARRAY_AS_PROPS);
    }
}
