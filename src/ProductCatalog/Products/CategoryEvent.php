<?php
/**
 * Category event
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \Zend\EventManager\Event;

/**
 * Category event
 *
 * Pass the Category DTO to domain and application events
 */
class CategoryEvent extends Event
{
    /** @type CategoryDto */
    protected $category;

    /**
     * @param  CategoryDto   $category
     * @return CategoryEvent
     */
    public function setCategory(Category $category)
    {
        $this->category = $category->render(new CategoryDto());

        return $this;
    }

    /**
     * @return CategoryDto
     */
    public function getCategory()
    {
        return $this->category;
    }
}
