<?php
/**
 * Trademark event
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \Zend\EventManager\Event;
use \ProductCatalog\Products\Trademark;
use \ProductCatalog\Products\TrademarkDto;

/**
 * Trademark event
 */
class TrademarkEvent extends Event
{
    /** @type TrademarkDto */
    protected $trademark;

    /**
     * @param  Trademark      $trademark
     * @return TrademarkEvent
     */
    public function setTrademark(Trademark $trademark)
    {
        $this->trademark = $trademark->render(new TrademarkDto());

        return $this;
    }

    /**
     * @return TrademarkDto
     */
    public function getTrademark()
    {
        return $this->trademark;
    }
}
