<?php
namespace ProductCatalog\Products;

interface ProductRepository
{
    /**
     * @return Product[]
     */
    public function allProducts();

    /**
     * @param  array     $criteria
     * @return Product[]
     */
    public function productsThatMeetCriteria(array $criteria);

    /**
     * @param  int     $productId
     * @return Product
     */
    public function productOfId($productId);

    /**
     * @param string $productUrl
     */
    public function productOfUrl($productUrl);

    /**
     * @param Product $product
     */
    public function persist(Product $product);

    /**
     * @param Product $product
     */
    public function remove(Product $product);
}
