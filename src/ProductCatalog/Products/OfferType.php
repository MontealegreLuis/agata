<?php
namespace ProductCatalog\Products;

use \InvalidArgumentException;

class OfferType
{
    /** @type int The offer consist of a percentage of the product's price */
    const PERCENTAGE = 0;

    /**
     * @type integer
     *     The offer consist of a fixed amount that will be sustracted to the product's price
     */
    const FIXED_AMOUNT = 1;

    /**
     * @type integer
     *     The offer consist of a special price that will replace the product's price while the
     *     offer is valid
     */
    const SPECIAL_PRICE = 2;

    /** @type integer */
    protected $type;

    /**
     * @param  integer                  $type
     * @throws InvalidArgumentException
     */
    public function __construct($type)
    {
        if (!in_array($type, [static::PERCENTAGE, static::FIXED_AMOUNT, static::SPECIAL_PRICE])) {
            throw new InvalidArgumentException('The type of offer provide is not valid');
        }
        $this->type = (integer) $type;
    }

    /**
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
}
