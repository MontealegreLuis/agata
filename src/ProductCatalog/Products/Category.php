<?php
/**
 * Category entity
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \SplFileInfo;
use \ProductCatalog\DomainEvents\ProvidesDomainEvents;
use \ProductCatalog\DomainEvents\DomainObjectChanged;
use \ProductCatalog\DomainEvents\DomainEventsProvider;

/**
 * Category entity
 */
class Category implements DomainEventsProvider
{
    use ProvidesDomainEvents;

    /** @type integer */
    protected $id;

    /** @type string */
    protected $name;

    /** @type string */
    protected $url;

    /** @type SplFileInfo */
    protected $image;

    /** @type Category */
    protected $parentCategory;

    /** @type integer */
    protected $productCount;

    /**
     * @param string      $name
     * @param string      $url
     * @param SplFileInfo $image
     * @param Category    $parentCategory
     * @param integer     $productCount
     */
    public function __construct(
        $name,
        SplFileInfo $image = null,
        Category $parentCategory = null,
        $url = null,
        $productCount = null,
        $id = null
    )
    {
        $this->name = $name;
        $this->image = $image;
        if ($parentCategory) {
            $this->setParentCategory($parentCategory);
        }
        $this->url = $url;
        $this->productCount = $productCount;
        $this->id = $id;
    }

    /**
     * @param string      $name
     * @param SplFileInfo $image
     * @param Category    $parentCategory
     */
    public function edit($name, SplFileInfo $image = null, Category $parentCategory = null)
    {
        $this->rename($name);
        $this->image = $image;
        if ($parentCategory) {
           $this->setParentCategory($parentCategory);
        } else {
           $this->clearParentCategory();
        }
    }

    /**
     * @param string $name
     */
    protected function rename($name)
    {
        if ($this->name !== $name) {
            $this->eventsBus->post(new DomainObjectChanged('CategoryRenamed', $this->name, $name));
            $this->name = $name;
        }
    }

    /**
     * @param  Category        $parentCategory
     * @throws DomainException
     */
    protected function setParentCategory(Category $parentCategory)
    {
        if ($this->id === $parentCategory->id) {
            $exception = new CategoryDomainException('A category cannot be its own sub-category');
            $exception->setCategory($this);
            throw $exception;
        }
        $this->parentCategory = $parentCategory;
    }

    /**
     * This category is now a parent category.
     */
    protected function clearParentCategory()
    {
        $this->parentCategory = null;
    }

    /**
     * @param  CategoryDto $view
     * @return CategoryDto
     */
    public function render(CategoryDto $view)
    {
        $view->name = $this->name;
        $view->url = $this->url;
        $view->image = null;
        if ($this->image) {
           $view->image = $this->image->getBasename();
        }
        if ($this->parentCategory) {
           $view->parentCategory =  $this->parentCategory->render(new CategoryDto());
        }
        $view->productCount = $this->productCount;
        $view->id   = $this->id;

        return $view;
    }
}
