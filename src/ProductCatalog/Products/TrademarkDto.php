<?php
/**
 * DTO for Trademark entities
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \ArrayObject;

/**
 * DTO for Trademark entities
 */
class TrademarkDto extends ArrayObject
{
    /** @type integer */
    protected $id;

    /** @type string */
    protected $name;

    /** @type string */
    protected $url;

    /**
     * Set array as properties flag by default
     */
    public function __construct($array = [])
    {
        parent::__construct($array);
        $this->setFlags(parent::ARRAY_AS_PROPS);
    }
}
