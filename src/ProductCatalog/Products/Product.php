<?php
namespace ProductCatalog\Products;

use \SplFileInfo;
use \ProductCatalog\DomainEvents\ProvidesDomainEvents;
use \ProductCatalog\DomainEvents\DomainObjectChanged;
use \ProductCatalog\DomainEvents\DomainEventsProvider;
use \Money\Money;

class Product implements DomainEventsProvider
{
    use ProvidesDomainEvents;

    /** @type integer */
    protected $id;

    /** @type string */
    protected $model;

    /** @type Money */
    protected $price;

    /** @type string */
    protected $features;

    /** @type Trademark */
    protected $trademark;

    /** @type Category */
    protected $category;

    /** @type string */
    protected $url;

    /** @type SplFileInfo */
    protected $image;

    /**
     * @param string      $model
     * @param string      $features
     * @param Trademark   $trademark
     * @param Category    $category
     * @param SplFileInfo $image
     */
    public function __construct(
        $model,
        $features,
        Money $price,
        Trademark $trademark,
        Category $category,
        SplFileInfo $image = null,
        $url = null,
        $id = null
    )
    {
        $this->model = $model;
        $this->features = $features;
        $this->price = $price;
        $this->trademark = $trademark;
        $this->category = $category;
        $this->image = $image;
        $this->url = $url;
        $this->id = $id;
    }

    /**
     * @param string    $model
     * @param string    $features
     * @param Trademark $trademark
     * @param Category  $category
     */
    public function edit($model, $features, Money $price, Trademark $trademark, Category $category)
    {
        $this->rename($model);
        $this->features = $features;
        $this->price = $price;
        $this->trademark =$trademark;
        $this->category = $category;
    }

    /**
     * @param string $model
     */
    protected function rename($model)
    {
        if ($this->model !== $model) {
            $this->eventsBus->post(new DomainObjectChanged('ProductRenamed', $this->model, $model));
            $this->model = $model;
        }
    }

    /**
     * @param  ProductDto $view
     * @return ProductDto
     */
    public function render(ProductDto $view)
    {
        $view->model = $this->model;
        $view->features = $this->features;
        $view->price = $this->price->getAmount() / 100;
        $view->trademark = $this->trademark->render(new TrademarkDto());
        $view->category = $this->category->render(new CategoryDto());
        $view->image = null;
        if ($this->image) {
            $view->image = $this->image->getBasename();
        }
        $view->url = $this->url;
        $view->id = $this->id;

        return $view;
    }
}
