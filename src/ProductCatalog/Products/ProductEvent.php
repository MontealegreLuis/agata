<?php
/**
 * Product event
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
namespace ProductCatalog\Products;

use \Zend\EventManager\Event;

/**
 * Product event
 */
class ProductEvent extends Event
{
    /** @type ProductDto */
    protected $product;

    /**
     * @param  Product      $originalProduct
     * @return ProductEvent
     */
    public function setProduct(Product $product)
    {
        $this->product = $product->render(new ProductDto());

        return $this;
    }

    /**
     * @return ProductDto
     */
    public function getProduct()
    {
        return $this->product;
    }
}
