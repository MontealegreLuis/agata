<?php
namespace ProductCatalog\Products;

use \SplFileInfo;

class OfferFactory
{
    /**
     * @param  DateRange          $duration
     * @param  OfferType          $type
     * @param  string             $description
     * @param  Money | Percentage $amount
     * @param  SplFileInfo        $image
     * @param  string             $id
     * @return Offer
     */
    public function create(
        DateRange $duration,
        OfferType $type,
        $description,
        $amount,
        SplFileInfo $image = null,
        $id = null
    )
    {
        if ($type === OfferType::FIXED_AMOUNT) {
            return new AmountOffer($duration, $type, $description, $amount, $image, $id);
        }
        if ($type === OfferType::PERCENTAGE) {
            return new PercentageOffer($duration, $type, $description, $amount, $image, $id);
        }

        return new SpecialPriceOffer($duration, $type, $description, $amount, $image, $id);

    }
}
