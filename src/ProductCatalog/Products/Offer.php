<?php
namespace ProductCatalog\Products;

use \SplFileInfo;

abstract class Offer
{
    /** @type integer */
    protected $id;

    /** @type DateRange */
    protected $duration;

    /** @type OfferType */
    protected $type;

    /** @type string */
    protected $description;

    /** @type SplFileInfo */
    protected $image;

    /**
     * @param DateRange   $duration
     * @param OfferType   $type
     * @param string      $description
     * @param SplFileInfo $image
     * @param integer     $id
     */
    public function __construct(
        DateRange $duration,
        OfferType $type,
        $description,
        SplFileInfo $image = null,
        $id = null
    )
    {
        $this->duration = $duration;
        $this->type = $type;
        $this->description = $description;
        $this->image = $image;
        $this->id = $id;
    }

    /**
     * @param  OfferDto $view
     * @return OfferDto
     */
    public function render(OfferDto $view)
    {
        $view->id = $this->id;
        $view->startDate = $this->duration->getStartDate()->format('Y-m-d');
        $view->stopDate = $this->duration->getStopDate()->format('Y-m-d');
        $view->type = $this->type->getType();
        $view->description = $this->description;

        if ($this->image) {
            $view->image = $this->image->getBasename();
        }

        return $view;
    }

    /**
     * @param DateRange $duration
     * @param OfferType $type
     * @param string    $description
     */
    public function edit(DateRange $duration, $description)
    {
        $this->duration = $duration;
        $this->description = $description;
    }

    /**
     * @param Product $product
     */
    //abstract public function applyOffer(Product $product);
}
