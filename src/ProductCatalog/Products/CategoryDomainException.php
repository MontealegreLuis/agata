<?php
namespace ProductCatalog\Products;

use \DomainException;

class CategoryDomainException extends DomainException
{
    /** @type CategoryDto */
    protected $category;

    /**
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category->render(new CategoryDto());
    }

    /**
     * @return CategoryDto
     */
    public function getCategory()
    {
        return $this->category;
    }
}
