<?php
namespace ProductCatalog\Products;

use \DateTime;
use \InvalidArgumentException;

class DateRange
{
    /**
     * @var DateTime
     */
    protected $startDate;

    /**
     * @var DateTime
     */
    protected $stopDate;

    public function __construct(DateTime $startDate, DateTime $stopDate)
    {
        if ($stopDate < $startDate) {
            throw new InvalidArgumentException('Stop date must be a date before start date');
        }
        $this->startDate = $startDate;
        $this->stopDate = $stopDate;
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return DateTime
     */
    public function getStopDate()
    {
        return $this->stopDate;
    }
}
