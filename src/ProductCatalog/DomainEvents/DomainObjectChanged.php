<?php
/**
 * Domain event to represent a change in a domain object
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ProductCatalog\DomainEvents;

/**
 * Domain event to represent a change in a domain object
 */
class DomainObjectChanged extends DomainEvent
{
    /** @type mixed */
    protected $originalValue;

    /** @type mixed */
    protected $newValue;

    /**
     * @param string $name
     * @param mixed  $originalValue
     * @param mixed  $newValue
     */
    public function __construct($name, $originalValue, $newValue)
    {
        parent::__construct($name);
        $this->originalValue = $originalValue;
        $this->newValue = $newValue;
    }

    /**
     * @return mixed
     */
    public function getOriginalValue()
    {
        return $this->originalValue;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return $this->newValue;
    }
}
