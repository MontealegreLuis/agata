<?php
/**
 * Interface for objects that raise domain events
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ProductCatalog\DomainEvents;

/**
 * Interface for objects that raise domain events
 */
interface DomainEventsProvider
{
    /**
     * @param DomainEventsBus $bus
     */
    public function setEventsBus(DomainEventsBus $bus);
}
