<?php
/**
 * Handles domain events
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2014
 */
namespace ProductCatalog\DomainEvents;

/**
 * Handles domain events
 */
class DomainEventsBus
{
    /** @type DomainEvent[] */
    protected $events;

    /** @type Callable[] */
    protected $listeners;

    /**
     * Initialize events and event listeners
     */
    public function __construct()
    {
        $this->events = [];
        $this->listeners = [];
    }

    /**
     * @param DomainEvent $event
     */
    public function post(DomainEvent $event)
    {
        $this->events[] = $event;
    }

    /**
     * @param string   $eventName
     * @param Callable $listener
     */
    public function register($eventName, Callable $listener)
    {
        if (empty($this->listeners[$eventName])) {
            $this->listeners[$eventName] = [];
        }
        $this->listeners[$eventName][] = $listener;
    }

    /**
     * Call all the registered listeners for all the posted events
     */
    public function dispatchEvents()
    {
        foreach ($this->events as $event) {
            $this->callListenersForEvent($event);
        }
    }

    /**
     * Call all the listeners for the given event
     *
     * @param DomainEvent $eventName
     */
    protected function callListenersForEvent(DomainEvent $event)
    {
        foreach ($this->listeners[$event->getName()] as $listener) {
            $listener($event);
        }
    }
}
