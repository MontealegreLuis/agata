<?php
return [
   'encoding' => 'UTF-8',
   'doctype' => 'HTML5',
   'contentType' => 'text/html; charset=utf-8',
   'helperPath' => [
       'ZendX_JQuery_View_Helper' => 'ZendX/JQuery/View/Helper',
       'ZendApplication\View\Helper' => 'ZendApplication/View/Helper'
   ],
   'scriptPaths' => [
        'application/views/scripts',
        'application/views/layouts',
    ],
    'viewRenderer' => [
        'viewBasePathSpec' => ':moduleDir/Resources/views',
        'viewSuffix' => 'twig'
    ],
    'twig' => [
        'cache' => 'tmp/cache/twig/',
        'auto_reload' => true,
        'strict_variables' => true,
    ],
    'bundleHelpers' => [
        'command' => 'java -Xmx128m -jar %s :sourceFile -o :filename --charset utf-8',
        'yuiCompressorPath' => 'data/yui/yuicompressor-2.4.2.jar',
        'jsBundle' => [
            'cachePath' => 'tmp/cache/js',
            'publicPath' => 'public',
            'publicPathPrefix' => 'scripts/min'
        ],
        'cssBundle' => [
            'cachePath' => 'tmp/cache/css',
            'publicPath' => 'public',
            'publicPathPrefix' => 'styles/min'
        ]
    ],
];
