<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/category.php',
    [
        'category.create' => [
            'route' => 'admin-catalog',
        ],
        'category.save' => [
            'route' => 'admin-catalog',
        ],
        'category.edit' => [
            'route' => 'admin-catalog',
        ],
        'category.update' => [
            'route' => 'admin-catalog',
        ],
        'category.show' => [
            'route' => 'admin-catalog',
        ],
        'category.delete' => [
            'route' => 'admin-catalog',
        ],
        'category.list' => [
            'route' => 'admin-catalog',
        ]
    ]
);
