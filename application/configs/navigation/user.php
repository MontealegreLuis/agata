<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/SecurityModule/Resources/config/navigation.php',
    [
        'login' => [
            'route' => 'admin-auth',
        ],
        'authenticate' => [
            'route' => 'admin-auth',
        ],
        'logout' => [
            'route' => 'admin-auth',
        ],
        'restricted.home' => [
            'module' => 'catalog',
            'controller' => 'product',
            'action' => 'list',
            'route' => 'admin-catalog',
        ]
    ]
);
