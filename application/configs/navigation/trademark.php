<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/trademark.php',
    [
    'trademark.create' => [
            'route' => 'admin-catalog',
        ],
        'trademark.save' => [
            'route' => 'admin-catalog',
        ],
        'trademark.edit' => [
            'route' => 'admin-catalog',
        ],
        'trademark.update' => [
            'route' => 'admin-catalog',
        ],
        'trademark.show' => [
            'route' => 'admin-catalog',
        ],
        'trademark.delete' => [
            'route' => 'admin-catalog',
        ],
        'trademark.list' => [
            'route' => 'admin-catalog',
        ]
    ]
);
