<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/DefaultModule/Resources/config/navigation/breadcrumbs.php', [
    'categories' => [
        'route' => 'web-landing',
        'pages' => [
            'products' => [
                'route' => 'web-category',
                'pages' => [
                   'product' => [
                        'route' => 'web-product',
                   ],
               ],
           ],
       ],
    ],
]);
