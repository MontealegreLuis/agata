<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/product.php',
    [
        'product.create' => [
            'route' => 'admin-catalog',
        ],
        'product.save' => [
            'route' => 'admin-catalog',
        ],
        'product.edit' => [
            'route' => 'admin-catalog',
        ],
        'product.update' => [
            'route' => 'admin-catalog',
        ],
        'product.show' => [
            'route' => 'admin-catalog',
        ],
        'product.delete' => [
            'route' => 'admin-catalog',
        ],
        'product.list' => [
            'route' => 'admin-catalog',
        ]
    ]
);
