<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/actions/trademark.php', [
    'list' => [
        'route' => 'admin-catalog',
    ],
    'create' => [
        'route' => 'admin-catalog',
    ],
    'delete' => [
        'route' => 'admin-catalog',
    ],
    'edit' => [
        'route' => 'admin-catalog',
    ],
    'show' => [
        'route' => 'admin-catalog',
    ],
]);
