<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/actions/offer.php', [
    'list' => [
        'route' => 'admin-catalog',
    ],
    'create' => [
        'route' => 'admin-catalog',
    ],
    'delete' => [
        'route' => 'admin-catalog',
    ],
    'edit' => [
        'route' => 'admin-catalog',
    ],
    'products' => [
        'route' => 'admin-catalog',
    ],
]);
