<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/DefaultModule/Resources/config/navigation/menu.php', [
    'products' => [
        'route' => 'web-category',
     ],
    'contact' => [
        'route' => 'web-default',
    ],
    'about' => [
        'route' => 'web-default',
    ],
]);
