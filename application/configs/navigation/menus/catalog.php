<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/menu.php', [
    'products' => [
        'route' => 'admin-catalog'
    ],
    'offers' => [
        'route' => 'admin-catalog'
    ],
    'category' => [
        'route' => 'admin-catalog',
    ],
    'trademark' => [
        'route' => 'admin-catalog',
    ],
]) + [
    'logout' => [
        'label' => 'Salir',
        'module' => 'security',
        'controller' => 'user',
        'action' => 'logout',
        'route' => 'admin-auth',
    ],
];
