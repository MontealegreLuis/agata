<?php
use \Zend\Stdlib\ArrayUtils;

return ArrayUtils::merge(
    require 'src/CatalogModule/Resources/config/navigation/offer.php',
    [
        'offer.create' => [
            'route' => 'admin-catalog',
        ],
        'offer.save' => [
            'route' => 'admin-catalog',
        ],
        'offer.edit' => [
            'route' => 'admin-catalog',
        ],
        'offer.update' => [
            'route' => 'admin-catalog',
        ],
        'offer.products' => [
            'route' => 'admin-catalog',
        ],
        'offer.apply' => [
            'route' => 'admin-catalog',
        ],
        'offer.delete' => [
            'route' => 'admin-catalog',
        ],
        'offer.list' => [
            'route' => 'admin-catalog',
        ]
    ]
);
