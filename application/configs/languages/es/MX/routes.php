<?php
return array_merge(
    require 'src/CatalogModule/Resources/config/translations/es-mx/routes.php',
    require 'src/DefaultModule/Resources/config/translations/es-mx/routes.php',
    require 'src/SecurityModule/Resources/config/translations/es-mx/routes.php'
);
