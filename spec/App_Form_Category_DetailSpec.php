<?php
namespace spec;

use \PhpSpec\ObjectBehavior;
use \Prophecy\Argument;
use \Mandragora_Form_Element_TreeMulticheckbox as TreeMulticheckbox;
use \Zend_Validate_InArray as InArrayValidator;

class App_Form_Category_DetailSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('App_Form_Category_Detail');
    }

    function it_can_set_category_tree_values(TreeMulticheckbox $treeChecbox)
    {
        $categoryTree = [
            1 => [
                'label' => 'Básculas',
                'children' => [
                    2 => [
                        'label' => 'Básculas de baño',
                        'children' => []
                    ]
                ]
            ]
        ];

        $treeChecbox->addPrefixPaths(Argument::any())->shouldBeCalled();
        $treeChecbox->getOrder()->shouldBeCalled();
        $this->addElement($treeChecbox, 'parentCategory');

        $treeChecbox->setMultiOptions($categoryTree)->shouldBeCalled();
        $this->setCategoryTree($categoryTree);
    }

    function it_can_set_inarray_validator_for_tree_category_values(
        TreeMulticheckbox $treeChecbox, InArrayValidator $validator
    )
    {
        $categoryTree = [
            1 => [
                'label' => 'Básculas',
                'children' => [
                    2 => [
                        'label' => 'Básculas camioneras',
                        'children' => []
                    ]
                ]
            ]
        ];
        $haystack = [1, 2];

        $treeChecbox->addPrefixPaths(Argument::any())->shouldBeCalled();
        $treeChecbox->getOrder()->shouldBeCalled();
        $this->addElement($treeChecbox, 'parentCategory');

        $validator->setHaystack($haystack)->shouldBeCalled();
        $treeChecbox->getValidator('InArray')->willReturn($validator);
        $this->setInArrayValidator($categoryTree);
    }

}
