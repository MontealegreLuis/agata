<?php
namespace spec;

use \PhpSpec\ObjectBehavior;
use \Prophecy\Argument;
use \Mandragora_Application_Doctrine_Manager as DoctrineManager;
use \App_Model_Category as Category;
use \App_Model_Gateway_Cache_Category as CategoryGateway;
use \App_Form_Category_Detail as CategoryForm;

class App_Service_CategorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith('App_Model_Service');
        $this->shouldHaveType('App_Service_Category');
    }

    function it_can_get_form_for_creating_a_new_category(
        DoctrineManager $manager, CategoryGateway $gateway, CategoryForm $form
    )
    {
        $action = '/categoria/guardar';
        $categoryId = 1;
        $parentCategories = [
	       ['id' => $categoryId, 'name' => 'Básculas', 'url' => 'basculas', 'image' => 'basculas.jpg']
        ];
        $categoryTree = [
            $categoryId => [
                'label' => 'Básculas',
                'children' => [],
            ],
        ];

        $manager->isConnectionOpen()->willReturn(true);

        $form->setAction($action)->shouldBeCalled();
        $form->prepareForCreating()->shouldBeCalled();
        $form->setCategoryTree($categoryTree)->shouldBeCalled();
        $form->setInArrayValidator($categoryTree)->shouldBeCalled();

        $gateway->findAllParentCategories()->willReturn($parentCategories);
        $gateway->findChildrenByCategoryId($categoryId)->willReturn([]);

        $this->beConstructedWith('App_Model_Category');
        $this->setDoctrineManager($manager);
        $this->setGateway($gateway);
        $this->setForm($form);

        $this->getFormForCreating($action);
    }

    function it_can_save_a_new_category(
        DoctrineManager $manager, Category $category, CategoryGateway $gateway, CategoryForm $form
    )
    {
        $categoryValues = ['id' => 2, 'name' => 'Básculas', 'parentCategory' => 1];

        $manager->isConnectionOpen()->willReturn(true);

        $category->updateUrl()->shouldBeCalled();
        $category->fromArray($categoryValues)->willReturn($category);

        $form->getValues()->willReturn($categoryValues);
        $form->hasImage()->willReturn(false);

        $gateway->insert($category)->shouldBeCalled();

        $this->beConstructedWith('App_Model_Category');
        $this->setDoctrineManager($manager);
        $this->setGateway($gateway);
        $this->setForm($form);
        $this->setModel($category);

        $this->createCategory();
    }
}
