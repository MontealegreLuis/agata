<?php
namespace spec;

use \PhpSpec\ObjectBehavior;
use \Prophecy\Argument;
use \Doctrine_Record as Record;
use \Doctrine_Table as Table;
use \Doctrine_Query as Query;

class App_Model_Gateway_CategorySpec extends ObjectBehavior
{
    function it_is_initializable(Record $record)
    {
        $this->beConstructedWith($record);
        $this->beAnInstanceOf('App_Model_Gateway_Category');
    }

    function it_can_find_all_parent_categories(Record $record, Table $table, Query $query)
    {
        $query->from('App_Model_Dao_Category c')->shouldBeCalled();
        $query->where('c.parentCategory IS NULL')->shouldBeCalled();
        $query->fetchArray()->shouldBeCalled();

        $table->createQuery()->willReturn($query);
        $record->getTable()->willReturn($table);

        $this->beConstructedWith($record);

        $this->findAllParentCategories();
    }

    function it_can_find_children_categories(Record $record, Table $table, Query $query)
    {
        $categoryId = 1;

        $query->from('App_Model_Dao_Category c')->shouldBeCalled();
        $query->where('c.parentCategory = :parentCategoryId')->shouldBeCalled();
        $query->fetchArray([':parentCategoryId' => $categoryId])->shouldBeCalled();

        $table->createQuery()->willReturn($query);
        $record->getTable()->willReturn($table);

        $this->beConstructedWith($record);

        $this->findChildrenByCategoryId($categoryId);
    }
}
