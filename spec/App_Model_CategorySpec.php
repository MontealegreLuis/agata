<?php
namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class App_Model_CategorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('App_Model_Category');
    }

    function it_can_access_parentCategory_property()
    {
        $this->parentCategory = 1;
        $this->__get('parentCategory')->shouldReturn(1);
    }
}
