<?php
return [
    "COMPOSER_VENDOR_DIR={$this->rocketeer->getFolder('shared')}/vendor composer install -o --prefer-dist --no-dev",
    "ln -s {$this->rocketeer->getFolder('shared')}/vendor {$this->releasesManager->getCurrentReleasePath()}/vendor",
    "cp {$this->rocketeer->getFolder('shared')}/local.build.properties {$this->releasesManager->getCurrentReleasePath()}/local.build.properties",
    "phing.phar app:config -verbose",
    "phing.phar app:symlinks -verbose",
    "phing.phar app:clear-cache -verbose",
    "phing.phar db:migration -verbose",
];
