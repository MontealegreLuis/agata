<?php
return [
    'scm' => 'git',
    'repository' => 'gitmandragora@git.mandragora-web-systems.com:agata.git',
    'branch'     => 'master',
    'shallow' => true,
];
