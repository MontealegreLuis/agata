<?php return array(
    'before' => array(
        'setup'   => array(),
        'deploy'  => array(),
        'cleanup' => array(),
    ),
    'after' => array(
        'setup'   => array(),
        'deploy'  => array(),
        'cleanup' => array(),
    ),
    'custom' => array(),
);
