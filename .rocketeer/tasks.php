<?php
use Agata\Deploy\Commands\CustomDeployCommand;
use Rocketeer\Facades\Console;

return Console::add(new CustomDeployCommand());
