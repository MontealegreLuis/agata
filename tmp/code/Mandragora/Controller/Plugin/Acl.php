<?php
/**
 * Plugin for authentication and authorization
 *
 * PHP version 5.3
 *
 * This source file is subject to the license that is bundled with this package in the
 * file LICENSE.
 *
 * @package    Mandragora
 * @subpackage Controller_Plugin
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
use \Zend_Registry as Registry;

/**
 * Plugin for authentication and authorization.
 *
 * It verifies if the user is logged and has permission to access the current
 * action during the predispatch phase.
 *
 * @package    Mandragora
 * @subpackage Controller_Plugin
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
class   Mandragora_Controller_Plugin_Acl
extends Mandragora_Controller_Plugin_Abstract
{
    /**
     * Verify if the user is logged and has permission to perform the current
     * action. If not redirect to the "unauthorized action"
     *
     * @param $request
     *      The current request information
     * @return void
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $sessionOptions = $this->getResource('session')->getOptions();
        $options = array(
            'cacheManager' => $this->getResource('cachemanager'),
            'doctrineManager' => $this->getResource('doctrine'),
            'namespace' => $sessionOptions['name'],
        );
        $aclHandler = Mandragora_Acl::factory('Handler', $options);
        $aclHandler->execute($request);
        if ($aclHandler->isNotAuthenticated()) {
            //redirect to login
            $router = Mandragora_Service_Router::factory('Helper');
            if ($request->isGet()) {
                $name = Zend_Auth::getInstance()->getStorage()->getNamespace();
                $session = new Zend_Session_Namespace($name);
                $session->requestUrl = $request->getRequestUri();
            }
            $url = $router->getDefaultRoute('login');
            $view = Registry::get('view');
            $this->flash('error')->addMessage('session.sessionExpired');
            $route = $url['route'];
            unset($url['route']);
            $this->_response->setRedirect($view->url($url, $route, true));
        } elseif ($aclHandler->isNotAuthorized()) {
            //redirect to unauthorized page
            $router = Mandragora_Service_Router::factory('Helper');
            $url = $router->getDefaultRoute('unauthorized');
            $view = Zend_Layout::getMvcInstance()->getView();
            $route = $url['route'];
            unset($url['route']);
            $this->_response->setRedirect($view->url($url, $route, true));
        }
    }

}
