<?php
/**
 * Cache decorator for Category's Gateway
 *
 * PHP version 5.3
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @package    App
 * @subpackage Gateway_Cache
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
use \Mandragora_Gateway_Decorator_CacheAbstract as CacheDecorator;
use \Zend_Cache as Cache;

/**
 * Cache decorator for Category's Gateway
 *
 * @package    App
 * @subpackage Gateway_Cache
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
class App_Model_Gateway_Cache_Category extends CacheDecorator
{
    /**
     * @return array
     * @throws Mandragora_Gateway_NoResultsFoundException
     */
    public function findOneById($id)
    {
        $cacheId = 'category' . (int) $id;
        $category = $this->getCache()->load($cacheId);
        if (!$category) {
            $category = $this->gateway->findOneById((int) $id);
            $this->getCache()->save($category, $cacheId);
        }

        return $category;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cacheId = 'category';
        $categories = $this->getCache()->load($cacheId);
        if (!$categories) {
            $categories = $this->gateway->findAll();
            $this->getCache()->save($categories, $cacheId);
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function findAllParentCategories()
    {
        $cacheId = 'parentCategories';
        $categories = $this->getCache()->load($cacheId);
        if (!$categories) {
            $categories = $this->gateway->findAllParentCategories();
            $this->getCache()->save($categories, $cacheId);
        }

        return $categories;
    }

    /**
     * @param  int   $categoryId
     * @return array
     */
    public function findChildrenByCategoryId($categoryId)
    {
        $cacheId = 'childrenCategories' . (int) $categoryId;
        $categories = $this->getCache()->load($cacheId);
        if (!$categories) {
            $categories = $this->gateway->findChildrenByCategoryId($categoryId);
            $this->getCache()->save($categories, $cacheId, ['childrenCategories']);
        }

        return $categories;
    }

    /**
     * @param  Mandragora_Model_Abstract $category
     * @return void
     */
    public function insert(Mandragora_Model_Abstract $category)
    {
        $this->gateway->insert($category);
        $cacheId = 'category' . $category->id;
        $this->getCache()->save($category->toArray(true), $cacheId);
        $this->getCache()->remove('category');
        $this->updateChildrenAndParentCache($category);
    }

    /**
     * @param  Mandragora_Model_Abstract $category
     * @return void
     */
    public function update(Mandragora_Model_Abstract $category)
    {
        $this->gateway->update($category);
        $cacheId = 'category' . $category->id;
        $this->getCache()->save($category->toArray(true), $cacheId);
        $this->getCache()->remove('category');
        $this->updateChildrenAndParentCache($category);
    }

    /**
     * @param  Mandragora_Model_Abstract $category
     * @return void
     */
    public function delete(Mandragora_Model_Abstract $category)
    {
        $this->gateway->delete($category);
        $cacheId = 'category' . $category->id;
        $this->getCache()->remove($cacheId);
        $this->getCache()->remove('category');
        $this->updateChildrenAndParentCache($category);
    }

    /**
     * @param App_Model_Category $category
     */
    protected function updateChildrenAndParentCache(App_Model_Category $category)
    {
        if (!$category->parentCategory) {
            $this->getCache()->remove('parentCategories');
        } else {
            $this->getCache()->clean(Cache::CLEANING_MODE_MATCHING_TAG, ['childrenCategories']);
        }
    }
}
