<?php
/**
 * Cache decorator for Trademark's Gateway
 *
 * PHP version 5
 *
 * LICENSE: Redistribution and use of this file in source and binary forms,
 * with or without modification, is not permitted under any circumstance
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package    App
 * @subpackage Gateway_Cache
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011
 * @version    SVN: $Id$
 */

/**
 * Cache decorator for Trademark's Gateway
 *
 * @package    App
 * @subpackage Gateway_Cache
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011
 * @version    SVN: $Id$
 */
class   App_Model_Gateway_Cache_Trademark
extends Mandragora_Gateway_Decorator_CacheAbstract
{
    /**
     * @return array
     * @throws Mandragora_Gateway_NoResultsFoundException
     */
    public function findOneById($id)
    {
        $cacheId = 'trademark' . (int) $id;
        $trademark = $this->getCache()->load($cacheId);
        if (!$trademark) {
            $trademark = $this->gateway->findOneById((int) $id);
            $this->getCache()->save($trademark, $cacheId);
        }

        return $trademark;
    }

    /**
     * @param  Mandragora_Model_Abstract $trademark
     * @return void
     */
    public function insert(Mandragora_Model_Abstract $trademark)
    {
        $this->gateway->insert($trademark);
        $cacheId = 'trademark' . $trademark->id;
        $this->getCache()->save($trademark->toArray(true), $cacheId);
        $this->getCache()->remove('trademark');
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $cacheId = 'trademark';
        $trademarks = $this->getCache()->load($cacheId);
        if (!$trademarks) {
            $trademarks = $this->gateway->findAll();
            $this->getCache()->save($trademarks, $cacheId);
        }

        return $trademarks;
    }

    /**
     * @param  Mandragora_Model_Abstract $trademark
     * @return void
     */
    public function update(Mandragora_Model_Abstract $trademark)
    {
        $this->gateway->update($trademark);
        $cacheId = 'trademark' . $trademark->id;
        $this->getCache()->save($trademark->toArray(true), $cacheId);
        $this->getCache()->remove('trademark');
    }

    /**
     * @param  Mandragora_Model_Abstract $trademark
     * @return void
     */
    public function delete(Mandragora_Model_Abstract $trademark)
    {
        $this->gateway->delete($trademark);
        $cacheId = 'trademark' . $trademark->id;
        $this->getCache()->remove($cacheId);
        $this->getCache()->remove('trademark');
    }

}
