<?php
/**
 * Cache decorator for User's Gateway
 *
 * PHP version 5
 *
 * LICENSE: Redistribution and use of this file in source and binary forms,
 * with or without modification, is not permitted under any circumstance
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @package    App
 * @subpackage Gateway_Cache
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011
 * @version    SVN: $Id$
 */

/**
 * Cache decorator for User's Gateway
 *
 * @package    App
 * @subpackage Gateway_Cache
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011
 * @version    SVN: $Id$
 */
class   App_Model_Gateway_Cache_User
extends Mandragora_Gateway_Decorator_CacheAbstract
{
    /**
     * @return array
     * @throws Mandragora_Gateway_NoResultsFoundException
     */
    public function findOneById($id)
    {
        $cacheId = 'user' . (int) $id;
        $user = $this->getCache()->load($cacheId);
        if (!$user) {
            $user = $this->gateway->findOneById((int) $id);
            $this->getCache()->save($user, $cacheId);
        }

        return $user;
    }

    /**
     * @return array
     * @throws Mandragora_Doctrine_Gateway_NoResultsFoundException
     */
    public function findOneByUsername($username)
    {
        $cacheId = 'user' . md5(serialize($username));
        $user = $this->getCache()->load($cacheId);
        if (!$user) {
            $user = $this->gateway->findOneByUsername((string) $username);
            $this->getCache()->save($user, $cacheId);
        }

        return $user;
    }

    /**
     * @param  Mandragora_Model_Abstract $user
     * @return void
     */
    public function insert(Mandragora_Model_Abstract $user)
    {
        $this->gateway->insert($user);
        $cacheId = 'user' . $user->id;
        $this->getCache()->save($user->toArray(true), $cacheId);
    }

    /**
     * @param  Mandragora_Model_Abstract $user
     * @return void
     */
    public function update(Mandragora_Model_Abstract $user)
    {
        $this->gateway->update($user);
        $cacheId = 'user' . $user->id;
        $this->getCache()->save($user->toArray(true), $cacheId);
    }

    /**
     * @param  Mandragora_Model_Abstract $user
     * @return void
     */
    public function delete(Mandragora_Model_Abstract $user)
    {
        $this->gateway->delete($user);
        $cacheId = 'user' . $user->id;
        $this->getCache()->remove($cacheId);
    }

}
