<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('App_Model_Dao_Resource', 'doctrine');

/**
 * App_Model_Dao_Resource
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property string $name
 * @property integer $version
 * @property Doctrine_Collection $Permission
 *
 * @package    App
 * @subpackage Dao
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class App_Model_Dao_Resource extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('resource');
        $this->hasColumn('name', 'string', 20, array(
             'type' => 'string',
             'length' => 25,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
             ));
        $this->hasColumn('version', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8,
             'fixed' => false,
             'unsigned' => true,
             'primary' => false,
             'default' => '1',
             'notnull' => true,
             'autoincrement' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('App_Model_Dao_Permission as Permission', array(
             'local' => 'name',
             'foreign' => 'resourceName'));
    }
}
