<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('App_Model_Dao_Permission', 'doctrine');

/**
 * App_Model_Dao_Permission
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @property string $name
 * @property string $roleName
 * @property string $resourceName
 * @property integer $version
 * @property App_Model_Role $Role
 * @property App_Model_Resource $Resource
 *
 * @package    App
 * @subpackage Dao
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class App_Model_Dao_Permission extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('permission');
        $this->hasColumn('name', 'string', 45, array(
             'type' => 'string',
             'length' => 45,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => false,
             ));
        $this->hasColumn('roleName', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'notnull' => true,
             'autoincrement' => false,
             ));
        $this->hasColumn('resourceName', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             'notnull' => true,
             'autoincrement' => false,
             ));
        $this->hasColumn('version', 'integer', 8, array(
             'type' => 'integer',
             'length' => 8,
             'fixed' => false,
             'unsigned' => true,
             'primary' => false,
             'default' => '1',
             'notnull' => true,
             'autoincrement' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('App_Model_Dao_Role as Role', array(
             'local' => 'roleName',
             'foreign' => 'name'));

        $this->hasOne('App_Model_Dao_Resource as Resource', array(
             'local' => 'resourceName',
             'foreign' => 'name'));
    }
}
