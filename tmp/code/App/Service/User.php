<?php
/**
 * Service class for User's model
 *
 * PHP version 5
 *
 * LICENSE: Redistribution and use of this file in source and binary forms,
 * with or without modification, is not permitted under any circumstance
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   Application
 * @package    App
 * @subpackage Service
 * @author     MMA <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010
 * @version    SVN: $Id$
 */

/**
 * Service class for User's model
 *
 * @category   Application
 * @package    App
 * @subpackage Service
 * @author     LMV <misraim.mendoza@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010
 * @version    SVN: $Id$
 */
class   App_Service_User
extends Mandragora_Service_Crud_Doctrine_Abstract
{
    /**
     * @return void
     */
    protected function init()
    {
        $this->openConnection();
        $this->decorateGateway();
    }

    /**
     * @param  string           $namespace
     * @return Zend_Auth_Result
     */
    public function login($namespace)
    {
        $this->init();
        $this->getModel($this->getForm()->getValues());
        $adapter = new Mandragora_Auth_Adapter(
            $this->getModel(), $this->getGateway()
        );
        $authenticator = Zend_Auth::getInstance();
        $authenticator->setStorage(new Zend_Auth_Storage_Session($namespace));

        return $authenticator->authenticate($adapter);
    }

    /**
     * @param  array $errors
     * @return void
     */
    public function setLoginFormAuthenticationErrors(array $errors)
    {
        foreach ($errors as $name => $message) {
            $this->getLoginForm()
                 ->getElement($name)
                 ->setErrors(array($message));
        }
    }

    /**
     * Clears the user's identity
     *
     * @return void
     */
    public function logout()
    {
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
    }

    /**
     * @return boolean
     */
    public function isUserLogged()
    {
        return Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * @param  string                                              $userName
     * @throws Mandragora_Doctrine_Gateway_NoResultsFoundException
     */
    public function retrieveUserByUsername($userName)
    {
        try {
            $userValues = $this->getGateway()->findOneByUsername($userName);

            return $this->getModel($userValues);
        } catch (Mandragora_Gateway_NoResultsFoundException $nrfe) {
            return false;
        }
    }


    /**
     * @param  string                 $action
     * @return Edeco_Form_User_Detail
     */
    public function getFormForCreating($action)
    {
        $this->getForm('Detail')->setAction($action);
        $this->getForm()->prepareForCreating();

        return $this->getForm();
    }

    /**
     * @param  string                 $action
     * @return Edeco_Form_User_Detail
     */
    public function getFormForEditing($action)
    {
        $this->getForm('Detail')->setAction($action);
        $this->getForm()->prepareForEditing();
        $this->getForm()->setState(Edeco_Enum_UserState::values());

        return $this->getForm();
    }

}
