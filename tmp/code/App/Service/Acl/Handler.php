<?php
class App_Service_Acl_Handler implements Mandragora_Service_Acl_Interface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var boolean
     */
    protected $notAuthenticated;

    /**
     * @var boolean
     */
    protected $notAuthorized;

    /**
     * @var Zend_Acl
     */
    protected $acl;

    /**
     * @param  array $options
     * @return void
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * @param  Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function execute(Zend_Controller_Request_Abstract $request)
    {
        $this->createAcl();
        $action = $request->getActionName();
        $resource = sprintf(
            '%s_%s',  $request->getModuleName(), $request->getControllerName()
        );
        $auth = Zend_Auth::getInstance();
        $storage = new Zend_Auth_Storage_Session($this->options['namespace']);
        $auth->setStorage($storage);
        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
            $role = $identity->roleName;
            if ($this->acl->has($resource)
                && !$this->acl->isAllowed($role, $resource, $action)) {
                $this->notAuthorized = true;
            }
        } else {
            if ($this->acl->has($resource)
                && !$this->acl->isAllowed('guest', $resource, $action)) {
                $this->notAuthenticated = true;
            }
        }
    }

    /**
     * @return void
     */
    public function createAcl()
    {
        $cacheManager = $this->options['cacheManager'];
        $doctrineManager = $this->options['doctrineManager'];
        $cache = $cacheManager->getCache('default');
        $acl = $cache->load('acl');
        if (!$acl) {
            $acl = Mandragora_Acl_Wrapper::getInstance();
            $rolesService = Mandragora_Service::factory('Role');
            $rolesService->setCacheManager($cacheManager);
            $rolesService->setDoctrineManager($doctrineManager);
            $permissionsService = Mandragora_Service::factory('Permission');
            $permissionsService->setCacheManager($cacheManager);
            $permissionsService->setDoctrineManager($doctrineManager);
            $resourceService = Mandragora_Service::factory('Resource');
            $resourceService->setCacheManager($cacheManager);
            $resourceService->setDoctrineManager($doctrineManager);
            $acl->setRoles($rolesService->retrieveAllRoles());
            $acl->setResources($resourceService->retrieveAllResources());
            $acl->setPermissions($permissionsService->retrieveAllPermissions());
            $cache->save($acl, 'acl');
        }
        Mandragora_Acl_Wrapper::setInstance($acl); //Recover acl from cache
        $this->acl = Mandragora_Acl_Wrapper::getInstance();
    }

    /**
     * @return boolean
     */
    public function isNotAuthenticated()
    {
        return $this->notAuthenticated;
    }

    /**
     * @return boolean
     */
    public function isNotAuthorized()
    {
        return $this->notAuthorized;
    }

}
