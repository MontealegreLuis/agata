<?php
/**
 * Bootstrap file for Doctrine's CLI application
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the
 * file LICENSE.
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */

/**
 * Bootstrap file for Doctrine's CLI application
 *
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 */
require 'vendor/autoload.php';

use \Zend\ServiceManager\ServiceManager;
use \Doctrine1\Doctrine1Provider;
use \Doctrine_Cli as Cli;
use \Doctrine_Core as Doctrine;
use \Agata\ZendApplication\ServiceManager\ApplicationProvider;

define('APPLICATION_PATH', realpath('application'));
define('APPLICATION_ENV', 'doctrineCLI');

$serviceManager = new ServiceManager();
$config = new ApplicationProvider();
$config->configureServiceManager($serviceManager);
$doctrine = new Doctrine1Provider();
$doctrine->configureServiceManager($serviceManager);

$connection = $serviceManager->get('connection');

$cli = new Cli(require APPLICATION_PATH . '/configs/services/doctrine.php');
$cli->registerTaskClass('Doctrine1\Task\LoadSqlFile');
$cli->registerTaskClass('Doctrine1\Task\CreateTablesFromModels');
$cli->run($_SERVER['argv']);
