/**
 * Class to relate products with the given offer
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 * @package    Agata
 * @subpackage Pages
 */
var Agata = {};
Agata.Pages = {};
Agata.Pages.Offer = {};
Agata.Pages.Offer.OfferProducts = (function() {

    var filterProducts = function() {

        var categoryId = $('#categoryId').val();
        var trademarkId = $('#trademarkId').val();
        var $spiner = $(this);
        var queryString = {};

        if (categoryId) {
            queryString.categoryId = categoryId;
        }
        if (trademarkId) {
            queryString.trademarkId = trademarkId;
        }

        $spiner.ajaxLoader().ajaxError();
        $.ajax({
            url: '/oferta/filtrar-productos',
            data: queryString,
            dataType: 'html',
            success: function(productsTable) {
                $('.multicheckbox').html(productsTable);
            },
            beforeSend: function(jqXHR, settings) {
                $spiner.ajaxLoader('show');
            },
            complete: function(jqXHR, settings) {
                $spiner.ajaxLoader('hide');
            }
        });
    };

    var checkAllProducts = function() {
        var $checkAll = $('#allProducts');
        $checkAll.on('click', function() {
            $('table :checkbox').prop('checked', this.checked)
        });

        $('.multicheckbox').on('click', 'table :checkbox', function() {
            if (!this.checked) {
                $checkAll.prop('checked', this.checked)
            }
        });
    };

    /**
     * @return void
     */
    var _constructor = function() {
        var baseUrl = '/oferta/productos';

        /**
         * @return void
         */
        this.init = function() {
            $('#categoryId').change(filterProducts);
            $('#trademarkId').change(filterProducts);
            checkAllProducts();
        };
    };

    return _constructor;
})();
