/**
 * Class for pages add offer and update offer
 *
 * @author     LMV <luis.montealegre@mandragora-web.systems>
 * @copyright  Mandrágora Web-Based Systems 2014
 * @package    Agata
 * @subpackage Pages
 */
var Agata = {};
Agata.Pages = {};
Agata.Pages.Offer = {};
Agata.Pages.Offer.Detail = (function() {
    /**
     * @return void
     */
    var wrap =  function(message) {
        return $.format('<ul class="error-list"><li class="error-message">{0}</li></ul>', message);
    };

    var amountPercentageRules = {
        required: true,
        min: 0,
        max: 100,
        messages: {
            required: wrap('Ingrese una cantidad'),
            min: wrap('No puede crear un descuento menor a 0%'),
            max: wrap('No puede crear un descuento mayor al 100%')
        }
    };
    var amountRules = {
        required: true,
        messages: {
            required: wrap('Ingrese una cantidad')
        }
    };

    var formatAmount = function() {
        var $select = $(this);
        var $symbol = $('#symbol');
        var $amount = $('#amount');
        var $wrapper = $('#wrapper');

        if ('' === $select.val()) {
            return;
        }

        $symbol.remove();
        $amount.rules('remove');
        if ($wrapper.length === 0) {
            $amount.wrap('<div id="wrapper" class="input-group"></div>');
        }
        if ('0' === $select.val()) {
            $amount.after('<span class="input-group-addon" id="symbol">%</span>');
            $amount.rules('add', amountPercentageRules);
        } else {
            $amount.before('<span class="input-group-addon" id="symbol">$</span>');
            $amount.rules('add', amountRules);
        }
        $amount.prop('disabled', false);
    };

    /**
     * @return void
     */
    var setupCalendars = function() {
        $('.date').datepicker({
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            maxDate: '+1y',
            monthNames: [
                'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
                'Diciembre'
            ],
            dateFormat: 'yy-mm-dd'
        });
    };

    /**
     * @return void
     */
    var initValidator = function() {
        $('#offer').validate({
            submitHandler: function(form) {
                var $type = $('#type');
                if ($type.is(':disabled')) {
                    $type.removeAttr('disabled');
                }
                form.submit();
            },
            errorElement : 'div',
            rules: {
                startDate: {
                    required: true,
                    date: true
                },
                stopDate: {
                    required: true,
                    date: true
                },
                type : {
                    required: true
                },
                amount : {
                    required: true
                },
                description : {
                    required: true
                },
                image: {
                    required: false,
                    accept: '.jpg'
                }
            },
            messages: {
                startDate: {
                    required: wrap('Por favor ingrese una fecha de inicio para la oferta'),
                    date: wrap('Ingrese una fecha de inicio válida'),
                },
                stopDate: {
                    required: wrap('Por favor ingrese una fecha final para la oferta'),
                    date: wrap('Ingrese una fecha final válida'),
                },
                type: {
                    required: wrap('Seleccione un tipo de oferta')
                },
                amount: {
                    required: wrap('Ingrese una cantidad')
                },
                description: {
                    required: wrap('Ingrese una descripción para la oferta')
                },
                image: {
                    accept: wrap('El archivo seleccionado no es una imagen .jpg')
                }
            }
        });
        //$('#image').file();
    };

    /**
     * @return void
     */
    var _constructor = function() {

        /**
         * @return void
         */
        this.init = function(cssUrl, imagesUrl) {
            var $amount = $('#amount');
            $amount.autoNumeric('init');

            setupCalendars();
            initValidator();
            $amount.prop('disabled', true);

            if ($('.delete').length > 0) {
                var optionsConfirm = {
                    title: 'Confirmar eliminación',
                    message: '¿Desea eliminar esta oferta?',
                    ok: 'Sí',
                    cancel: 'No'
                };
                $('.delete').confirm(optionsConfirm);
            }

            var $type = $('#type');
            $type.change(formatAmount);
            if ($.trim($type.val()) !== '') {
                $type.trigger('change');
            }

            $('#description').focus();
        };
    };

    return _constructor;
})();
