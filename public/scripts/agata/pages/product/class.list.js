/**
 * Class for the list of product page
 *
 * @author     MMA <misraim.mendoza@mandragora-web.systems>
 * @copyright  Mandrágora Web-Based Systems 2010-2014
 * @package    Agata
 * @subpackage Pages
 */
var Agata = {};
Agata.Pages = {};
Agata.Pages.Product = {};
Agata.Pages.Product.List = (function() {
    
    /**
     * @return void
     */
    var _constructor = function(){
        
        /**
         * @return void
         */ 
        this.init = function() {
            var optionsConfirm = {
                title: 'Confirmar eliminación',
                message: '¿Desea eliminar este producto?',
                ok: 'Sí',
                cancel: 'No'
            };
            $('.delete').confirm(optionsConfirm);
        };
    };
    
    return _constructor;
})();