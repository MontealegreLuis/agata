/**
 * Class for pages add product and update product
 
 * @author     MMA <misraim.mendoza@mandragora-web.systems>
 * @copyright  Mandrágora Web-Based Systems
 * @version    SVN: $Id$
 * @package    Agata
 * @subpackage Pages
 */
var Agata = {};
Agata.Pages = {};
Agata.Pages.Product = {};
Agata.Pages.Product.Detail = (function() {

    /**
     * @return void
     */
    var wrap =  function(message) {
        return $.format(
            '<ul class="error-list"><li class="error-message">{0}</li></ul>', 
            message
        );
    };
    
    /**
     * @return void
     */
    var initValidator = function() {
    	var isImageFileRequired = $('#image').length == 0;
        $('#product').validate({
            submitHandler: function(form) {
                $('.rte-zone').each(function(){
                    if ($(this).is('iframe')) {
                        var id = $(this).attr('id');
                        var textAreaId = 'textarea[id="' + id + '"]';
                        var html = $(this).contents().find('body').html(); 
                        var iframeContent = $('<div>' + html + '</div>');
                        $.rteTidy.tidy(iframeContent);
                        $(textAreaId).val(iframeContent.html());
                    }
                });
                form.submit();
            },
            errorElement : 'div',
            rules: {
                model: {
                    required: true,
                    minlength: 2,
                    maxlength: 45
                },
                features: {
                    rterequired: true,
                    rteminlength: 15,
                    rtemaxlength: 1500
                },
                price: {
                    required: true
                },
                categoryId : {
                    required: true
                },
                trademarkId : {
                    required: true
                },
                image: {
                	required: isImageFileRequired,
                	accept: '.jpg'
                }
            },
            messages: {
                model: {
                    required: wrap(
                        'Por favor ingrese el modelo del producto'
                    ),
                    minlength: $.format(
                        wrap(
                     'El modelo del producto debe tener al menos {0} caracteres'
                        )
                    ),
                    maxlength: $.format(
                        wrap(
                       'El modelo del producto debe tener máximo {0} caracteres'
                        )
                    )
                },
                features: {
                    rterequired: wrap('Por favor ingrese las caracteristicas'),
                    rteminlength: $.format(
                        wrap(
                            'Las caracteristicas deben tener al menos {0} caracteres'
                        )
                    ),
                    rtemaxlength: $.format(
                        wrap('Las caracteristicas deben tener máximo {0} caracteres')
                    )
                },
                price: {
                    required: wrap('Por favor ingrese el precio del producto')
                },
                categoryId: {
                    required: wrap(
                        'Por favor selecciona una categoria'
                    )
                },
                trademarkId: {
                    required: wrap(
                        'Por favor selecciona una marca'
                    )
                },
                image: {
                    required: wrap(
                        'No seleccionó ninguna imagen para el producto'
                    ),
                    accept: $.format(
                    		wrap(
                            'El archivo seleccionado no es una imagen .jpg'
                        )
                    )
                }
            }
        });
        //$('#image').file();
    };

    /**
     * @return void
     */
    var _constructor = function(){

        /**
         * @return void
         */
        this.init = function(cssUrl, imagesUrl) {
            $('#price').autoNumeric('init');
            $('.rte-zone').rte({cssUrl: cssUrl, mediaUrl: imagesUrl});

            /*
             * Move the textarea elements after the iframe, so that error
             * messages display correctly
             */
            $('textarea.rte-zone').each(function() {
                var dd = $(this).parent();
                var textArea = $(this).remove();
                textArea.appendTo(dd);
            });
            initValidator();
            if ($('.delete').length > 0) {
                var optionsConfirm = {
                    title: 'Confirmar eliminación',
                    message: '¿Desea eliminar este producto?',
                    ok: 'Sí',
                    cancel: 'No'
                };
                $('.delete').confirm(optionsConfirm);
            }
            $('#model').focus();
        };
    };
   

    return _constructor;
})();