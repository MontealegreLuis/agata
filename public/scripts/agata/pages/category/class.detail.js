/**
 * Class for pages add category and update category
 *
 * @author     MMA <misraim.mendoza@mandragora-web.systems>
 * @copyright  Mandrágora Web-Based Systems
 * @version    SVN: $Id$
 * @package    Agata
 * @subpackage Pages
 */
var Agata = {};
Agata.Pages = {};
Agata.Pages.Category = {};
Agata.Pages.Category.Detail = (function() {
	
    /**
     * @return string
     */
	var wrap =  function(message) {
        return $.format('<ul class="error-list"><li class="error-message">{0}</li></ul>', message);
    };
	
	/**
	 * @return void
	 */
	var _initValidator = function() {
	    var isImageFileRequired = $('#image').length == 0;
        var validator = $("#category").validate({
            errorElement : 'div',
            rules: {
            	name: {
                    required: true,
                    minlength: 1,
                    maxlength: 45
                },
                image: {
                	required: isImageFileRequired,
                	accept: '.jpg'
                }
            },
            messages: {
            	name: {
                    required: wrap('Por favor ingrese el nombre de la categoría'),
                    minlength: $.format(
                        wrap('El nombre de la categoría debe tener al menos {0} caracteres')
                    ),
                    maxlength: $.format(
                        wrap('El nombre de la categoría debe tener máximo {0} caracteres')
                    )
                },
                image: {
                    required: wrap('No seleccionó ninguna imagen para la categoría'),
                    accept: wrap('El archivo seleccionado no es una imagen .jpg')
                }
            }
        });
        //$('#image').file();
        $('#name').focus();
	};
	
	/**
	 * @retun void
	 */
	var _constructor = function(){
		
	    /**
	     * @return void
	     */
		this.init = function() {
		    _initValidator();

		    $('#parentCategory').treeview({collapsed: false});

		    var $checkboxes = $('input[type="checkbox"]');
		    $checkboxes.click(function() {
		        $checkboxes.filter(':checked').not(this).removeAttr('checked');
		    });

		    if ($('.delete').length > 0) {
                var optionsConfirm = {
                    title: 'Confirmar eliminación',
                    message: '¿Desea eliminar esta categoría?',
                    ok: 'Sí',
                    cancel: 'No'
                };
                $('.delete').confirm(optionsConfirm);
            }
		};
	};
	
	return _constructor;
})();