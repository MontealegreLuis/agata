/**
 * Class for pages add trademark and update trademark
 *
 * @author     MMA <misraim.mendoza@mandragora-web.systems>
 * @copyright  Mandrágora Web-Based Systems
 * @version    SVN: $Id$
 * @package    Agata
 * @subpackage Pages
 */
var Agata = {};
Agata.Pages = {};
Agata.Pages.Trademark = {};
Agata.Pages.Trademark.Detail = (function() {
	
    /**
     * @return void
     */
	var wrap =  function(message) {
        return $.format(
            '<ul class="error-list"><li class="error-message">{0}</li></ul>', 
            message
        );
    };
	
	/**
	 * @return void
	 */
	var _initValidator = function() {
	    var isImageFileRequired = $('#image').length == 0;
	    var validateOptions = {
            errorElement : 'div',
            rules: {
                name: {
                    required: true,
                    minlength: 1,
                    maxlength: 45
                }
            },
            messages: {
                name: {
                    required: wrap('Por favor ingrese el nombre de la marca'),
                    minlength: $.format(wrap('El nombre de la marca debe tener al menos {0} caracteres')),
                    maxlength: $.format(wrap('El nombre de la marca debe tener máximo {0} caracteres'))
                }
            }
        };
        var validator = $("#trademark").validate(validateOptions);
        $('#name').focus();
	};
	
	/**
	 * @retun void
	 */
	var _constructor = function(){
		
	    /**
	     * @return void
	     */
		this.init = function() {
		    _initValidator();
		    if ($('.delete').length > 0) {
                var optionsConfirm = {
                    title: 'Confirmar eliminación',
                    message: '¿Desea eliminar esta marca?',
                    ok: 'Sí',
                    cancel: 'No'
                };
                $('.delete').confirm(optionsConfirm);
            }
		};
	};
	
	return _constructor;
})();