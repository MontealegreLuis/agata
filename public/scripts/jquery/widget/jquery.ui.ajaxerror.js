/**
 * Set ajax error default function
 *
 * @package    mandragora
 * @author     LMV <luis.montealegre@mandragora-web-systems.com>
 * @copyright  Mandrágora Web-Based Systems 2012-2013
 */
(function($) {
    $.widget('mandragora.ajaxError', {
        options: {
            statusCodes: {
                '401' : 'Unauthenticated access.',
                '403' : 'Forbidden resource cannot be accessed',
                '404' : 'Page not found',
                '405' : 'Invalid HTTP method',
                '500' : 'Internal Server Error',
                '503' : 'Service Unavailable'
            },
            exceptions: {
                'parsererror' : 'Error parsing JSON. Request failed',
                'timeout' : 'Request Time out',
                'abort' : 'Request was aborted by the server',
                'unknown' : 'Unknown Error'
            },
            environment: 'production'
        },
        _create: function() {
            var $widget = this;
            var message = '';
            $.ajaxSetup({
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status) {
                        message = $widget.options.statusCodes[jqXHR.status];
                        if (!message){
                            message = $widget.options.exceptions[textStatus];
                            if (!message) {
                                message = $widget.options.exceptions['unknown'];
                            }
                        }
                    }
                    if ('production' !== $widget.options.environment) {
                        message += '<br /> ' + jqXHR.responseText;
                    }
                    $widget.element.alert({message: message});
                }
            });
        },
    });
})(jQuery);