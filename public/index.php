<?php
/**
 * Bootstrap file
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
use \ZendApplication\ErrorHandler\ErrorToExceptionHandler;
use \ZendApplication\Application\Application;
use \Agata\ZendApplication\ServiceManager\AgataKernel;

/**
 * Bootstrap file
 *
 * @author     MMA <misraim.mendoza@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
chdir('../');
require 'vendor/autoload.php';
$errorHandler = new ErrorToExceptionHandler();
$errorHandler->register();

define('PUBLIC_PATH', __DIR__);
define('APPLICATION_PATH', PUBLIC_PATH . '/../application');
define('APPLICATION_ENV', getenv('APPLICATION_ENV'));

$application = new Application(APPLICATION_ENV);
$application->setOptions(require 'application/configs/application.php')
            ->setKernel(new AgataKernel())
            ->bootstrap()->run();
