INSERT INTO `category` (`id`, `name`, `url`, `image`, `version`) VALUES
(1, 'Básculas', 'basculas', 'basculas.jpg', 1);

INSERT INTO `trademark` (`id`, `name`, `url`, `version`) VALUES
(1, 'TORREY', 'torrey', 1),
(2, 'TECNO COR', 'tecno-cor', 1);

INSERT INTO `product` (`id`, `model`, `url`, `features`, `image`, `trademarkId`, `categoryId`, `version`) VALUES
(1, 'Báscula Porcionadora PZC-10', 'bascula-porcionadora-pzc10', '<ul><li>Control de recetas !gramo x gramo¡.\r\n</li><li>Prepare platillos iguales siempre.\r\n</li><li>No mas estimaciones, pese en kg, Lb y Oz.\r\n</li><li>Capacidade de 5kg y 10 kg.\r\n</li><li>Acero Inoxidable grado alimenticio.</li></ul>', 'bascula-porcionadora-pzc10.jpg', 1, 1, 8),
(2, 'Báscula Porcionadora L-EQ', 'bascula-porcionadora-leq', '<ul><li>Fácil limpieza, Acero inoxidable.\r\n</li><li>Control de porciones Tara.\r\n</li><li>Portátil con batería recargable.\r\n</li><li>No mas conversiones, pesa en kg, lb y oz.\r\n</li><li>Capacidades de 5kg y 10kg.\r\n</li><li>Pantalla iluminada y dígitos grandes.</li></ul>', 'bascula-porcionadora-leq.jpg', 1, 1, 4),
(3, 'Báscula contra agua EQB-W', 'bascula-contra-agua-eqbw', '<ul><li>Soporta el hielo y la humedad.\r\n</li><li>Limpieza rápida y fácil.\r\n</li><li>Acero Inoxidable grado alimenticio.\r\n</li><li>Capacidades de 20,50kg y 100kg.\r\n</li><li>Indicador herméticamente.\r\n</li><li>Sellado para lavar a chorro.</li></ul>', 'bascula-contra-agua-eqbw.jpg', 1, 1, 30),
(4, 'Báscula etiquetadora TLS', 'bascula-etiquetadora-tls', '<ul><li>Control total y modernidad para su negocio.\r\n</li><li>Capacidades de 20kg y 40kg.\r\n</li><li>Tecnología Touch Screen.\r\n</li><li>Memoria de hasta 10,000 productos.\r\n</li><li>Imprima tickets o etiquetas.\r\n</li><li>Tecnología inalambrica.</li></ul>', 'bascula-etiquetadora-tls.jpg', 1, 1, 4),
(5, 'Báscula Camionera Fosa', 'bascula-camionera-fosa', '<ul><li>Ideal con poco espacio.\r\n</li><li>Capacidades de hasta 100 Ton. y 32m de largo.\r\n</li><li>Confiabilidad de pesada.</li></ul>', 'bascula-camionera-fosa.jpg', 1, 1, 5),
(6, 'Báscula Camionera Rampa', 'bascula-camionera-rampa', '<ul><li>La mas económica.\r\n</li><li>La mejor opción en precio.\r\n</li><li>Capacidad de hasta 100 Ton. y 32m de largo.</li></ul>', 'bascula-camionera-rampa.jpg', 1, 1, 4),
(7, 'Báscula Camionera Portatil', 'bascula-camionera-portatil', '<ul><li>Módulos armables que llegan a abarcar 9,15m de largo.\r\n</li><li>Fácil reubicación.\r\n</li><li>La mas fácil de instalar.\r\n</li><li>Puede reubicarse a bajo costo.\r\n</li><li>Capacidades de 50 Ton.</li></ul>', 'bascula-camionera-portatil.jpg', 1, 1, 4),
(8, 'Báscula camionera BTR-80', 'bascula-camionera-btr80', '<ul><li>Fácil instalación y operación.\r\n</li><li>Asesoría e instalación profesional.\r\n</li><li>Capacidades de hasta 120 Ton.\r\n</li><li>Ahorre maniobras y traslados.</li></ul>', 'bascula-camionera-btr80.jpg', 1, 1, 4),
(9, 'Báscula de plataforma PLP', 'bascula-de-plataforma-plp', '<ul><li>Construcción robusta que garantiza durabilidad y desempeño.\r\n</li><li>Grandes dimensiones para pesar cualquier tipo de material.\r\n</li><li>Tecnología wireless.\r\n</li><li>Capacidad de hasta 5,000kg.</li></ul>', 'bascula-de-plataforma-plp.jpg', 1, 1, 4),
(10, 'Báscula contadora QC-5/10', 'bascula-contadora-qc510', '<ul><li>Obtenga peso y cantidad exacta de piezas.\r\n</li><li>Cuenta cualquier pieza aún siendo pequeña.\r\n</li><li>Reduzca el tiempo y el numero de personas al hacer inventario.\r\n</li><li>Capacidad de 5kg y 20kg.</li></ul>', 'bascula-contadora-qc510.jpg', 1, 1, 4),
(11, 'Báscula de recibo L-EQM', 'bascula-de-recibo-leqm', '<ul><li>Diseño y tecnología que soporta el uso rudo.\r\n</li><li>Capacidad y tamaño adecuado para su negocio.\r\n</li><li>Facilidad de transporte.\r\n</li><li>Capacidad hasta 500kg.</li></ul>', 'bascula-de-recibo-leqm.jpg', 1, 1, 4),
(12, 'Báscula colgante CRS-HD', 'bascula-colgante-crshd', '<ul><li>Precisión y control en una maniobra.\r\n</li><li>Mayor productividad y control en cada pesada.\r\n</li><li>Obtenga el peso exacto en una sola pesada.\r\n</li><li>Capacidad de 500kg hasta 15 Ton.</li></ul>', 'bascula-colgante-crshd.jpg', 1, 1, 4),
(13, 'Báscula colgante CRS', 'bascula-colgante-crs', '<ul><li>Capacidad de 500kg, 1,000kg, 5, 10 o 15 Ton.\r\n</li><li>Mayor rapidez y exactitud en su pesada.\r\n</li><li>Puede acoplarse a una grúa o polipasto.\r\n</li><li>Fácil manejo y sin ajustes.</li></ul>', 'bascula-colgante-crs.jpg', 1, 1, 4),
(14, 'Báscula de recibo SR', 'bascula-de-recibo-sr', '<ul><li>Plataforma ligera y fácil de transportar.\r\n</li><li>Perfecta para uso rudo.\r\n</li><li>Fácil lectura por su pantalla iluminada.\r\n</li><li>Conexión a PC o impresora.\r\n</li><li>Capacidad de 50kg.</li></ul>', 'bascula-de-recibo-sr.jpg', 1, 1, 4);

INSERT INTO `user` (`id`, `username`, `password`, `roleName`, `version`) VALUES
(1, 'misraim.mendoza@mandragora-web-systems.com', '$2y$10$K2WoW8XMknXqiJj8FS1BQubWmRGOLQyXgM.EBfO0IpFZfhAiYJYYe', 'admin', '1');

-- INSERT INTO `role` (`name`, `parentRole`, `version`) VALUES
-- ('admin', NULL, 1),
-- ('client', 'guest', 1),
-- ('guest', NULL, 1);

-- INSERT INTO `resource` (`name`, `version`) VALUES
-- ('*', 1),
-- ('admin_category', 1),
-- ('admin_product', 1),
-- ('admin_trademark', 1),
-- ('admin_index', '1');

-- INSERT INTO `permission` (`name`, `roleName`, `resourceName`, `version`) VALUES
-- ('*', 'admin', '*', 1),
-- ('*', 'guest', 'admin_index', 1);
