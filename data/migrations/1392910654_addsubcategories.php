<?php
/**
 * Migration to add support for sub-categories and product offers.
 *
 * PHP version 5.4
 *
 * This source file is subject to the license that is bundled with this package in the file LICENSE.
 *
 * @author     MMA <misraim.mendoza@mandragora-web.systems.com>
 * @copyright  Mandrágora Web-Based Systems 2011-2014
 */
class addsubcategories extends Doctrine_Migration_Base
{
    /**
     * Add support for sub-categories
     * Change password column size in order to use the new PHP 5.5 password API
     * Add tables for the offers feature
     */
    public function up()
    {
        $this->addColumn('category', 'parentCategoryId', 'integer');
        $this->addColumn('product', 'price', 'integer');
        $this->addColumn('product', 'currency', 'string', 3, ['default' => 'MXN', 'fixed' => true]);
        $this->changeColumn('user', 'password', 'string', 255);
        $this->createTable( 'offer', [
            'id' => ['type' => 'integer', 'notnull' => true, 'primary' => true, 'autoincrement' => true],
            'startDate' => ['type' => 'date', 'notnull' => true],
            'stopDate' => ['type' => 'date', 'notnull' => true],
            'type' => ['type' => 'integer', 'length' => 1, 'unsigned' => true, 'notnull' => true],
            'amount' => ['type' => 'integer', 'notnull' => true],
            'image' => ['type' => 'string', 'length' => 50],
            'description' => ['type' => 'string', 'length' => 3000],
            'version' => ['type' => 'integer', 'notnull' => true],
        ]);
        $this->createTable( 'offer_product', [
            'offerId' => ['type' => 'integer', 'notnull' => true, 'primary' => true],
            'productId' => ['type' => 'integer', 'notnull' => true, 'primary' => true],
        ]);
        $this->createForeignKey( 'offer_product', 'offer_fk', [
            'local'        => 'offerId',
            'foreign'      => 'id',
            'foreignTable' => 'offer',
        ]);
        $this->createForeignKey( 'offer_product', 'product_fk', [
            'local'        => 'productId',
            'foreign'      => 'id',
            'foreignTable' => 'product',
        ]);
    }

    /**
     * Undo changes
     */
    public function down()
    {
        $this->removeColumn('category', 'parentCategory');
        $this->removeColumn('product', 'price');
        $this->removeColumn('product', 'currency');
        $this->changeColumn('user', 'password', 'string', 45);
        $this->dropTable('offer');
        $this->dropTable('offer_product');
    }
}
